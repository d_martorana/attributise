Imports System.Runtime.InteropServices

Module Module1
    Public stAttributoInUso, stCheck As String
    Public objApplication As Global.SolidEdgeFramework.Application = Nothing
    Public objDocument As Global.SolidEdgeFramework.SolidEdgeDocument = Nothing
    Public objPropertySets As Global.SolidEdgeFramework.PropertySets = Nothing
    Public objProperties As Global.SolidEdgeFramework.Properties = Nothing
    Public objProperty As Global.SolidEdgeFramework.Property = Nothing
    Public stColoreSfondo, stColorePulsanti, stColoreCaselle As Color
    Public colFormBackground, colButtonBackGround, colTextBoxBackGround As Color
    Public bPrimaAttivazione As Boolean = False
    Public dsConfig As New Data.DataSet
    ''' <summary>
    ''' Struttura di colori
    ''' </summary>
    Public Structure ColoriBase
        Public FormBackGround As String
        Public ButtonBackGround As String
        Public TextBoxBackGround As String
    End Structure
    Public stModello, stCodiceColore1, stCodiceColore2, stCodiceColore3, stCodiceColoreStampato1, stCodiceColoreStampato2, stLivello As String
    Public dbColori As ColoriBase
    Public stFileConfigurazione, stStringa As String
    Public stMat As String
    Public bMat, stTabGenerico, stTabResina, stTabPressofuso, stTabLamiera, stTabGomma, stTabTorneria, stTabFresatura As Boolean
    Dim stSpunta As String = "�"
    Public Decimali As String = "{0:n2}"
    Public Interi As String = "{0:n0}"
    Public stConfig As String = "\\Cashprosrv2\software\SolidEdge_ST6\2D-3D\Setup\Materiali\config.xml"
End Module

Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Le informazioni generali relative a un assembly sono controllate dal seguente
' insieme di attributi. Per modificare le informazioni associate a un assembly 
' occorre quindi modificare i valori di questi attributi.

' Rivedere i valori degli attributi dell'assembly.

<Assembly: AssemblyTitle("Attributi SE")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("CTS cashpro")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'Se il progetto viene esposto a COM, il GUID che segue verr� utilizzato per creare l'ID della libreria dei tipi.
<Assembly: Guid("137A5639-319D-4288-AD9B-68BEEBB8DAC7")>

' Le informazioni sulla versione di un assembly sono costituite dai seguenti valori:
'
'      Numero di versione principale
'      Numero di versione secondario 
'      Numero build
'      Revisione
'
' � possibile specificare tutti i valori oppure impostare valori predefiniti per i numeri relativi alla revisione e alla build
' utilizzando il carattere "*" come mostrato di seguito:

<Assembly: AssemblyVersion("1.5.3.*")>

<Assembly: NeutralResourcesLanguageAttribute("it")>
<Assembly: AssemblyFileVersion("1.5.3")>

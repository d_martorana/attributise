Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Data.OleDb
Imports System.Windows.Forms

Public Class frmAttributiSE

#Region "Variabili"
    Inherits System.Windows.Forms.Form
    Private m_application As Global.SolidEdgeFramework.Application
    Public stFamAttributi As String = ""
    Public stResMat As String
    Public solidEdgePath As String
    Public solidEdgeLanguage As Integer
    Public solidEdgePreferencesPath As String
    Public ini As INIFile
    Friend WithEvents txtAut05 As System.Windows.Forms.TextBox
    Friend WithEvents txtAut04 As System.Windows.Forms.TextBox
    Friend WithEvents txtAut03 As System.Windows.Forms.TextBox
    Friend WithEvents txtAut02 As System.Windows.Forms.TextBox
    Friend WithEvents txtAut01 As System.Windows.Forms.TextBox
    Friend WithEvents txtAut06 As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents cboSpessore As System.Windows.Forms.ComboBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents cboMateriale As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmdReset As System.Windows.Forms.Button
    Friend WithEvents TblAbbinamentiBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents cboCodifica As System.Windows.Forms.ComboBox
    Friend WithEvents cboDescrizione As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtDestinazione As System.Windows.Forms.TextBox
    Friend WithEvents txtDerivato As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents cboProduttore As System.Windows.Forms.ComboBox
    Friend WithEvents cboAbbreviazione As System.Windows.Forms.ComboBox
    Friend WithEvents cboRating As System.Windows.Forms.ComboBox
    Friend WithEvents cboPNMateriale As System.Windows.Forms.ComboBox
    Friend WithEvents cboFlame As System.Windows.Forms.ComboBox
    Friend WithEvents cboFileUL As System.Windows.Forms.ComboBox
    Friend WithEvents cboColore As System.Windows.Forms.ComboBox
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents Label139 As System.Windows.Forms.Label
    Friend WithEvents Label140 As System.Windows.Forms.Label
    Friend WithEvents Label141 As System.Windows.Forms.Label
    Friend WithEvents Label142 As System.Windows.Forms.Label
    Friend WithEvents Label143 As System.Windows.Forms.Label
    Friend WithEvents Label144 As System.Windows.Forms.Label
    Friend WithEvents cboFamiglia As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Dim stAttributo As String = ""
    Public stTipoDocumento, stNomeFile, stScala, stPropertySet, stProperty, stFileCodice, stFileLivello As String '= ""
    Public iScala As Double
    Public dbConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Program Files\Solid Edge ST8\Preferences\Materials\dbMateriali.accdb"
    Public connOleDB As New OleDbConnection(dbConnectionString)
    Public cmdSQL As OleDbDataReader
    Public stSQL As String = ""
    Public stAND As String = ""
    Friend WithEvents cboTTermico As System.Windows.Forms.ComboBox
    Friend WithEvents pnlTorneria As System.Windows.Forms.Panel
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents chkTorDNI As System.Windows.Forms.CheckBox
    Friend WithEvents txtTorH As System.Windows.Forms.TextBox
    Friend WithEvents txtTorNIB As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents chkTorCHB As System.Windows.Forms.CheckBox
    Friend WithEvents chkTorCHR As System.Windows.Forms.CheckBox
    Friend WithEvents chkTorDCO As System.Windows.Forms.CheckBox
    Friend WithEvents ChkTorDCL As System.Windows.Forms.CheckBox
    Friend WithEvents chkTorDCA As System.Windows.Forms.CheckBox
    Friend WithEvents chkTorEFN As System.Windows.Forms.CheckBox
    Friend WithEvents chkTorCLE As System.Windows.Forms.CheckBox
    Friend WithEvents chkTorEFC As System.Windows.Forms.CheckBox
    Friend WithEvents pnlLamiera As System.Windows.Forms.Panel
    Friend WithEvents cboLamSFR As System.Windows.Forms.ComboBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents PictureBox32 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox31 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox30 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox29 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox28 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox24 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox21 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents cboLamTTG As System.Windows.Forms.ComboBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents chkLamTTG As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamBPS As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamCLD As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamHAD As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamDCL As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamDCA As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamDNI As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamLCM As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamSFR As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamTBN As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamTRF As System.Windows.Forms.CheckBox
    Friend WithEvents txtLamBAI As System.Windows.Forms.TextBox
    Friend WithEvents chkLamBAI As System.Windows.Forms.CheckBox
    Friend WithEvents txtLamCHA As System.Windows.Forms.TextBox
    Friend WithEvents chkLamCHA As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamBHF As System.Windows.Forms.CheckBox
    Friend WithEvents txtLamRAD As System.Windows.Forms.TextBox
    Friend WithEvents chkLamRAD As System.Windows.Forms.CheckBox
    Friend WithEvents chkLamBHM As System.Windows.Forms.CheckBox
    Friend WithEvents pnlPressofuso As System.Windows.Forms.Panel
    Friend WithEvents cboPreMAE As System.Windows.Forms.ComboBox
    Friend WithEvents chkPreMAE As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox50 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox49 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox48 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox47 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox46 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox43 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox42 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox41 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox40 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox39 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox38 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox37 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox36 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox35 As System.Windows.Forms.PictureBox
    Friend WithEvents cboPrePLF As System.Windows.Forms.ComboBox
    Friend WithEvents cboPreDRA As System.Windows.Forms.ComboBox
    Friend WithEvents cboPreTTG As System.Windows.Forms.ComboBox
    Friend WithEvents chkPreDNI As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreDAR As System.Windows.Forms.CheckBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents PictureBox34 As System.Windows.Forms.PictureBox
    Friend WithEvents chkPreTTG As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreIPJ As System.Windows.Forms.TextBox
    Friend WithEvents chkPreIPJ As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreDCL As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreFSA As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreCLH As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreTBN As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreTRF As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreMNR As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreMRA As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreDCA As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreFSH As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreMAH As System.Windows.Forms.TextBox
    Friend WithEvents chkPreMAR As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreHOS As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreIPP As System.Windows.Forms.TextBox
    Friend WithEvents chkPreIPP As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreIPT As System.Windows.Forms.TextBox
    Friend WithEvents chkPreIPT As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreSCB As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreMEP As System.Windows.Forms.TextBox
    Friend WithEvents txtPreMOF As System.Windows.Forms.TextBox
    Friend WithEvents chkPreMEP As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreMOF As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreMGM As System.Windows.Forms.TextBox
    Friend WithEvents chkPreMGM As System.Windows.Forms.CheckBox
    Friend WithEvents chkPrePLF As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreCHA As System.Windows.Forms.TextBox
    Friend WithEvents chkPreCHA As System.Windows.Forms.CheckBox
    Friend WithEvents txtPreRAD As System.Windows.Forms.TextBox
    Friend WithEvents chkPreRAD As System.Windows.Forms.CheckBox
    Friend WithEvents chkPreDRA As System.Windows.Forms.CheckBox
    Friend WithEvents pnlResina As System.Windows.Forms.Panel
    Friend WithEvents cboResSRA As System.Windows.Forms.ComboBox
    Friend WithEvents cboResSVD As System.Windows.Forms.ComboBox
    Friend WithEvents cboResMSA As System.Windows.Forms.ComboBox
    Friend WithEvents chkResMSA As System.Windows.Forms.CheckBox
    Friend WithEvents chkResDNI As System.Windows.Forms.CheckBox
    Friend WithEvents cboResMEM As System.Windows.Forms.ComboBox
    Friend WithEvents cboResMGM As System.Windows.Forms.ComboBox
    Friend WithEvents cboResPLF As System.Windows.Forms.ComboBox
    Friend WithEvents cboResDRA As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox68 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox67 As System.Windows.Forms.PictureBox
    Friend WithEvents chkResDAR As System.Windows.Forms.CheckBox
    Friend WithEvents PictureBox63 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox62 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox60 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox58 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox56 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox55 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox54 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox53 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox52 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox51 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox45 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox44 As System.Windows.Forms.PictureBox
    Friend WithEvents Label116 As System.Windows.Forms.Label
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents Label113 As System.Windows.Forms.Label
    Friend WithEvents Label123 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents chkResHAD As System.Windows.Forms.CheckBox
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label122 As System.Windows.Forms.Label
    Friend WithEvents Label121 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents txtResIPJ As System.Windows.Forms.TextBox
    Friend WithEvents chkResIPJ As System.Windows.Forms.CheckBox
    Friend WithEvents chkResSVD As System.Windows.Forms.CheckBox
    Friend WithEvents chkResMAN As System.Windows.Forms.CheckBox
    Friend WithEvents chkResMAR As System.Windows.Forms.CheckBox
    Friend WithEvents chkResMEL As System.Windows.Forms.CheckBox
    Friend WithEvents txtResMAH As System.Windows.Forms.TextBox
    Friend WithEvents chkResMAH As System.Windows.Forms.CheckBox
    Friend WithEvents txtResIPP As System.Windows.Forms.TextBox
    Friend WithEvents chkResIPP As System.Windows.Forms.CheckBox
    Friend WithEvents txtResIPT As System.Windows.Forms.TextBox
    Friend WithEvents chkResIPT As System.Windows.Forms.CheckBox
    Friend WithEvents chkResSRT As System.Windows.Forms.CheckBox
    Friend WithEvents chkResMEM As System.Windows.Forms.CheckBox
    Friend WithEvents chkResMGM As System.Windows.Forms.CheckBox
    Friend WithEvents chkResPLF As System.Windows.Forms.CheckBox
    Friend WithEvents txtResCHA As System.Windows.Forms.TextBox
    Friend WithEvents chkResCHA As System.Windows.Forms.CheckBox
    Friend WithEvents txtResRAD As System.Windows.Forms.TextBox
    Friend WithEvents chkResRAD As System.Windows.Forms.CheckBox
    Friend WithEvents chkResDRA As System.Windows.Forms.CheckBox
    Friend WithEvents pnlGomma As System.Windows.Forms.Panel
    Friend WithEvents chkGomDNI As System.Windows.Forms.CheckBox
    Friend WithEvents cboGomFLH As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents txtGomHRA As System.Windows.Forms.TextBox
    Friend WithEvents chkGomHGR As System.Windows.Forms.CheckBox
    Friend WithEvents chkGomHAI As System.Windows.Forms.CheckBox
    Friend WithEvents txtGomSHA As System.Windows.Forms.TextBox
    Friend WithEvents txtGomSHD As System.Windows.Forms.TextBox
    Friend WithEvents chkGomHAR As System.Windows.Forms.CheckBox
    Friend WithEvents chkGomMEA As System.Windows.Forms.CheckBox
    Friend WithEvents chkGomCLE As System.Windows.Forms.CheckBox
    Friend WithEvents chkGomHOS As System.Windows.Forms.CheckBox
    Friend WithEvents txtGomTRA As System.Windows.Forms.TextBox
    Friend WithEvents chkGomGTR As System.Windows.Forms.CheckBox
    Friend WithEvents txtGomTOR As System.Windows.Forms.TextBox
    Friend WithEvents chkGomGTO As System.Windows.Forms.CheckBox
    Friend WithEvents txtGomAPU As System.Windows.Forms.TextBox
    Friend WithEvents chkGomASP As System.Windows.Forms.CheckBox
    Friend WithEvents txtGomATO As System.Windows.Forms.TextBox
    Friend WithEvents chkGomAST As System.Windows.Forms.CheckBox
    Friend WithEvents txtGomMGT As System.Windows.Forms.TextBox
    Friend WithEvents chkGomMAR As System.Windows.Forms.CheckBox
    Friend WithEvents chkGomFLA As System.Windows.Forms.CheckBox
    Friend WithEvents pnlFresatura As System.Windows.Forms.Panel
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents Label124 As System.Windows.Forms.Label
    Friend WithEvents Label125 As System.Windows.Forms.Label
    Friend WithEvents PictureBox65 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox69 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox71 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox72 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox76 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox80 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox81 As System.Windows.Forms.PictureBox
    Friend WithEvents cboFreTTG As System.Windows.Forms.ComboBox
    Friend WithEvents Label128 As System.Windows.Forms.Label
    Friend WithEvents chkFreTTG As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreCLD As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreHAD As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreDCL As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreDCA As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreDNI As System.Windows.Forms.CheckBox
    Friend WithEvents chkFrePFD As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreRWP As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreTBN As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreTRF As System.Windows.Forms.CheckBox
    Friend WithEvents txtFreIPP As System.Windows.Forms.TextBox
    Friend WithEvents chkFreIPP As System.Windows.Forms.CheckBox
    Friend WithEvents txtFreIPT As System.Windows.Forms.TextBox
    Friend WithEvents chkFreIPT As System.Windows.Forms.CheckBox
    Friend WithEvents txtFreCHA As System.Windows.Forms.TextBox
    Friend WithEvents chkFreCHA As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreBHF As System.Windows.Forms.CheckBox
    Friend WithEvents txtFreRAD As System.Windows.Forms.TextBox
    Friend WithEvents chkFreRAD As System.Windows.Forms.CheckBox
    Friend WithEvents chkFreBHM As System.Windows.Forms.CheckBox
    Friend WithEvents Label131 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label130 As System.Windows.Forms.Label
    Friend WithEvents pnlLogo As System.Windows.Forms.Panel
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox59 As PictureBox
    Friend WithEvents PictureBox26 As PictureBox
    Friend WithEvents grpPrincipale As GroupBox
    Friend WithEvents chkFreSEN As CheckBox
    Friend WithEvents Label6 As Label
    Friend WithEvents chkResSGL As CheckBox
    Friend WithEvents txtSpessore As System.Windows.Forms.TextBox
    Public stFamigliaMateriale As String = ""
    Friend WithEvents cboTorCHS As ComboBox
    Friend WithEvents chkTorCHS As CheckBox
    Friend WithEvents cboTorCHN As ComboBox
    Friend WithEvents chkTorCHN As CheckBox
    Friend WithEvents PictureBox74 As PictureBox
    Friend WithEvents cboTorEES As ComboBox
    Friend WithEvents Label26 As Label
    Friend WithEvents chkTorEES As CheckBox
    Friend WithEvents Label32 As Label
    Friend WithEvents chkTorEEC As CheckBox
    Friend WithEvents chkTorEER As CheckBox
    Friend WithEvents PictureBox75 As PictureBox
    Friend WithEvents PictureBox77 As PictureBox
    Friend WithEvents PictureBox78 As PictureBox
    Friend WithEvents PictureBox83 As PictureBox
    Friend WithEvents PictureBox70 As PictureBox
    Friend WithEvents cboTorIES As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents chkTorIES As CheckBox
    Friend WithEvents Label7 As Label
    Friend WithEvents chkTorIEC As CheckBox
    Friend WithEvents chkTorIER As CheckBox
    Friend WithEvents PictureBox66 As PictureBox
    Friend WithEvents PictureBox64 As PictureBox
    Friend WithEvents PictureBox61 As PictureBox
    Friend WithEvents PictureBox57 As PictureBox
    Friend WithEvents chkTorCAQ As CheckBox
    Friend WithEvents chkFreSAR As CheckBox
    Friend WithEvents cboFreSEL As ComboBox
    Friend WithEvents PictureBox33 As PictureBox
    Friend WithEvents cboFreSEU As ComboBox
    Friend WithEvents Label44 As Label
    Friend WithEvents chkFreBEC As CheckBox
    Friend WithEvents chkFreBER As CheckBox
    Friend WithEvents PictureBox79 As PictureBox
    Friend WithEvents PictureBox84 As PictureBox
    Friend WithEvents PictureBox85 As PictureBox
    Friend WithEvents PictureBox86 As PictureBox
    Friend WithEvents PictureBox82 As PictureBox
    Friend WithEvents chkPreSAR As CheckBox
    Friend WithEvents Label5 As Label
    Friend WithEvents chkResSAR As CheckBox
    Friend WithEvents pnlGrOfficina As Panel
    Friend WithEvents chkOffIPC As CheckBox
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents Label38 As Label
    Friend WithEvents cboOffSFR As ComboBox
    Friend WithEvents Label56 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents Label83 As Label
    Friend WithEvents Label115 As Label
    Friend WithEvents Label117 As Label
    Friend WithEvents Label119 As Label
    Friend WithEvents Label126 As Label
    Friend WithEvents Label127 As Label
    Friend WithEvents PictureBox73 As PictureBox
    Friend WithEvents PictureBox87 As PictureBox
    Friend WithEvents PictureBox88 As PictureBox
    Friend WithEvents PictureBox89 As PictureBox
    Friend WithEvents PictureBox90 As PictureBox
    Friend WithEvents PictureBox91 As PictureBox
    Friend WithEvents PictureBox95 As PictureBox
    Friend WithEvents chkOffCDN As CheckBox
    Friend WithEvents chkOffSBN As CheckBox
    Friend WithEvents chkOffDCL As CheckBox
    Friend WithEvents chkOffDCA As CheckBox
    Friend WithEvents chkOffDNR As CheckBox
    Friend WithEvents chkOffSFR As CheckBox
    Friend WithEvents chkOffPFD As CheckBox
    Friend WithEvents chkOffRWP As CheckBox
    Friend WithEvents txtOffIPP As System.Windows.Forms.TextBox
    Friend WithEvents chkOffIPP As CheckBox
    Friend WithEvents txtOffIPT As System.Windows.Forms.TextBox
    Friend WithEvents chkOffIPT As CheckBox
    Friend WithEvents txtOffSWM As System.Windows.Forms.TextBox
    Friend WithEvents chkOffSWM As CheckBox
    Friend WithEvents txtOffSWP As System.Windows.Forms.TextBox
    Friend WithEvents chkOffSWP As CheckBox
    Friend WithEvents chkOffSAR As CheckBox
    Friend WithEvents chkLamSCQ As CheckBox
    Friend WithEvents cboLamESL As ComboBox
    Friend WithEvents PictureBox92 As PictureBox
    Friend WithEvents cboLamESH As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents chkLamBEC As CheckBox
    Friend WithEvents chkLamBER As CheckBox
    Friend WithEvents PictureBox93 As PictureBox
    Friend WithEvents PictureBox94 As PictureBox
    Friend WithEvents PictureBox96 As PictureBox
    Friend WithEvents chkLamBHE As CheckBox
    Friend WithEvents chkGomSAR As CheckBox
    Public spessore As Integer

#End Region
#Region " Codice generato da Progettazione Windows Form "

    Public Sub New()
        MyBase.New()

        'Chiamata richiesta da Progettazione Windows Form.
        InitializeComponent()

        'Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent()

    End Sub

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue � richiesta da Progettazione Windows Form.
    'Pu� essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtCodice As System.Windows.Forms.TextBox
    Friend WithEvents txtDescrizione As System.Windows.Forms.TextBox
    Friend WithEvents txtAutore As System.Windows.Forms.TextBox
    Friend WithEvents txtResponsabile As System.Windows.Forms.TextBox
    Friend WithEvents cboTSuperficiale As System.Windows.Forms.ComboBox
    Friend WithEvents cmdEsci As System.Windows.Forms.Button
    Friend WithEvents cmdSalva As System.Windows.Forms.Button
    Friend WithEvents cmdLeggi As System.Windows.Forms.Button
    Friend WithEvents txtLivello As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Public WithEvents cboData As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdSalvaEsci As System.Windows.Forms.Button
    Friend WithEvents txtData06 As System.Windows.Forms.TextBox
    Friend WithEvents txtData05 As System.Windows.Forms.TextBox
    Friend WithEvents txtData04 As System.Windows.Forms.TextBox
    Friend WithEvents txtData03 As System.Windows.Forms.TextBox
    Friend WithEvents txtData02 As System.Windows.Forms.TextBox
    Friend WithEvents txtData01 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtMod01 As System.Windows.Forms.TextBox
    Friend WithEvents txtMod02 As System.Windows.Forms.TextBox
    Friend WithEvents txtMod03 As System.Windows.Forms.TextBox
    Friend WithEvents txtMod04 As System.Windows.Forms.TextBox
    Friend WithEvents txtMod05 As System.Windows.Forms.TextBox
    Friend WithEvents txtMod06 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblCaratteriDescrizione As System.Windows.Forms.Label
    Friend WithEvents lblCaratteriCodice As System.Windows.Forms.Label
    Friend WithEvents cmdSlitta As System.Windows.Forms.Button
    Friend WithEvents cmdAzzeraRev As System.Windows.Forms.Button
    'Friend WithEvents txtMatConduttivit� As System.Windows.Forms.TextBox
    'Friend WithEvents txtMatDensit� As System.Windows.Forms.TextBox
    'Friend WithEvents txtMatTensione As System.Windows.Forms.TextBox
    'Friend WithEvents txtMatSnervamento As System.Windows.Forms.TextBox
    Friend WithEvents txtPoisson As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents cboScala As System.Windows.Forms.ComboBox
    Friend WithEvents txtSemilavorato As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtSostituisce As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    'Friend WithEvents txtMatElasticit� As System.Windows.Forms.TextBox
    'Friend WithEvents txtMatCalore As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAttributiSE))
        Me.cmdEsci = New System.Windows.Forms.Button()
        Me.cmdSalva = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtCodice = New System.Windows.Forms.TextBox()
        Me.txtDescrizione = New System.Windows.Forms.TextBox()
        Me.txtAutore = New System.Windows.Forms.TextBox()
        Me.txtResponsabile = New System.Windows.Forms.TextBox()
        Me.cboTSuperficiale = New System.Windows.Forms.ComboBox()
        Me.cmdLeggi = New System.Windows.Forms.Button()
        Me.txtLivello = New System.Windows.Forms.TextBox()
        Me.cmdSalvaEsci = New System.Windows.Forms.Button()
        Me.cboData = New System.Windows.Forms.DateTimePicker()
        Me.cmdAzzeraRev = New System.Windows.Forms.Button()
        Me.cboScala = New System.Windows.Forms.ComboBox()
        Me.txtSemilavorato = New System.Windows.Forms.TextBox()
        Me.txtSostituisce = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtData06 = New System.Windows.Forms.TextBox()
        Me.txtData05 = New System.Windows.Forms.TextBox()
        Me.txtData04 = New System.Windows.Forms.TextBox()
        Me.txtData03 = New System.Windows.Forms.TextBox()
        Me.txtData02 = New System.Windows.Forms.TextBox()
        Me.txtData01 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtMod01 = New System.Windows.Forms.TextBox()
        Me.txtMod02 = New System.Windows.Forms.TextBox()
        Me.txtMod03 = New System.Windows.Forms.TextBox()
        Me.txtMod04 = New System.Windows.Forms.TextBox()
        Me.txtMod05 = New System.Windows.Forms.TextBox()
        Me.txtMod06 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtAut05 = New System.Windows.Forms.TextBox()
        Me.txtAut04 = New System.Windows.Forms.TextBox()
        Me.txtAut03 = New System.Windows.Forms.TextBox()
        Me.txtAut02 = New System.Windows.Forms.TextBox()
        Me.txtAut01 = New System.Windows.Forms.TextBox()
        Me.txtAut06 = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txtSpessore = New System.Windows.Forms.TextBox()
        Me.cmdSlitta = New System.Windows.Forms.Button()
        Me.lblCaratteriDescrizione = New System.Windows.Forms.Label()
        Me.lblCaratteriCodice = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.cboSpessore = New System.Windows.Forms.ComboBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.cboMateriale = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cmdReset = New System.Windows.Forms.Button()
        Me.cboCodifica = New System.Windows.Forms.ComboBox()
        Me.cboDescrizione = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDestinazione = New System.Windows.Forms.TextBox()
        Me.txtDerivato = New System.Windows.Forms.TextBox()
        Me.cboProduttore = New System.Windows.Forms.ComboBox()
        Me.cboAbbreviazione = New System.Windows.Forms.ComboBox()
        Me.cboRating = New System.Windows.Forms.ComboBox()
        Me.cboPNMateriale = New System.Windows.Forms.ComboBox()
        Me.cboFlame = New System.Windows.Forms.ComboBox()
        Me.cboFileUL = New System.Windows.Forms.ComboBox()
        Me.cboColore = New System.Windows.Forms.ComboBox()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.Label139 = New System.Windows.Forms.Label()
        Me.Label140 = New System.Windows.Forms.Label()
        Me.Label141 = New System.Windows.Forms.Label()
        Me.Label142 = New System.Windows.Forms.Label()
        Me.Label143 = New System.Windows.Forms.Label()
        Me.Label144 = New System.Windows.Forms.Label()
        Me.cboFamiglia = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cboTTermico = New System.Windows.Forms.ComboBox()
        Me.pnlTorneria = New System.Windows.Forms.Panel()
        Me.chkTorCAQ = New System.Windows.Forms.CheckBox()
        Me.cboTorCHS = New System.Windows.Forms.ComboBox()
        Me.chkTorCHS = New System.Windows.Forms.CheckBox()
        Me.cboTorCHN = New System.Windows.Forms.ComboBox()
        Me.chkTorCHN = New System.Windows.Forms.CheckBox()
        Me.PictureBox74 = New System.Windows.Forms.PictureBox()
        Me.cboTorEES = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.chkTorEES = New System.Windows.Forms.CheckBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.chkTorEEC = New System.Windows.Forms.CheckBox()
        Me.chkTorEER = New System.Windows.Forms.CheckBox()
        Me.PictureBox75 = New System.Windows.Forms.PictureBox()
        Me.PictureBox77 = New System.Windows.Forms.PictureBox()
        Me.PictureBox78 = New System.Windows.Forms.PictureBox()
        Me.PictureBox83 = New System.Windows.Forms.PictureBox()
        Me.PictureBox70 = New System.Windows.Forms.PictureBox()
        Me.cboTorIES = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.chkTorIES = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkTorIEC = New System.Windows.Forms.CheckBox()
        Me.chkTorIER = New System.Windows.Forms.CheckBox()
        Me.PictureBox66 = New System.Windows.Forms.PictureBox()
        Me.PictureBox64 = New System.Windows.Forms.PictureBox()
        Me.PictureBox61 = New System.Windows.Forms.PictureBox()
        Me.PictureBox57 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox59 = New System.Windows.Forms.PictureBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.chkTorDNI = New System.Windows.Forms.CheckBox()
        Me.txtTorH = New System.Windows.Forms.TextBox()
        Me.txtTorNIB = New System.Windows.Forms.TextBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.chkTorCHB = New System.Windows.Forms.CheckBox()
        Me.chkTorCHR = New System.Windows.Forms.CheckBox()
        Me.chkTorDCO = New System.Windows.Forms.CheckBox()
        Me.ChkTorDCL = New System.Windows.Forms.CheckBox()
        Me.chkTorDCA = New System.Windows.Forms.CheckBox()
        Me.chkTorEFN = New System.Windows.Forms.CheckBox()
        Me.chkTorCLE = New System.Windows.Forms.CheckBox()
        Me.chkTorEFC = New System.Windows.Forms.CheckBox()
        Me.pnlLamiera = New System.Windows.Forms.Panel()
        Me.chkLamSCQ = New System.Windows.Forms.CheckBox()
        Me.cboLamESL = New System.Windows.Forms.ComboBox()
        Me.PictureBox92 = New System.Windows.Forms.PictureBox()
        Me.cboLamESH = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkLamBEC = New System.Windows.Forms.CheckBox()
        Me.chkLamBER = New System.Windows.Forms.CheckBox()
        Me.PictureBox93 = New System.Windows.Forms.PictureBox()
        Me.PictureBox94 = New System.Windows.Forms.PictureBox()
        Me.PictureBox96 = New System.Windows.Forms.PictureBox()
        Me.chkLamBHE = New System.Windows.Forms.CheckBox()
        Me.Label131 = New System.Windows.Forms.Label()
        Me.cboLamSFR = New System.Windows.Forms.ComboBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.PictureBox32 = New System.Windows.Forms.PictureBox()
        Me.PictureBox31 = New System.Windows.Forms.PictureBox()
        Me.PictureBox30 = New System.Windows.Forms.PictureBox()
        Me.PictureBox29 = New System.Windows.Forms.PictureBox()
        Me.PictureBox28 = New System.Windows.Forms.PictureBox()
        Me.PictureBox24 = New System.Windows.Forms.PictureBox()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.cboLamTTG = New System.Windows.Forms.ComboBox()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.chkLamTTG = New System.Windows.Forms.CheckBox()
        Me.chkLamBPS = New System.Windows.Forms.CheckBox()
        Me.chkLamCLD = New System.Windows.Forms.CheckBox()
        Me.chkLamHAD = New System.Windows.Forms.CheckBox()
        Me.chkLamDCL = New System.Windows.Forms.CheckBox()
        Me.chkLamDCA = New System.Windows.Forms.CheckBox()
        Me.chkLamDNI = New System.Windows.Forms.CheckBox()
        Me.chkLamLCM = New System.Windows.Forms.CheckBox()
        Me.chkLamSFR = New System.Windows.Forms.CheckBox()
        Me.chkLamTBN = New System.Windows.Forms.CheckBox()
        Me.chkLamTRF = New System.Windows.Forms.CheckBox()
        Me.txtLamBAI = New System.Windows.Forms.TextBox()
        Me.chkLamBAI = New System.Windows.Forms.CheckBox()
        Me.txtLamCHA = New System.Windows.Forms.TextBox()
        Me.chkLamCHA = New System.Windows.Forms.CheckBox()
        Me.chkLamBHF = New System.Windows.Forms.CheckBox()
        Me.txtLamRAD = New System.Windows.Forms.TextBox()
        Me.chkLamRAD = New System.Windows.Forms.CheckBox()
        Me.chkLamBHM = New System.Windows.Forms.CheckBox()
        Me.pnlPressofuso = New System.Windows.Forms.Panel()
        Me.chkPreSAR = New System.Windows.Forms.CheckBox()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.cboPreMAE = New System.Windows.Forms.ComboBox()
        Me.chkPreMAE = New System.Windows.Forms.CheckBox()
        Me.PictureBox50 = New System.Windows.Forms.PictureBox()
        Me.PictureBox49 = New System.Windows.Forms.PictureBox()
        Me.PictureBox48 = New System.Windows.Forms.PictureBox()
        Me.PictureBox47 = New System.Windows.Forms.PictureBox()
        Me.PictureBox46 = New System.Windows.Forms.PictureBox()
        Me.PictureBox43 = New System.Windows.Forms.PictureBox()
        Me.PictureBox42 = New System.Windows.Forms.PictureBox()
        Me.PictureBox41 = New System.Windows.Forms.PictureBox()
        Me.PictureBox40 = New System.Windows.Forms.PictureBox()
        Me.PictureBox39 = New System.Windows.Forms.PictureBox()
        Me.PictureBox38 = New System.Windows.Forms.PictureBox()
        Me.PictureBox37 = New System.Windows.Forms.PictureBox()
        Me.PictureBox36 = New System.Windows.Forms.PictureBox()
        Me.PictureBox35 = New System.Windows.Forms.PictureBox()
        Me.cboPrePLF = New System.Windows.Forms.ComboBox()
        Me.cboPreDRA = New System.Windows.Forms.ComboBox()
        Me.cboPreTTG = New System.Windows.Forms.ComboBox()
        Me.chkPreDNI = New System.Windows.Forms.CheckBox()
        Me.chkPreDAR = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.PictureBox34 = New System.Windows.Forms.PictureBox()
        Me.chkPreTTG = New System.Windows.Forms.CheckBox()
        Me.txtPreIPJ = New System.Windows.Forms.TextBox()
        Me.chkPreIPJ = New System.Windows.Forms.CheckBox()
        Me.chkPreDCL = New System.Windows.Forms.CheckBox()
        Me.chkPreFSA = New System.Windows.Forms.CheckBox()
        Me.chkPreCLH = New System.Windows.Forms.CheckBox()
        Me.chkPreTBN = New System.Windows.Forms.CheckBox()
        Me.chkPreTRF = New System.Windows.Forms.CheckBox()
        Me.chkPreMNR = New System.Windows.Forms.CheckBox()
        Me.chkPreMRA = New System.Windows.Forms.CheckBox()
        Me.chkPreDCA = New System.Windows.Forms.CheckBox()
        Me.chkPreFSH = New System.Windows.Forms.CheckBox()
        Me.txtPreMAH = New System.Windows.Forms.TextBox()
        Me.chkPreMAR = New System.Windows.Forms.CheckBox()
        Me.chkPreHOS = New System.Windows.Forms.CheckBox()
        Me.txtPreIPP = New System.Windows.Forms.TextBox()
        Me.chkPreIPP = New System.Windows.Forms.CheckBox()
        Me.txtPreIPT = New System.Windows.Forms.TextBox()
        Me.chkPreIPT = New System.Windows.Forms.CheckBox()
        Me.chkPreSCB = New System.Windows.Forms.CheckBox()
        Me.txtPreMEP = New System.Windows.Forms.TextBox()
        Me.txtPreMOF = New System.Windows.Forms.TextBox()
        Me.chkPreMEP = New System.Windows.Forms.CheckBox()
        Me.chkPreMOF = New System.Windows.Forms.CheckBox()
        Me.txtPreMGM = New System.Windows.Forms.TextBox()
        Me.chkPreMGM = New System.Windows.Forms.CheckBox()
        Me.chkPrePLF = New System.Windows.Forms.CheckBox()
        Me.txtPreCHA = New System.Windows.Forms.TextBox()
        Me.chkPreCHA = New System.Windows.Forms.CheckBox()
        Me.txtPreRAD = New System.Windows.Forms.TextBox()
        Me.chkPreRAD = New System.Windows.Forms.CheckBox()
        Me.chkPreDRA = New System.Windows.Forms.CheckBox()
        Me.pnlResina = New System.Windows.Forms.Panel()
        Me.chkResSAR = New System.Windows.Forms.CheckBox()
        Me.chkResSGL = New System.Windows.Forms.CheckBox()
        Me.cboResSRA = New System.Windows.Forms.ComboBox()
        Me.cboResSVD = New System.Windows.Forms.ComboBox()
        Me.cboResMSA = New System.Windows.Forms.ComboBox()
        Me.chkResMSA = New System.Windows.Forms.CheckBox()
        Me.chkResDNI = New System.Windows.Forms.CheckBox()
        Me.cboResMEM = New System.Windows.Forms.ComboBox()
        Me.cboResMGM = New System.Windows.Forms.ComboBox()
        Me.cboResPLF = New System.Windows.Forms.ComboBox()
        Me.cboResDRA = New System.Windows.Forms.ComboBox()
        Me.PictureBox68 = New System.Windows.Forms.PictureBox()
        Me.PictureBox67 = New System.Windows.Forms.PictureBox()
        Me.chkResDAR = New System.Windows.Forms.CheckBox()
        Me.PictureBox63 = New System.Windows.Forms.PictureBox()
        Me.PictureBox62 = New System.Windows.Forms.PictureBox()
        Me.PictureBox60 = New System.Windows.Forms.PictureBox()
        Me.PictureBox58 = New System.Windows.Forms.PictureBox()
        Me.PictureBox56 = New System.Windows.Forms.PictureBox()
        Me.PictureBox55 = New System.Windows.Forms.PictureBox()
        Me.PictureBox54 = New System.Windows.Forms.PictureBox()
        Me.PictureBox53 = New System.Windows.Forms.PictureBox()
        Me.PictureBox52 = New System.Windows.Forms.PictureBox()
        Me.PictureBox51 = New System.Windows.Forms.PictureBox()
        Me.PictureBox45 = New System.Windows.Forms.PictureBox()
        Me.PictureBox44 = New System.Windows.Forms.PictureBox()
        Me.Label116 = New System.Windows.Forms.Label()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.Label113 = New System.Windows.Forms.Label()
        Me.Label123 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.chkResHAD = New System.Windows.Forms.CheckBox()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label122 = New System.Windows.Forms.Label()
        Me.Label121 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.txtResIPJ = New System.Windows.Forms.TextBox()
        Me.chkResIPJ = New System.Windows.Forms.CheckBox()
        Me.chkResSVD = New System.Windows.Forms.CheckBox()
        Me.chkResMAN = New System.Windows.Forms.CheckBox()
        Me.chkResMAR = New System.Windows.Forms.CheckBox()
        Me.chkResMEL = New System.Windows.Forms.CheckBox()
        Me.txtResMAH = New System.Windows.Forms.TextBox()
        Me.chkResMAH = New System.Windows.Forms.CheckBox()
        Me.txtResIPP = New System.Windows.Forms.TextBox()
        Me.chkResIPP = New System.Windows.Forms.CheckBox()
        Me.txtResIPT = New System.Windows.Forms.TextBox()
        Me.chkResIPT = New System.Windows.Forms.CheckBox()
        Me.chkResSRT = New System.Windows.Forms.CheckBox()
        Me.chkResMEM = New System.Windows.Forms.CheckBox()
        Me.chkResMGM = New System.Windows.Forms.CheckBox()
        Me.chkResPLF = New System.Windows.Forms.CheckBox()
        Me.txtResCHA = New System.Windows.Forms.TextBox()
        Me.chkResCHA = New System.Windows.Forms.CheckBox()
        Me.txtResRAD = New System.Windows.Forms.TextBox()
        Me.chkResRAD = New System.Windows.Forms.CheckBox()
        Me.chkResDRA = New System.Windows.Forms.CheckBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.pnlGomma = New System.Windows.Forms.Panel()
        Me.chkGomSAR = New System.Windows.Forms.CheckBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.chkGomDNI = New System.Windows.Forms.CheckBox()
        Me.cboGomFLH = New System.Windows.Forms.ComboBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.txtGomHRA = New System.Windows.Forms.TextBox()
        Me.chkGomHGR = New System.Windows.Forms.CheckBox()
        Me.chkGomHAI = New System.Windows.Forms.CheckBox()
        Me.txtGomSHA = New System.Windows.Forms.TextBox()
        Me.txtGomSHD = New System.Windows.Forms.TextBox()
        Me.chkGomHAR = New System.Windows.Forms.CheckBox()
        Me.chkGomMEA = New System.Windows.Forms.CheckBox()
        Me.chkGomCLE = New System.Windows.Forms.CheckBox()
        Me.chkGomHOS = New System.Windows.Forms.CheckBox()
        Me.txtGomTRA = New System.Windows.Forms.TextBox()
        Me.chkGomGTR = New System.Windows.Forms.CheckBox()
        Me.txtGomTOR = New System.Windows.Forms.TextBox()
        Me.chkGomGTO = New System.Windows.Forms.CheckBox()
        Me.txtGomAPU = New System.Windows.Forms.TextBox()
        Me.chkGomASP = New System.Windows.Forms.CheckBox()
        Me.txtGomATO = New System.Windows.Forms.TextBox()
        Me.chkGomAST = New System.Windows.Forms.CheckBox()
        Me.txtGomMGT = New System.Windows.Forms.TextBox()
        Me.chkGomMAR = New System.Windows.Forms.CheckBox()
        Me.chkGomFLA = New System.Windows.Forms.CheckBox()
        Me.pnlFresatura = New System.Windows.Forms.Panel()
        Me.chkFreSAR = New System.Windows.Forms.CheckBox()
        Me.cboFreSEL = New System.Windows.Forms.ComboBox()
        Me.PictureBox33 = New System.Windows.Forms.PictureBox()
        Me.cboFreSEU = New System.Windows.Forms.ComboBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.chkFreBEC = New System.Windows.Forms.CheckBox()
        Me.chkFreBER = New System.Windows.Forms.CheckBox()
        Me.PictureBox79 = New System.Windows.Forms.PictureBox()
        Me.PictureBox84 = New System.Windows.Forms.PictureBox()
        Me.PictureBox85 = New System.Windows.Forms.PictureBox()
        Me.PictureBox86 = New System.Windows.Forms.PictureBox()
        Me.chkFreSEN = New System.Windows.Forms.CheckBox()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.Label130 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label124 = New System.Windows.Forms.Label()
        Me.Label125 = New System.Windows.Forms.Label()
        Me.PictureBox65 = New System.Windows.Forms.PictureBox()
        Me.PictureBox69 = New System.Windows.Forms.PictureBox()
        Me.PictureBox71 = New System.Windows.Forms.PictureBox()
        Me.PictureBox72 = New System.Windows.Forms.PictureBox()
        Me.PictureBox76 = New System.Windows.Forms.PictureBox()
        Me.PictureBox80 = New System.Windows.Forms.PictureBox()
        Me.PictureBox81 = New System.Windows.Forms.PictureBox()
        Me.cboFreTTG = New System.Windows.Forms.ComboBox()
        Me.PictureBox82 = New System.Windows.Forms.PictureBox()
        Me.Label128 = New System.Windows.Forms.Label()
        Me.chkFreTTG = New System.Windows.Forms.CheckBox()
        Me.chkFreCLD = New System.Windows.Forms.CheckBox()
        Me.chkFreHAD = New System.Windows.Forms.CheckBox()
        Me.chkFreDCL = New System.Windows.Forms.CheckBox()
        Me.chkFreDCA = New System.Windows.Forms.CheckBox()
        Me.chkFreDNI = New System.Windows.Forms.CheckBox()
        Me.chkFrePFD = New System.Windows.Forms.CheckBox()
        Me.chkFreRWP = New System.Windows.Forms.CheckBox()
        Me.chkFreTBN = New System.Windows.Forms.CheckBox()
        Me.chkFreTRF = New System.Windows.Forms.CheckBox()
        Me.txtFreIPP = New System.Windows.Forms.TextBox()
        Me.chkFreIPP = New System.Windows.Forms.CheckBox()
        Me.txtFreIPT = New System.Windows.Forms.TextBox()
        Me.chkFreIPT = New System.Windows.Forms.CheckBox()
        Me.txtFreCHA = New System.Windows.Forms.TextBox()
        Me.chkFreCHA = New System.Windows.Forms.CheckBox()
        Me.chkFreBHF = New System.Windows.Forms.CheckBox()
        Me.txtFreRAD = New System.Windows.Forms.TextBox()
        Me.chkFreRAD = New System.Windows.Forms.CheckBox()
        Me.chkFreBHM = New System.Windows.Forms.CheckBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlLogo = New System.Windows.Forms.Panel()
        Me.grpPrincipale = New System.Windows.Forms.GroupBox()
        Me.pnlGrOfficina = New System.Windows.Forms.Panel()
        Me.chkOffSAR = New System.Windows.Forms.CheckBox()
        Me.chkOffIPC = New System.Windows.Forms.CheckBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.cboOffSFR = New System.Windows.Forms.ComboBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.Label115 = New System.Windows.Forms.Label()
        Me.Label117 = New System.Windows.Forms.Label()
        Me.Label119 = New System.Windows.Forms.Label()
        Me.Label126 = New System.Windows.Forms.Label()
        Me.Label127 = New System.Windows.Forms.Label()
        Me.PictureBox73 = New System.Windows.Forms.PictureBox()
        Me.PictureBox87 = New System.Windows.Forms.PictureBox()
        Me.PictureBox88 = New System.Windows.Forms.PictureBox()
        Me.PictureBox89 = New System.Windows.Forms.PictureBox()
        Me.PictureBox90 = New System.Windows.Forms.PictureBox()
        Me.PictureBox91 = New System.Windows.Forms.PictureBox()
        Me.PictureBox95 = New System.Windows.Forms.PictureBox()
        Me.chkOffCDN = New System.Windows.Forms.CheckBox()
        Me.chkOffSBN = New System.Windows.Forms.CheckBox()
        Me.chkOffDCL = New System.Windows.Forms.CheckBox()
        Me.chkOffDCA = New System.Windows.Forms.CheckBox()
        Me.chkOffDNR = New System.Windows.Forms.CheckBox()
        Me.chkOffSFR = New System.Windows.Forms.CheckBox()
        Me.chkOffPFD = New System.Windows.Forms.CheckBox()
        Me.chkOffRWP = New System.Windows.Forms.CheckBox()
        Me.txtOffIPP = New System.Windows.Forms.TextBox()
        Me.chkOffIPP = New System.Windows.Forms.CheckBox()
        Me.txtOffIPT = New System.Windows.Forms.TextBox()
        Me.chkOffIPT = New System.Windows.Forms.CheckBox()
        Me.txtOffSWM = New System.Windows.Forms.TextBox()
        Me.chkOffSWM = New System.Windows.Forms.CheckBox()
        Me.txtOffSWP = New System.Windows.Forms.TextBox()
        Me.chkOffSWP = New System.Windows.Forms.CheckBox()
        Me.GroupBox2.SuspendLayout()
        Me.pnlTorneria.SuspendLayout()
        CType(Me.PictureBox74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlLamiera.SuspendLayout()
        CType(Me.PictureBox92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlPressofuso.SuspendLayout()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlResina.SuspendLayout()
        CType(Me.PictureBox68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlGomma.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFresatura.SuspendLayout()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPrincipale.SuspendLayout()
        Me.pnlGrOfficina.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox95, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdEsci
        '
        Me.cmdEsci.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmdEsci.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdEsci.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdEsci.Location = New System.Drawing.Point(443, 428)
        Me.cmdEsci.Name = "cmdEsci"
        Me.cmdEsci.Size = New System.Drawing.Size(80, 49)
        Me.cmdEsci.TabIndex = 60
        Me.cmdEsci.Text = "Esci"
        Me.cmdEsci.UseVisualStyleBackColor = False
        '
        'cmdSalva
        '
        Me.cmdSalva.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmdSalva.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSalva.Location = New System.Drawing.Point(79, 428)
        Me.cmdSalva.Name = "cmdSalva"
        Me.cmdSalva.Size = New System.Drawing.Size(63, 49)
        Me.cmdSalva.TabIndex = 57
        Me.cmdSalva.Text = "Salva"
        Me.cmdSalva.UseVisualStyleBackColor = False
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.Location = New System.Drawing.Point(11, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(72, 16)
        Me.Label10.TabIndex = 45
        Me.Label10.Text = "Codice:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label11.Location = New System.Drawing.Point(293, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 16)
        Me.Label11.TabIndex = 46
        Me.Label11.Text = "Descrizione:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.Location = New System.Drawing.Point(12, 99)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 16)
        Me.Label15.TabIndex = 50
        Me.Label15.Text = "Autore:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.Location = New System.Drawing.Point(293, 99)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(72, 16)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = "Responsabile:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label17.Location = New System.Drawing.Point(435, 95)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 16)
        Me.Label17.TabIndex = 52
        Me.Label17.Text = "Data:"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label18.Location = New System.Drawing.Point(12, 225)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(135, 15)
        Me.Label18.TabIndex = 53
        Me.Label18.Text = "Trattamento superficiale:"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label21.Location = New System.Drawing.Point(152, 137)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(135, 16)
        Me.Label21.TabIndex = 56
        Me.Label21.Text = "Codifica Cat. Tecnologica:"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'txtCodice
        '
        Me.txtCodice.BackColor = System.Drawing.Color.White
        Me.txtCodice.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodice.Location = New System.Drawing.Point(11, 32)
        Me.txtCodice.MaxLength = 17
        Me.txtCodice.Name = "txtCodice"
        Me.txtCodice.Size = New System.Drawing.Size(224, 22)
        Me.txtCodice.TabIndex = 0
        '
        'txtDescrizione
        '
        Me.txtDescrizione.BackColor = System.Drawing.Color.White
        Me.txtDescrizione.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescrizione.Location = New System.Drawing.Point(293, 32)
        Me.txtDescrizione.Name = "txtDescrizione"
        Me.txtDescrizione.Size = New System.Drawing.Size(258, 22)
        Me.txtDescrizione.TabIndex = 2
        '
        'txtAutore
        '
        Me.txtAutore.BackColor = System.Drawing.Color.White
        Me.txtAutore.Location = New System.Drawing.Point(11, 115)
        Me.txtAutore.Name = "txtAutore"
        Me.txtAutore.Size = New System.Drawing.Size(272, 20)
        Me.txtAutore.TabIndex = 7
        '
        'txtResponsabile
        '
        Me.txtResponsabile.BackColor = System.Drawing.Color.White
        Me.txtResponsabile.Location = New System.Drawing.Point(294, 115)
        Me.txtResponsabile.Name = "txtResponsabile"
        Me.txtResponsabile.Size = New System.Drawing.Size(128, 20)
        Me.txtResponsabile.TabIndex = 8
        '
        'cboTSuperficiale
        '
        Me.cboTSuperficiale.BackColor = System.Drawing.Color.White
        Me.cboTSuperficiale.ItemHeight = 13
        Me.cboTSuperficiale.Location = New System.Drawing.Point(11, 241)
        Me.cboTSuperficiale.Name = "cboTSuperficiale"
        Me.cboTSuperficiale.Size = New System.Drawing.Size(130, 21)
        Me.cboTSuperficiale.Sorted = True
        Me.cboTSuperficiale.TabIndex = 17
        '
        'cmdLeggi
        '
        Me.cmdLeggi.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmdLeggi.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdLeggi.Location = New System.Drawing.Point(9, 428)
        Me.cmdLeggi.Name = "cmdLeggi"
        Me.cmdLeggi.Size = New System.Drawing.Size(63, 49)
        Me.cmdLeggi.TabIndex = 56
        Me.cmdLeggi.Text = "Leggi"
        Me.cmdLeggi.UseVisualStyleBackColor = False
        '
        'txtLivello
        '
        Me.txtLivello.BackColor = System.Drawing.Color.White
        Me.txtLivello.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLivello.Location = New System.Drawing.Point(243, 32)
        Me.txtLivello.MaxLength = 17
        Me.txtLivello.Name = "txtLivello"
        Me.txtLivello.Size = New System.Drawing.Size(40, 22)
        Me.txtLivello.TabIndex = 1
        '
        'cmdSalvaEsci
        '
        Me.cmdSalvaEsci.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmdSalvaEsci.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdSalvaEsci.Location = New System.Drawing.Point(41, 658)
        Me.cmdSalvaEsci.Name = "cmdSalvaEsci"
        Me.cmdSalvaEsci.Size = New System.Drawing.Size(98, 49)
        Me.cmdSalvaEsci.TabIndex = 61
        Me.cmdSalvaEsci.Text = "Salva ed Esci"
        Me.cmdSalvaEsci.UseVisualStyleBackColor = False
        Me.cmdSalvaEsci.Visible = False
        '
        'cboData
        '
        Me.cboData.CalendarMonthBackground = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(216, Byte), Integer))
        Me.cboData.CalendarTitleForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(216, Byte), Integer))
        Me.cboData.CustomFormat = ""
        Me.cboData.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.cboData.Location = New System.Drawing.Point(435, 115)
        Me.cboData.Name = "cboData"
        Me.cboData.Size = New System.Drawing.Size(116, 20)
        Me.cboData.TabIndex = 9
        '
        'cmdAzzeraRev
        '
        Me.cmdAzzeraRev.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmdAzzeraRev.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdAzzeraRev.Location = New System.Drawing.Point(149, 428)
        Me.cmdAzzeraRev.Name = "cmdAzzeraRev"
        Me.cmdAzzeraRev.Size = New System.Drawing.Size(98, 49)
        Me.cmdAzzeraRev.TabIndex = 58
        Me.cmdAzzeraRev.Text = "Azzera Revisioni"
        Me.cmdAzzeraRev.UseVisualStyleBackColor = False
        '
        'cboScala
        '
        Me.cboScala.BackColor = System.Drawing.Color.White
        Me.cboScala.Enabled = False
        Me.cboScala.Items.AddRange(New Object() {"20:1", "10:1", "5:1", "2:1", "1:1", "1:2", "1:5", "1:10", "1:20"})
        Me.cboScala.Location = New System.Drawing.Point(1175, 65)
        Me.cboScala.Name = "cboScala"
        Me.cboScala.Size = New System.Drawing.Size(119, 21)
        Me.cboScala.TabIndex = 10
        Me.cboScala.Visible = False
        '
        'txtSemilavorato
        '
        Me.txtSemilavorato.BackColor = System.Drawing.Color.White
        Me.txtSemilavorato.Location = New System.Drawing.Point(431, 74)
        Me.txtSemilavorato.Name = "txtSemilavorato"
        Me.txtSemilavorato.Size = New System.Drawing.Size(120, 20)
        Me.txtSemilavorato.TabIndex = 6
        '
        'txtSostituisce
        '
        Me.txtSostituisce.BackColor = System.Drawing.Color.White
        Me.txtSostituisce.Location = New System.Drawing.Point(11, 74)
        Me.txtSostituisce.Name = "txtSostituisce"
        Me.txtSostituisce.Size = New System.Drawing.Size(129, 20)
        Me.txtSostituisce.TabIndex = 3
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label35.Location = New System.Drawing.Point(240, 16)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(40, 16)
        Me.Label35.TabIndex = 220
        Me.Label35.Text = "Livello:"
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label36.Location = New System.Drawing.Point(153, 225)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(110, 17)
        Me.Label36.TabIndex = 223
        Me.Label36.Text = "Trattamento termico:"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'txtData06
        '
        Me.txtData06.BackColor = System.Drawing.Color.White
        Me.txtData06.Location = New System.Drawing.Point(351, 86)
        Me.txtData06.MaxLength = 8
        Me.txtData06.Name = "txtData06"
        Me.txtData06.Size = New System.Drawing.Size(61, 20)
        Me.txtData06.TabIndex = 38
        '
        'txtData05
        '
        Me.txtData05.BackColor = System.Drawing.Color.White
        Me.txtData05.Location = New System.Drawing.Point(351, 62)
        Me.txtData05.MaxLength = 8
        Me.txtData05.Name = "txtData05"
        Me.txtData05.Size = New System.Drawing.Size(61, 20)
        Me.txtData05.TabIndex = 34
        '
        'txtData04
        '
        Me.txtData04.BackColor = System.Drawing.Color.White
        Me.txtData04.Location = New System.Drawing.Point(351, 38)
        Me.txtData04.MaxLength = 8
        Me.txtData04.Name = "txtData04"
        Me.txtData04.Size = New System.Drawing.Size(61, 20)
        Me.txtData04.TabIndex = 30
        '
        'txtData03
        '
        Me.txtData03.BackColor = System.Drawing.Color.White
        Me.txtData03.Location = New System.Drawing.Point(68, 86)
        Me.txtData03.MaxLength = 8
        Me.txtData03.Name = "txtData03"
        Me.txtData03.Size = New System.Drawing.Size(61, 20)
        Me.txtData03.TabIndex = 26
        '
        'txtData02
        '
        Me.txtData02.BackColor = System.Drawing.Color.White
        Me.txtData02.Location = New System.Drawing.Point(68, 62)
        Me.txtData02.MaxLength = 8
        Me.txtData02.Name = "txtData02"
        Me.txtData02.Size = New System.Drawing.Size(61, 20)
        Me.txtData02.TabIndex = 22
        '
        'txtData01
        '
        Me.txtData01.BackColor = System.Drawing.Color.White
        Me.txtData01.Location = New System.Drawing.Point(68, 38)
        Me.txtData01.MaxLength = 8
        Me.txtData01.Name = "txtData01"
        Me.txtData01.Size = New System.Drawing.Size(61, 20)
        Me.txtData01.TabIndex = 18
        '
        'Label3
        '
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Location = New System.Drawing.Point(59, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "Data:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label2.Location = New System.Drawing.Point(-9, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 16)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Mod:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtMod01
        '
        Me.txtMod01.BackColor = System.Drawing.Color.White
        Me.txtMod01.Location = New System.Drawing.Point(0, 38)
        Me.txtMod01.Name = "txtMod01"
        Me.txtMod01.Size = New System.Drawing.Size(60, 20)
        Me.txtMod01.TabIndex = 17
        '
        'txtMod02
        '
        Me.txtMod02.BackColor = System.Drawing.Color.White
        Me.txtMod02.Location = New System.Drawing.Point(0, 62)
        Me.txtMod02.Name = "txtMod02"
        Me.txtMod02.Size = New System.Drawing.Size(60, 20)
        Me.txtMod02.TabIndex = 21
        '
        'txtMod03
        '
        Me.txtMod03.BackColor = System.Drawing.Color.White
        Me.txtMod03.Location = New System.Drawing.Point(0, 86)
        Me.txtMod03.Name = "txtMod03"
        Me.txtMod03.Size = New System.Drawing.Size(60, 20)
        Me.txtMod03.TabIndex = 25
        '
        'txtMod04
        '
        Me.txtMod04.BackColor = System.Drawing.Color.White
        Me.txtMod04.Location = New System.Drawing.Point(283, 38)
        Me.txtMod04.Name = "txtMod04"
        Me.txtMod04.Size = New System.Drawing.Size(60, 20)
        Me.txtMod04.TabIndex = 29
        '
        'txtMod05
        '
        Me.txtMod05.BackColor = System.Drawing.Color.White
        Me.txtMod05.Location = New System.Drawing.Point(283, 62)
        Me.txtMod05.Name = "txtMod05"
        Me.txtMod05.Size = New System.Drawing.Size(60, 20)
        Me.txtMod05.TabIndex = 33
        '
        'txtMod06
        '
        Me.txtMod06.BackColor = System.Drawing.Color.White
        Me.txtMod06.Location = New System.Drawing.Point(283, 86)
        Me.txtMod06.Name = "txtMod06"
        Me.txtMod06.Size = New System.Drawing.Size(60, 20)
        Me.txtMod06.TabIndex = 37
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.txtAut05)
        Me.GroupBox2.Controls.Add(Me.txtAut04)
        Me.GroupBox2.Controls.Add(Me.txtAut03)
        Me.GroupBox2.Controls.Add(Me.txtAut02)
        Me.GroupBox2.Controls.Add(Me.txtAut01)
        Me.GroupBox2.Controls.Add(Me.txtAut06)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.txtMod02)
        Me.GroupBox2.Controls.Add(Me.txtData06)
        Me.GroupBox2.Controls.Add(Me.txtData04)
        Me.GroupBox2.Controls.Add(Me.txtData02)
        Me.GroupBox2.Controls.Add(Me.txtMod04)
        Me.GroupBox2.Controls.Add(Me.Label43)
        Me.GroupBox2.Controls.Add(Me.txtMod03)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtData05)
        Me.GroupBox2.Controls.Add(Me.txtMod05)
        Me.GroupBox2.Controls.Add(Me.Label48)
        Me.GroupBox2.Controls.Add(Me.Label47)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtData03)
        Me.GroupBox2.Controls.Add(Me.txtData01)
        Me.GroupBox2.Controls.Add(Me.txtMod06)
        Me.GroupBox2.Controls.Add(Me.txtMod01)
        Me.GroupBox2.Location = New System.Drawing.Point(11, 310)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(544, 113)
        Me.GroupBox2.TabIndex = 25
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Gestione Modifiche"
        '
        'txtAut05
        '
        Me.txtAut05.BackColor = System.Drawing.Color.White
        Me.txtAut05.Location = New System.Drawing.Point(422, 62)
        Me.txtAut05.Name = "txtAut05"
        Me.txtAut05.Size = New System.Drawing.Size(119, 20)
        Me.txtAut05.TabIndex = 35
        '
        'txtAut04
        '
        Me.txtAut04.BackColor = System.Drawing.Color.White
        Me.txtAut04.Location = New System.Drawing.Point(422, 38)
        Me.txtAut04.Name = "txtAut04"
        Me.txtAut04.Size = New System.Drawing.Size(119, 20)
        Me.txtAut04.TabIndex = 31
        '
        'txtAut03
        '
        Me.txtAut03.BackColor = System.Drawing.Color.White
        Me.txtAut03.Location = New System.Drawing.Point(141, 86)
        Me.txtAut03.Name = "txtAut03"
        Me.txtAut03.Size = New System.Drawing.Size(131, 20)
        Me.txtAut03.TabIndex = 27
        '
        'txtAut02
        '
        Me.txtAut02.BackColor = System.Drawing.Color.White
        Me.txtAut02.Location = New System.Drawing.Point(141, 62)
        Me.txtAut02.Name = "txtAut02"
        Me.txtAut02.Size = New System.Drawing.Size(131, 20)
        Me.txtAut02.TabIndex = 23
        '
        'txtAut01
        '
        Me.txtAut01.BackColor = System.Drawing.Color.White
        Me.txtAut01.Location = New System.Drawing.Point(141, 38)
        Me.txtAut01.Name = "txtAut01"
        Me.txtAut01.Size = New System.Drawing.Size(131, 20)
        Me.txtAut01.TabIndex = 19
        '
        'txtAut06
        '
        Me.txtAut06.BackColor = System.Drawing.Color.White
        Me.txtAut06.Location = New System.Drawing.Point(422, 86)
        Me.txtAut06.Name = "txtAut06"
        Me.txtAut06.Size = New System.Drawing.Size(119, 20)
        Me.txtAut06.TabIndex = 39
        '
        'Label22
        '
        Me.Label22.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label22.Location = New System.Drawing.Point(274, 22)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(48, 16)
        Me.Label22.TabIndex = 61
        Me.Label22.Text = "Mod:"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label25
        '
        Me.Label25.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label25.Location = New System.Drawing.Point(342, 22)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(48, 16)
        Me.Label25.TabIndex = 62
        Me.Label25.Text = "Data:"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.Color.Silver
        Me.Label43.Location = New System.Drawing.Point(494, 21)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(38, 13)
        Me.Label43.TabIndex = 290
        Me.Label43.Text = "DinoM"
        '
        'Label48
        '
        Me.Label48.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label48.Location = New System.Drawing.Point(413, 22)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(48, 16)
        Me.Label48.TabIndex = 43
        Me.Label48.Text = "Autore:"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label47
        '
        Me.Label47.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label47.Location = New System.Drawing.Point(137, 22)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(48, 16)
        Me.Label47.TabIndex = 43
        Me.Label47.Text = "Autore:"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSpessore
        '
        Me.txtSpessore.BackColor = System.Drawing.Color.White
        Me.txtSpessore.Location = New System.Drawing.Point(1175, 96)
        Me.txtSpessore.Name = "txtSpessore"
        Me.txtSpessore.Size = New System.Drawing.Size(80, 20)
        Me.txtSpessore.TabIndex = 291
        Me.txtSpessore.Visible = False
        '
        'cmdSlitta
        '
        Me.cmdSlitta.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmdSlitta.Location = New System.Drawing.Point(253, 428)
        Me.cmdSlitta.Name = "cmdSlitta"
        Me.cmdSlitta.Size = New System.Drawing.Size(80, 49)
        Me.cmdSlitta.TabIndex = 59
        Me.cmdSlitta.Text = "Slitta in alto Revisioni"
        Me.cmdSlitta.UseVisualStyleBackColor = False
        '
        'lblCaratteriDescrizione
        '
        Me.lblCaratteriDescrizione.ForeColor = System.Drawing.Color.Black
        Me.lblCaratteriDescrizione.Location = New System.Drawing.Point(368, 17)
        Me.lblCaratteriDescrizione.Name = "lblCaratteriDescrizione"
        Me.lblCaratteriDescrizione.Size = New System.Drawing.Size(32, 16)
        Me.lblCaratteriDescrizione.TabIndex = 241
        Me.lblCaratteriDescrizione.Text = "31"
        Me.lblCaratteriDescrizione.Visible = False
        '
        'lblCaratteriCodice
        '
        Me.lblCaratteriCodice.ForeColor = System.Drawing.Color.Black
        Me.lblCaratteriCodice.Location = New System.Drawing.Point(82, 17)
        Me.lblCaratteriCodice.Name = "lblCaratteriCodice"
        Me.lblCaratteriCodice.Size = New System.Drawing.Size(32, 16)
        Me.lblCaratteriCodice.TabIndex = 242
        Me.lblCaratteriCodice.Text = "17"
        Me.lblCaratteriCodice.Visible = False
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label13.Enabled = False
        Me.Label13.Location = New System.Drawing.Point(1175, 49)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(40, 16)
        Me.Label13.TabIndex = 48
        Me.Label13.Text = "Scala:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.Label13.Visible = False
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label19.Location = New System.Drawing.Point(431, 58)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(80, 16)
        Me.Label19.TabIndex = 54
        Me.Label19.Text = "Semilavorato:"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label37.Location = New System.Drawing.Point(11, 58)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(80, 16)
        Me.Label37.TabIndex = 225
        Me.Label37.Text = "Sostituisce:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label39
        '
        Me.Label39.BackColor = System.Drawing.Color.Transparent
        Me.Label39.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label39.Location = New System.Drawing.Point(149, 58)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(80, 16)
        Me.Label39.TabIndex = 229
        Me.Label39.Text = "Derivato:"
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'cboSpessore
        '
        Me.cboSpessore.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboSpessore.BackColor = System.Drawing.Color.White
        Me.cboSpessore.Location = New System.Drawing.Point(11, 283)
        Me.cboSpessore.Name = "cboSpessore"
        Me.cboSpessore.Size = New System.Drawing.Size(129, 21)
        Me.cboSpessore.TabIndex = 21
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label42.Location = New System.Drawing.Point(11, 268)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(115, 15)
        Me.Label42.TabIndex = 284
        Me.Label42.Text = "Spessore / Diametro:"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'cboMateriale
        '
        Me.cboMateriale.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboMateriale.BackColor = System.Drawing.Color.White
        Me.cboMateriale.Enabled = False
        Me.cboMateriale.Location = New System.Drawing.Point(11, 202)
        Me.cboMateriale.Name = "cboMateriale"
        Me.cboMateriale.Size = New System.Drawing.Size(271, 21)
        Me.cboMateriale.Sorted = True
        Me.cboMateriale.TabIndex = 14
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label12.Location = New System.Drawing.Point(12, 183)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 16)
        Me.Label12.TabIndex = 282
        Me.Label12.Text = "Materiale:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'cmdReset
        '
        Me.cmdReset.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmdReset.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdReset.Location = New System.Drawing.Point(339, 428)
        Me.cmdReset.Name = "cmdReset"
        Me.cmdReset.Size = New System.Drawing.Size(98, 49)
        Me.cmdReset.TabIndex = 59
        Me.cmdReset.Text = "Reset"
        Me.cmdReset.UseVisualStyleBackColor = False
        '
        'cboCodifica
        '
        Me.cboCodifica.FormattingEnabled = True
        Me.cboCodifica.Location = New System.Drawing.Point(151, 156)
        Me.cboCodifica.Name = "cboCodifica"
        Me.cboCodifica.Size = New System.Drawing.Size(129, 21)
        Me.cboCodifica.Sorted = True
        Me.cboCodifica.TabIndex = 12
        '
        'cboDescrizione
        '
        Me.cboDescrizione.Enabled = False
        Me.cboDescrizione.FormattingEnabled = True
        Me.cboDescrizione.Location = New System.Drawing.Point(293, 156)
        Me.cboDescrizione.Name = "cboDescrizione"
        Me.cboDescrizione.Size = New System.Drawing.Size(258, 21)
        Me.cboDescrizione.Sorted = True
        Me.cboDescrizione.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.Location = New System.Drawing.Point(293, 140)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(188, 16)
        Me.Label8.TabIndex = 287
        Me.Label8.Text = "Descrizione Categoria Tecnologica:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Location = New System.Drawing.Point(293, 58)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 16)
        Me.Label9.TabIndex = 225
        Me.Label9.Text = "Destinazione:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'txtDestinazione
        '
        Me.txtDestinazione.BackColor = System.Drawing.Color.White
        Me.txtDestinazione.Location = New System.Drawing.Point(293, 74)
        Me.txtDestinazione.Name = "txtDestinazione"
        Me.txtDestinazione.Size = New System.Drawing.Size(129, 20)
        Me.txtDestinazione.TabIndex = 5
        '
        'txtDerivato
        '
        Me.txtDerivato.Location = New System.Drawing.Point(152, 74)
        Me.txtDerivato.Name = "txtDerivato"
        Me.txtDerivato.Size = New System.Drawing.Size(131, 20)
        Me.txtDerivato.TabIndex = 4
        '
        'cboProduttore
        '
        Me.cboProduttore.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboProduttore.Enabled = False
        Me.cboProduttore.FormattingEnabled = True
        Me.cboProduttore.Location = New System.Drawing.Point(294, 241)
        Me.cboProduttore.Name = "cboProduttore"
        Me.cboProduttore.Size = New System.Drawing.Size(128, 21)
        Me.cboProduttore.Sorted = True
        Me.cboProduttore.TabIndex = 19
        Me.cboProduttore.Tag = "IDProduttore"
        '
        'cboAbbreviazione
        '
        Me.cboAbbreviazione.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAbbreviazione.Enabled = False
        Me.cboAbbreviazione.FormattingEnabled = True
        Me.cboAbbreviazione.Location = New System.Drawing.Point(431, 202)
        Me.cboAbbreviazione.Name = "cboAbbreviazione"
        Me.cboAbbreviazione.Size = New System.Drawing.Size(120, 21)
        Me.cboAbbreviazione.Sorted = True
        Me.cboAbbreviazione.TabIndex = 16
        Me.cboAbbreviazione.Tag = "Abbreviazione"
        '
        'cboRating
        '
        Me.cboRating.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRating.Enabled = False
        Me.cboRating.FormattingEnabled = True
        Me.cboRating.Location = New System.Drawing.Point(431, 283)
        Me.cboRating.Name = "cboRating"
        Me.cboRating.Size = New System.Drawing.Size(120, 21)
        Me.cboRating.Sorted = True
        Me.cboRating.TabIndex = 24
        Me.cboRating.Tag = "FlamAt"
        '
        'cboPNMateriale
        '
        Me.cboPNMateriale.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboPNMateriale.Enabled = False
        Me.cboPNMateriale.FormattingEnabled = True
        Me.cboPNMateriale.Location = New System.Drawing.Point(293, 202)
        Me.cboPNMateriale.Name = "cboPNMateriale"
        Me.cboPNMateriale.Size = New System.Drawing.Size(129, 21)
        Me.cboPNMateriale.Sorted = True
        Me.cboPNMateriale.TabIndex = 15
        Me.cboPNMateriale.Tag = "PartNumber"
        '
        'cboFlame
        '
        Me.cboFlame.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFlame.Enabled = False
        Me.cboFlame.FormattingEnabled = True
        Me.cboFlame.Location = New System.Drawing.Point(152, 283)
        Me.cboFlame.Name = "cboFlame"
        Me.cboFlame.Size = New System.Drawing.Size(130, 21)
        Me.cboFlame.Sorted = True
        Me.cboFlame.TabIndex = 22
        Me.cboFlame.Tag = "FlamV"
        '
        'cboFileUL
        '
        Me.cboFileUL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFileUL.Enabled = False
        Me.cboFileUL.FormattingEnabled = True
        Me.cboFileUL.Location = New System.Drawing.Point(293, 283)
        Me.cboFileUL.Name = "cboFileUL"
        Me.cboFileUL.Size = New System.Drawing.Size(129, 21)
        Me.cboFileUL.Sorted = True
        Me.cboFileUL.TabIndex = 23
        Me.cboFileUL.Tag = "FlamFile"
        '
        'cboColore
        '
        Me.cboColore.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboColore.Enabled = False
        Me.cboColore.FormattingEnabled = True
        Me.cboColore.Location = New System.Drawing.Point(431, 241)
        Me.cboColore.Name = "cboColore"
        Me.cboColore.Size = New System.Drawing.Size(120, 21)
        Me.cboColore.Sorted = True
        Me.cboColore.TabIndex = 20
        Me.cboColore.Tag = "IDColore"
        '
        'Label112
        '
        Me.Label112.AutoSize = True
        Me.Label112.ForeColor = System.Drawing.Color.Black
        Me.Label112.Location = New System.Drawing.Point(290, 268)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(43, 13)
        Me.Label112.TabIndex = 298
        Me.Label112.Text = "File UL:"
        Me.Label112.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label139
        '
        Me.Label139.AutoSize = True
        Me.Label139.ForeColor = System.Drawing.Color.Black
        Me.Label139.Location = New System.Drawing.Point(428, 268)
        Me.Label139.Name = "Label139"
        Me.Label139.Size = New System.Drawing.Size(53, 13)
        Me.Label139.TabIndex = 297
        Me.Label139.Text = "Rating at:"
        Me.Label139.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label140
        '
        Me.Label140.AutoSize = True
        Me.Label140.ForeColor = System.Drawing.Color.Black
        Me.Label140.Location = New System.Drawing.Point(149, 268)
        Me.Label140.Name = "Label140"
        Me.Label140.Size = New System.Drawing.Size(48, 13)
        Me.Label140.TabIndex = 296
        Me.Label140.Text = "Flame V:"
        Me.Label140.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label141
        '
        Me.Label141.AutoSize = True
        Me.Label141.ForeColor = System.Drawing.Color.Black
        Me.Label141.Location = New System.Drawing.Point(428, 187)
        Me.Label141.Name = "Label141"
        Me.Label141.Size = New System.Drawing.Size(77, 13)
        Me.Label141.TabIndex = 295
        Me.Label141.Text = "Abbreviazione:"
        Me.Label141.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label142
        '
        Me.Label142.AutoSize = True
        Me.Label142.ForeColor = System.Drawing.Color.Black
        Me.Label142.Location = New System.Drawing.Point(428, 226)
        Me.Label142.Name = "Label142"
        Me.Label142.Size = New System.Drawing.Size(40, 13)
        Me.Label142.TabIndex = 294
        Me.Label142.Text = "Colore:"
        Me.Label142.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label143
        '
        Me.Label143.AutoSize = True
        Me.Label143.ForeColor = System.Drawing.Color.Black
        Me.Label143.Location = New System.Drawing.Point(293, 226)
        Me.Label143.Name = "Label143"
        Me.Label143.Size = New System.Drawing.Size(59, 13)
        Me.Label143.TabIndex = 293
        Me.Label143.Text = "Produttore:"
        Me.Label143.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label144
        '
        Me.Label144.AutoSize = True
        Me.Label144.ForeColor = System.Drawing.Color.Black
        Me.Label144.Location = New System.Drawing.Point(290, 187)
        Me.Label144.Name = "Label144"
        Me.Label144.Size = New System.Drawing.Size(115, 13)
        Me.Label144.TabIndex = 292
        Me.Label144.Text = "Part Number Materiale:"
        Me.Label144.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboFamiglia
        '
        Me.cboFamiglia.FormattingEnabled = True
        Me.cboFamiglia.Location = New System.Drawing.Point(11, 156)
        Me.cboFamiglia.Name = "cboFamiglia"
        Me.cboFamiglia.Size = New System.Drawing.Size(129, 21)
        Me.cboFamiglia.Sorted = True
        Me.cboFamiglia.TabIndex = 11
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label27.Location = New System.Drawing.Point(12, 137)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(135, 16)
        Me.Label27.TabIndex = 312
        Me.Label27.Text = "Famiglia Tecnologica:"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'cboTTermico
        '
        Me.cboTTermico.BackColor = System.Drawing.Color.White
        Me.cboTTermico.ItemHeight = 13
        Me.cboTTermico.Location = New System.Drawing.Point(151, 242)
        Me.cboTTermico.Name = "cboTTermico"
        Me.cboTTermico.Size = New System.Drawing.Size(130, 21)
        Me.cboTTermico.Sorted = True
        Me.cboTTermico.TabIndex = 18
        '
        'pnlTorneria
        '
        Me.pnlTorneria.AutoScroll = True
        Me.pnlTorneria.Controls.Add(Me.chkTorCAQ)
        Me.pnlTorneria.Controls.Add(Me.cboTorCHS)
        Me.pnlTorneria.Controls.Add(Me.chkTorCHS)
        Me.pnlTorneria.Controls.Add(Me.cboTorCHN)
        Me.pnlTorneria.Controls.Add(Me.chkTorCHN)
        Me.pnlTorneria.Controls.Add(Me.PictureBox74)
        Me.pnlTorneria.Controls.Add(Me.cboTorEES)
        Me.pnlTorneria.Controls.Add(Me.Label26)
        Me.pnlTorneria.Controls.Add(Me.chkTorEES)
        Me.pnlTorneria.Controls.Add(Me.Label32)
        Me.pnlTorneria.Controls.Add(Me.chkTorEEC)
        Me.pnlTorneria.Controls.Add(Me.chkTorEER)
        Me.pnlTorneria.Controls.Add(Me.PictureBox75)
        Me.pnlTorneria.Controls.Add(Me.PictureBox77)
        Me.pnlTorneria.Controls.Add(Me.PictureBox78)
        Me.pnlTorneria.Controls.Add(Me.PictureBox83)
        Me.pnlTorneria.Controls.Add(Me.PictureBox70)
        Me.pnlTorneria.Controls.Add(Me.cboTorIES)
        Me.pnlTorneria.Controls.Add(Me.Label14)
        Me.pnlTorneria.Controls.Add(Me.chkTorIES)
        Me.pnlTorneria.Controls.Add(Me.Label7)
        Me.pnlTorneria.Controls.Add(Me.chkTorIEC)
        Me.pnlTorneria.Controls.Add(Me.chkTorIER)
        Me.pnlTorneria.Controls.Add(Me.PictureBox66)
        Me.pnlTorneria.Controls.Add(Me.PictureBox64)
        Me.pnlTorneria.Controls.Add(Me.PictureBox61)
        Me.pnlTorneria.Controls.Add(Me.PictureBox57)
        Me.pnlTorneria.Controls.Add(Me.Label6)
        Me.pnlTorneria.Controls.Add(Me.PictureBox59)
        Me.pnlTorneria.Controls.Add(Me.Label45)
        Me.pnlTorneria.Controls.Add(Me.PictureBox6)
        Me.pnlTorneria.Controls.Add(Me.chkTorDNI)
        Me.pnlTorneria.Controls.Add(Me.txtTorH)
        Me.pnlTorneria.Controls.Add(Me.txtTorNIB)
        Me.pnlTorneria.Controls.Add(Me.PictureBox4)
        Me.pnlTorneria.Controls.Add(Me.PictureBox3)
        Me.pnlTorneria.Controls.Add(Me.PictureBox1)
        Me.pnlTorneria.Controls.Add(Me.Label41)
        Me.pnlTorneria.Controls.Add(Me.Label31)
        Me.pnlTorneria.Controls.Add(Me.Label30)
        Me.pnlTorneria.Controls.Add(Me.Label29)
        Me.pnlTorneria.Controls.Add(Me.Label28)
        Me.pnlTorneria.Controls.Add(Me.Label20)
        Me.pnlTorneria.Controls.Add(Me.chkTorCHB)
        Me.pnlTorneria.Controls.Add(Me.chkTorCHR)
        Me.pnlTorneria.Controls.Add(Me.chkTorDCO)
        Me.pnlTorneria.Controls.Add(Me.ChkTorDCL)
        Me.pnlTorneria.Controls.Add(Me.chkTorDCA)
        Me.pnlTorneria.Controls.Add(Me.chkTorEFN)
        Me.pnlTorneria.Controls.Add(Me.chkTorCLE)
        Me.pnlTorneria.Controls.Add(Me.chkTorEFC)
        Me.pnlTorneria.Location = New System.Drawing.Point(1151, 299)
        Me.pnlTorneria.Name = "pnlTorneria"
        Me.pnlTorneria.Size = New System.Drawing.Size(546, 514)
        Me.pnlTorneria.TabIndex = 314
        Me.pnlTorneria.Visible = False
        '
        'chkTorCAQ
        '
        Me.chkTorCAQ.Checked = True
        Me.chkTorCAQ.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorCAQ.Location = New System.Drawing.Point(170, 451)
        Me.chkTorCAQ.Name = "chkTorCAQ"
        Me.chkTorCAQ.Size = New System.Drawing.Size(243, 17)
        Me.chkTorCAQ.TabIndex = 424
        Me.chkTorCAQ.Text = "ARCA 'RT0041' COSMETIC QUALITY"
        Me.chkTorCAQ.UseVisualStyleBackColor = True
        '
        'cboTorCHS
        '
        Me.cboTorCHS.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboTorCHS.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTorCHS.Enabled = False
        Me.cboTorCHS.FormattingEnabled = True
        Me.cboTorCHS.Items.AddRange(New Object() {"4G", "4H", "5G", "5H", "6G", "6H", "7G", "7H", "8G", "8H"})
        Me.cboTorCHS.Location = New System.Drawing.Point(355, 285)
        Me.cboTorCHS.Name = "cboTorCHS"
        Me.cboTorCHS.Size = New System.Drawing.Size(47, 21)
        Me.cboTorCHS.Sorted = True
        Me.cboTorCHS.TabIndex = 423
        '
        'chkTorCHS
        '
        Me.chkTorCHS.Checked = True
        Me.chkTorCHS.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorCHS.ForeColor = System.Drawing.Color.Black
        Me.chkTorCHS.Location = New System.Drawing.Point(170, 289)
        Me.chkTorCHS.Name = "chkTorCHS"
        Me.chkTorCHS.Size = New System.Drawing.Size(190, 17)
        Me.chkTorCHS.TabIndex = 422
        Me.chkTorCHS.Text = "SCREW TOLLERANCE GRADE:"
        Me.chkTorCHS.UseVisualStyleBackColor = True
        '
        'cboTorCHN
        '
        Me.cboTorCHN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboTorCHN.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTorCHN.Enabled = False
        Me.cboTorCHN.FormattingEnabled = True
        Me.cboTorCHN.Items.AddRange(New Object() {"4G", "4H", "5G", "5H", "6G", "6H", "7G", "7H", "8G", "8H"})
        Me.cboTorCHN.Location = New System.Drawing.Point(386, 309)
        Me.cboTorCHN.Name = "cboTorCHN"
        Me.cboTorCHN.Size = New System.Drawing.Size(47, 21)
        Me.cboTorCHN.Sorted = True
        Me.cboTorCHN.TabIndex = 421
        '
        'chkTorCHN
        '
        Me.chkTorCHN.Checked = True
        Me.chkTorCHN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorCHN.ForeColor = System.Drawing.Color.Black
        Me.chkTorCHN.Location = New System.Drawing.Point(170, 313)
        Me.chkTorCHN.Name = "chkTorCHN"
        Me.chkTorCHN.Size = New System.Drawing.Size(225, 17)
        Me.chkTorCHN.TabIndex = 420
        Me.chkTorCHN.Text = "NUTSCREW TOLLERANCE GRADE:"
        Me.chkTorCHN.UseVisualStyleBackColor = True
        '
        'PictureBox74
        '
        Me.PictureBox74.BackColor = System.Drawing.Color.Black
        Me.PictureBox74.Location = New System.Drawing.Point(325, 163)
        Me.PictureBox74.Name = "PictureBox74"
        Me.PictureBox74.Size = New System.Drawing.Size(1, 55)
        Me.PictureBox74.TabIndex = 419
        Me.PictureBox74.TabStop = False
        '
        'cboTorEES
        '
        Me.cboTorEES.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboTorEES.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTorEES.Enabled = False
        Me.cboTorEES.FormattingEnabled = True
        Me.cboTorEES.Items.AddRange(New Object() {"0,10", "0,15", "0,20", "0,25", "0,30", "0,35", "0,40", "0,40", "0,45", "0,50"})
        Me.cboTorEES.Location = New System.Drawing.Point(252, 165)
        Me.cboTorEES.Name = "cboTorEES"
        Me.cboTorEES.Size = New System.Drawing.Size(63, 21)
        Me.cboTorEES.Sorted = True
        Me.cboTorEES.TabIndex = 418
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(3, 170)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(100, 13)
        Me.Label26.TabIndex = 417
        Me.Label26.Text = "EDGE SHAPE"
        '
        'chkTorEES
        '
        Me.chkTorEES.Checked = True
        Me.chkTorEES.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorEES.ForeColor = System.Drawing.Color.Black
        Me.chkTorEES.Location = New System.Drawing.Point(170, 167)
        Me.chkTorEES.Name = "chkTorEES"
        Me.chkTorEES.Size = New System.Drawing.Size(89, 17)
        Me.chkTorEES.TabIndex = 416
        Me.chkTorEES.Text = "EXTERNAL"
        Me.chkTorEES.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(383, 221)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(58, 13)
        Me.Label32.TabIndex = 415
        Me.Label32.Text = "ISO 13715"
        '
        'chkTorEEC
        '
        Me.chkTorEEC.Checked = True
        Me.chkTorEEC.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorEEC.Enabled = False
        Me.chkTorEEC.ForeColor = System.Drawing.Color.Black
        Me.chkTorEEC.Location = New System.Drawing.Point(342, 195)
        Me.chkTorEEC.Name = "chkTorEEC"
        Me.chkTorEEC.Size = New System.Drawing.Size(104, 17)
        Me.chkTorEEC.TabIndex = 414
        Me.chkTorEEC.Text = "CHAMFER x45�"
        Me.chkTorEEC.UseVisualStyleBackColor = True
        '
        'chkTorEER
        '
        Me.chkTorEER.Checked = True
        Me.chkTorEER.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorEER.Enabled = False
        Me.chkTorEER.ForeColor = System.Drawing.Color.Black
        Me.chkTorEER.Location = New System.Drawing.Point(342, 167)
        Me.chkTorEER.Name = "chkTorEER"
        Me.chkTorEER.Size = New System.Drawing.Size(70, 17)
        Me.chkTorEER.TabIndex = 413
        Me.chkTorEER.Text = "ROUND"
        Me.chkTorEER.UseVisualStyleBackColor = True
        '
        'PictureBox75
        '
        Me.PictureBox75.BackColor = System.Drawing.Color.Black
        Me.PictureBox75.Location = New System.Drawing.Point(325, 217)
        Me.PictureBox75.Name = "PictureBox75"
        Me.PictureBox75.Size = New System.Drawing.Size(190, 1)
        Me.PictureBox75.TabIndex = 412
        Me.PictureBox75.TabStop = False
        '
        'PictureBox77
        '
        Me.PictureBox77.BackColor = System.Drawing.Color.Black
        Me.PictureBox77.Location = New System.Drawing.Point(325, 189)
        Me.PictureBox77.Name = "PictureBox77"
        Me.PictureBox77.Size = New System.Drawing.Size(190, 1)
        Me.PictureBox77.TabIndex = 411
        Me.PictureBox77.TabStop = False
        '
        'PictureBox78
        '
        Me.PictureBox78.BackColor = System.Drawing.Color.Black
        Me.PictureBox78.Location = New System.Drawing.Point(0, 239)
        Me.PictureBox78.Name = "PictureBox78"
        Me.PictureBox78.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox78.TabIndex = 410
        Me.PictureBox78.TabStop = False
        '
        'PictureBox83
        '
        Me.PictureBox83.Image = CType(resources.GetObject("PictureBox83.Image"), System.Drawing.Image)
        Me.PictureBox83.Location = New System.Drawing.Point(215, 187)
        Me.PictureBox83.Name = "PictureBox83"
        Me.PictureBox83.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox83.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox83.TabIndex = 409
        Me.PictureBox83.TabStop = False
        '
        'PictureBox70
        '
        Me.PictureBox70.BackColor = System.Drawing.Color.Black
        Me.PictureBox70.Location = New System.Drawing.Point(325, 86)
        Me.PictureBox70.Name = "PictureBox70"
        Me.PictureBox70.Size = New System.Drawing.Size(1, 55)
        Me.PictureBox70.TabIndex = 408
        Me.PictureBox70.TabStop = False
        '
        'cboTorIES
        '
        Me.cboTorIES.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboTorIES.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboTorIES.Enabled = False
        Me.cboTorIES.FormattingEnabled = True
        Me.cboTorIES.Items.AddRange(New Object() {"0,10", "0,15", "0,20", "0,25", "0,30", "0,35", "0,40", "0,40", "0,45", "0,50"})
        Me.cboTorIES.Location = New System.Drawing.Point(252, 88)
        Me.cboTorIES.Name = "cboTorIES"
        Me.cboTorIES.Size = New System.Drawing.Size(63, 21)
        Me.cboTorIES.Sorted = True
        Me.cboTorIES.TabIndex = 407
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(3, 93)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(100, 13)
        Me.Label14.TabIndex = 406
        Me.Label14.Text = "EDGE SHAPE"
        '
        'chkTorIES
        '
        Me.chkTorIES.Checked = True
        Me.chkTorIES.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorIES.ForeColor = System.Drawing.Color.Black
        Me.chkTorIES.Location = New System.Drawing.Point(170, 90)
        Me.chkTorIES.Name = "chkTorIES"
        Me.chkTorIES.Size = New System.Drawing.Size(89, 17)
        Me.chkTorIES.TabIndex = 405
        Me.chkTorIES.Text = "INTERNAL"
        Me.chkTorIES.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(383, 144)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 404
        Me.Label7.Text = "ISO 13715"
        '
        'chkTorIEC
        '
        Me.chkTorIEC.Checked = True
        Me.chkTorIEC.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorIEC.Enabled = False
        Me.chkTorIEC.ForeColor = System.Drawing.Color.Black
        Me.chkTorIEC.Location = New System.Drawing.Point(342, 118)
        Me.chkTorIEC.Name = "chkTorIEC"
        Me.chkTorIEC.Size = New System.Drawing.Size(104, 17)
        Me.chkTorIEC.TabIndex = 403
        Me.chkTorIEC.Text = "CHAMFER x45�"
        Me.chkTorIEC.UseVisualStyleBackColor = True
        '
        'chkTorIER
        '
        Me.chkTorIER.Checked = True
        Me.chkTorIER.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorIER.Enabled = False
        Me.chkTorIER.ForeColor = System.Drawing.Color.Black
        Me.chkTorIER.Location = New System.Drawing.Point(342, 90)
        Me.chkTorIER.Name = "chkTorIER"
        Me.chkTorIER.Size = New System.Drawing.Size(70, 17)
        Me.chkTorIER.TabIndex = 402
        Me.chkTorIER.Text = "ROUND"
        Me.chkTorIER.UseVisualStyleBackColor = True
        '
        'PictureBox66
        '
        Me.PictureBox66.BackColor = System.Drawing.Color.Black
        Me.PictureBox66.Location = New System.Drawing.Point(325, 140)
        Me.PictureBox66.Name = "PictureBox66"
        Me.PictureBox66.Size = New System.Drawing.Size(190, 1)
        Me.PictureBox66.TabIndex = 401
        Me.PictureBox66.TabStop = False
        '
        'PictureBox64
        '
        Me.PictureBox64.BackColor = System.Drawing.Color.Black
        Me.PictureBox64.Location = New System.Drawing.Point(325, 112)
        Me.PictureBox64.Name = "PictureBox64"
        Me.PictureBox64.Size = New System.Drawing.Size(190, 1)
        Me.PictureBox64.TabIndex = 400
        Me.PictureBox64.TabStop = False
        '
        'PictureBox61
        '
        Me.PictureBox61.BackColor = System.Drawing.Color.Black
        Me.PictureBox61.Location = New System.Drawing.Point(0, 162)
        Me.PictureBox61.Name = "PictureBox61"
        Me.PictureBox61.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox61.TabIndex = 399
        Me.PictureBox61.TabStop = False
        '
        'PictureBox57
        '
        Me.PictureBox57.Image = CType(resources.GetObject("PictureBox57.Image"), System.Drawing.Image)
        Me.PictureBox57.Location = New System.Drawing.Point(215, 110)
        Me.PictureBox57.Name = "PictureBox57"
        Me.PictureBox57.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox57.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox57.TabIndex = 398
        Me.PictureBox57.TabStop = False
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(188, 432)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(282, 13)
        Me.Label6.TabIndex = 397
        Me.Label6.Text = "NOTCH AND SCRATCH NICK NOT PERMITTED"
        '
        'PictureBox59
        '
        Me.PictureBox59.BackColor = System.Drawing.Color.Black
        Me.PictureBox59.Location = New System.Drawing.Point(1, 402)
        Me.PictureBox59.Name = "PictureBox59"
        Me.PictureBox59.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox59.TabIndex = 364
        Me.PictureBox59.TabStop = False
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.Black
        Me.Label45.Location = New System.Drawing.Point(6, 5)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(222, 20)
        Me.Label45.TabIndex = 356
        Me.Label45.Text = "TURNING WORKING SPECS"
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Black
        Me.PictureBox6.Location = New System.Drawing.Point(0, 331)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox6.TabIndex = 353
        Me.PictureBox6.TabStop = False
        '
        'chkTorDNI
        '
        Me.chkTorDNI.Checked = True
        Me.chkTorDNI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTorDNI.Enabled = False
        Me.chkTorDNI.Location = New System.Drawing.Point(170, 336)
        Me.chkTorDNI.Name = "chkTorDNI"
        Me.chkTorDNI.Size = New System.Drawing.Size(202, 17)
        Me.chkTorDNI.TabIndex = 321
        Me.chkTorDNI.Text = "REFER TO THE 3D DRAWING"
        Me.chkTorDNI.UseVisualStyleBackColor = True
        '
        'txtTorH
        '
        Me.txtTorH.BackColor = System.Drawing.Color.White
        Me.txtTorH.Enabled = False
        Me.txtTorH.Location = New System.Drawing.Point(311, 65)
        Me.txtTorH.Name = "txtTorH"
        Me.txtTorH.Size = New System.Drawing.Size(36, 20)
        Me.txtTorH.TabIndex = 316
        '
        'txtTorNIB
        '
        Me.txtTorNIB.BackColor = System.Drawing.Color.White
        Me.txtTorNIB.Enabled = False
        Me.txtTorNIB.Location = New System.Drawing.Point(223, 65)
        Me.txtTorNIB.Name = "txtTorNIB"
        Me.txtTorNIB.Size = New System.Drawing.Size(36, 20)
        Me.txtTorNIB.TabIndex = 315
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Black
        Me.PictureBox4.Location = New System.Drawing.Point(0, 474)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox4.TabIndex = 336
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Black
        Me.PictureBox3.Location = New System.Drawing.Point(0, 356)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox3.TabIndex = 335
        Me.PictureBox3.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Black
        Me.PictureBox1.Location = New System.Drawing.Point(0, 86)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox1.TabIndex = 334
        Me.PictureBox1.TabStop = False
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(3, 482)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(85, 13)
        Me.Label41.TabIndex = 325
        Me.Label41.Text = "CLEANING"
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(3, 409)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(150, 37)
        Me.Label31.TabIndex = 327
        Me.Label31.Text = "HANDLING AND AESTHETIC DEFECT"
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(3, 362)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(150, 13)
        Me.Label30.TabIndex = 328
        Me.Label30.Text = "DIMENSION CHECKING"
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(3, 337)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(184, 13)
        Me.Label29.TabIndex = 329
        Me.Label29.Text = "DIMENSION NOT INDICATED"
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(3, 245)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(78, 13)
        Me.Label28.TabIndex = 331
        Me.Label28.Text = "TAPPING"
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(3, 45)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(100, 13)
        Me.Label20.TabIndex = 332
        Me.Label20.Text = "END FINISHING"
        '
        'chkTorCHB
        '
        Me.chkTorCHB.Checked = True
        Me.chkTorCHB.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorCHB.ForeColor = System.Drawing.Color.Black
        Me.chkTorCHB.Location = New System.Drawing.Point(170, 267)
        Me.chkTorCHB.Name = "chkTorCHB"
        Me.chkTorCHB.Size = New System.Drawing.Size(173, 17)
        Me.chkTorCHB.TabIndex = 318
        Me.chkTorCHB.Text = "BURR NOT PERMITTED"
        Me.chkTorCHB.UseVisualStyleBackColor = True
        '
        'chkTorCHR
        '
        Me.chkTorCHR.Checked = True
        Me.chkTorCHR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorCHR.ForeColor = System.Drawing.Color.Black
        Me.chkTorCHR.Location = New System.Drawing.Point(170, 244)
        Me.chkTorCHR.Name = "chkTorCHR"
        Me.chkTorCHR.Size = New System.Drawing.Size(136, 17)
        Me.chkTorCHR.TabIndex = 317
        Me.chkTorCHR.Text = "ROLL FORM TAP"
        Me.chkTorCHR.UseVisualStyleBackColor = True
        '
        'chkTorDCO
        '
        Me.chkTorDCO.AutoSize = True
        Me.chkTorDCO.Checked = True
        Me.chkTorDCO.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorDCO.ForeColor = System.Drawing.Color.Black
        Me.chkTorDCO.Location = New System.Drawing.Point(170, 410)
        Me.chkTorDCO.Name = "chkTorDCO"
        Me.chkTorDCO.Size = New System.Drawing.Size(334, 17)
        Me.chkTorDCO.TabIndex = 324
        Me.chkTorDCO.Text = "ON SURFACE IDENTIFIED WITH ""-- - --"" DEPRESSION, BURR"
        Me.chkTorDCO.UseVisualStyleBackColor = True
        '
        'ChkTorDCL
        '
        Me.ChkTorDCL.Checked = True
        Me.ChkTorDCL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkTorDCL.Enabled = False
        Me.ChkTorDCL.Location = New System.Drawing.Point(170, 384)
        Me.ChkTorDCL.Name = "ChkTorDCL"
        Me.ChkTorDCL.Size = New System.Drawing.Size(225, 17)
        Me.ChkTorDCL.TabIndex = 323
        Me.ChkTorDCL.Text = "LEAVE AT 20� � 5�C FOR 4 HOURS"
        Me.ChkTorDCL.UseVisualStyleBackColor = True
        '
        'chkTorDCA
        '
        Me.chkTorDCA.Checked = True
        Me.chkTorDCA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTorDCA.Enabled = False
        Me.chkTorDCA.Location = New System.Drawing.Point(170, 361)
        Me.chkTorDCA.Name = "chkTorDCA"
        Me.chkTorDCA.Size = New System.Drawing.Size(177, 17)
        Me.chkTorDCA.TabIndex = 322
        Me.chkTorDCA.Text = "ALL AFTER TREATMENT"
        Me.chkTorDCA.UseVisualStyleBackColor = True
        '
        'chkTorEFN
        '
        Me.chkTorEFN.Checked = True
        Me.chkTorEFN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorEFN.ForeColor = System.Drawing.Color.Black
        Me.chkTorEFN.Location = New System.Drawing.Point(170, 67)
        Me.chkTorEFN.Name = "chkTorEFN"
        Me.chkTorEFN.Size = New System.Drawing.Size(233, 17)
        Me.chkTorEFN.TabIndex = 314
        Me.chkTorEFN.Text = "NIB �              (mm)      H              (mm)"
        Me.chkTorEFN.UseVisualStyleBackColor = True
        '
        'chkTorCLE
        '
        Me.chkTorCLE.Checked = True
        Me.chkTorCLE.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTorCLE.Enabled = False
        Me.chkTorCLE.Location = New System.Drawing.Point(170, 479)
        Me.chkTorCLE.Name = "chkTorCLE"
        Me.chkTorCLE.Size = New System.Drawing.Size(243, 17)
        Me.chkTorCLE.TabIndex = 326
        Me.chkTorCLE.Text = "DIRT, OIL, GREASE NOT TOLERATED"
        Me.chkTorCLE.UseVisualStyleBackColor = True
        '
        'chkTorEFC
        '
        Me.chkTorEFC.Checked = True
        Me.chkTorEFC.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkTorEFC.ForeColor = System.Drawing.Color.Black
        Me.chkTorEFC.Location = New System.Drawing.Point(170, 44)
        Me.chkTorEFC.Name = "chkTorEFC"
        Me.chkTorEFC.Size = New System.Drawing.Size(208, 17)
        Me.chkTorEFC.TabIndex = 313
        Me.chkTorEFC.Text = "CUT OFF NIB NOT PERMITTED"
        Me.chkTorEFC.UseVisualStyleBackColor = True
        '
        'pnlLamiera
        '
        Me.pnlLamiera.AutoScroll = True
        Me.pnlLamiera.Controls.Add(Me.chkLamSCQ)
        Me.pnlLamiera.Controls.Add(Me.cboLamESL)
        Me.pnlLamiera.Controls.Add(Me.PictureBox92)
        Me.pnlLamiera.Controls.Add(Me.cboLamESH)
        Me.pnlLamiera.Controls.Add(Me.Label4)
        Me.pnlLamiera.Controls.Add(Me.chkLamBEC)
        Me.pnlLamiera.Controls.Add(Me.chkLamBER)
        Me.pnlLamiera.Controls.Add(Me.PictureBox93)
        Me.pnlLamiera.Controls.Add(Me.PictureBox94)
        Me.pnlLamiera.Controls.Add(Me.PictureBox96)
        Me.pnlLamiera.Controls.Add(Me.chkLamBHE)
        Me.pnlLamiera.Controls.Add(Me.Label131)
        Me.pnlLamiera.Controls.Add(Me.cboLamSFR)
        Me.pnlLamiera.Controls.Add(Me.Label82)
        Me.pnlLamiera.Controls.Add(Me.Label81)
        Me.pnlLamiera.Controls.Add(Me.Label80)
        Me.pnlLamiera.Controls.Add(Me.Label79)
        Me.pnlLamiera.Controls.Add(Me.Label78)
        Me.pnlLamiera.Controls.Add(Me.Label77)
        Me.pnlLamiera.Controls.Add(Me.Label73)
        Me.pnlLamiera.Controls.Add(Me.Label70)
        Me.pnlLamiera.Controls.Add(Me.Label69)
        Me.pnlLamiera.Controls.Add(Me.Label68)
        Me.pnlLamiera.Controls.Add(Me.Label67)
        Me.pnlLamiera.Controls.Add(Me.PictureBox32)
        Me.pnlLamiera.Controls.Add(Me.PictureBox31)
        Me.pnlLamiera.Controls.Add(Me.PictureBox30)
        Me.pnlLamiera.Controls.Add(Me.PictureBox29)
        Me.pnlLamiera.Controls.Add(Me.PictureBox28)
        Me.pnlLamiera.Controls.Add(Me.PictureBox24)
        Me.pnlLamiera.Controls.Add(Me.PictureBox21)
        Me.pnlLamiera.Controls.Add(Me.PictureBox20)
        Me.pnlLamiera.Controls.Add(Me.PictureBox19)
        Me.pnlLamiera.Controls.Add(Me.PictureBox18)
        Me.pnlLamiera.Controls.Add(Me.cboLamTTG)
        Me.pnlLamiera.Controls.Add(Me.Label65)
        Me.pnlLamiera.Controls.Add(Me.PictureBox17)
        Me.pnlLamiera.Controls.Add(Me.Label64)
        Me.pnlLamiera.Controls.Add(Me.chkLamTTG)
        Me.pnlLamiera.Controls.Add(Me.chkLamBPS)
        Me.pnlLamiera.Controls.Add(Me.chkLamCLD)
        Me.pnlLamiera.Controls.Add(Me.chkLamHAD)
        Me.pnlLamiera.Controls.Add(Me.chkLamDCL)
        Me.pnlLamiera.Controls.Add(Me.chkLamDCA)
        Me.pnlLamiera.Controls.Add(Me.chkLamDNI)
        Me.pnlLamiera.Controls.Add(Me.chkLamLCM)
        Me.pnlLamiera.Controls.Add(Me.chkLamSFR)
        Me.pnlLamiera.Controls.Add(Me.chkLamTBN)
        Me.pnlLamiera.Controls.Add(Me.chkLamTRF)
        Me.pnlLamiera.Controls.Add(Me.txtLamBAI)
        Me.pnlLamiera.Controls.Add(Me.chkLamBAI)
        Me.pnlLamiera.Controls.Add(Me.txtLamCHA)
        Me.pnlLamiera.Controls.Add(Me.chkLamCHA)
        Me.pnlLamiera.Controls.Add(Me.chkLamBHF)
        Me.pnlLamiera.Controls.Add(Me.txtLamRAD)
        Me.pnlLamiera.Controls.Add(Me.chkLamRAD)
        Me.pnlLamiera.Controls.Add(Me.chkLamBHM)
        Me.pnlLamiera.Location = New System.Drawing.Point(580, 122)
        Me.pnlLamiera.Name = "pnlLamiera"
        Me.pnlLamiera.Size = New System.Drawing.Size(540, 539)
        Me.pnlLamiera.TabIndex = 315
        Me.pnlLamiera.Visible = False
        '
        'chkLamSCQ
        '
        Me.chkLamSCQ.Checked = True
        Me.chkLamSCQ.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamSCQ.Location = New System.Drawing.Point(170, 491)
        Me.chkLamSCQ.Name = "chkLamSCQ"
        Me.chkLamSCQ.Size = New System.Drawing.Size(243, 17)
        Me.chkLamSCQ.TabIndex = 442
        Me.chkLamSCQ.Text = "ARCA 'RT0041' COSMETIC QUALITY"
        Me.chkLamSCQ.UseVisualStyleBackColor = True
        '
        'cboLamESL
        '
        Me.cboLamESL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboLamESL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboLamESL.Enabled = False
        Me.cboLamESL.FormattingEnabled = True
        Me.cboLamESL.Items.AddRange(New Object() {"-0,10", "-0,15", "-0,20", "-0,25", "-0,30", "-0,35", "-0,40", "-0,45", "-0,50"})
        Me.cboLamESL.Location = New System.Drawing.Point(268, 138)
        Me.cboLamESL.Name = "cboLamESL"
        Me.cboLamESL.Size = New System.Drawing.Size(63, 21)
        Me.cboLamESL.Sorted = True
        Me.cboLamESL.TabIndex = 441
        '
        'PictureBox92
        '
        Me.PictureBox92.BackColor = System.Drawing.Color.Black
        Me.PictureBox92.Location = New System.Drawing.Point(337, 89)
        Me.PictureBox92.Name = "PictureBox92"
        Me.PictureBox92.Size = New System.Drawing.Size(1, 55)
        Me.PictureBox92.TabIndex = 440
        Me.PictureBox92.TabStop = False
        '
        'cboLamESH
        '
        Me.cboLamESH.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboLamESH.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboLamESH.Enabled = False
        Me.cboLamESH.FormattingEnabled = True
        Me.cboLamESH.Items.AddRange(New Object() {"+0,09", "+0,075", "+0,065", "+0,055", "+0,04", "+0,035", "+0,025", "-0,10", "-0,15", "-0,20", "-0,25", "-0,30", "-0,35", "-0,40", "-0,45", "-0,50"})
        Me.cboLamESH.Location = New System.Drawing.Point(268, 116)
        Me.cboLamESH.Name = "cboLamESH"
        Me.cboLamESH.Size = New System.Drawing.Size(63, 21)
        Me.cboLamESH.Sorted = False
        Me.cboLamESH.TabIndex = 439
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(395, 147)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 438
        Me.Label4.Text = "ISO 13715"
        '
        'chkLamBEC
        '
        Me.chkLamBEC.Checked = True
        Me.chkLamBEC.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamBEC.Enabled = False
        Me.chkLamBEC.ForeColor = System.Drawing.Color.Black
        Me.chkLamBEC.Location = New System.Drawing.Point(354, 121)
        Me.chkLamBEC.Name = "chkLamBEC"
        Me.chkLamBEC.Size = New System.Drawing.Size(104, 17)
        Me.chkLamBEC.TabIndex = 437
        Me.chkLamBEC.Text = "CHAMFER x45�"
        Me.chkLamBEC.UseVisualStyleBackColor = True
        '
        'chkLamBER
        '
        Me.chkLamBER.Checked = True
        Me.chkLamBER.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamBER.Enabled = False
        Me.chkLamBER.ForeColor = System.Drawing.Color.Black
        Me.chkLamBER.Location = New System.Drawing.Point(354, 93)
        Me.chkLamBER.Name = "chkLamBER"
        Me.chkLamBER.Size = New System.Drawing.Size(70, 17)
        Me.chkLamBER.TabIndex = 436
        Me.chkLamBER.Text = "ROUND"
        Me.chkLamBER.UseVisualStyleBackColor = True
        '
        'PictureBox93
        '
        Me.PictureBox93.BackColor = System.Drawing.Color.Black
        Me.PictureBox93.Location = New System.Drawing.Point(337, 143)
        Me.PictureBox93.Name = "PictureBox93"
        Me.PictureBox93.Size = New System.Drawing.Size(178, 1)
        Me.PictureBox93.TabIndex = 435
        Me.PictureBox93.TabStop = False
        '
        'PictureBox94
        '
        Me.PictureBox94.BackColor = System.Drawing.Color.Black
        Me.PictureBox94.Location = New System.Drawing.Point(337, 115)
        Me.PictureBox94.Name = "PictureBox94"
        Me.PictureBox94.Size = New System.Drawing.Size(178, 1)
        Me.PictureBox94.TabIndex = 434
        Me.PictureBox94.TabStop = False
        '
        'PictureBox96
        '
        Me.PictureBox96.Image = CType(resources.GetObject("PictureBox96.Image"), System.Drawing.Image)
        Me.PictureBox96.Location = New System.Drawing.Point(170, 112)
        Me.PictureBox96.Name = "PictureBox96"
        Me.PictureBox96.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox96.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox96.TabIndex = 433
        Me.PictureBox96.TabStop = False
        '
        'chkLamBHE
        '
        Me.chkLamBHE.Checked = True
        Me.chkLamBHE.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamBHE.Location = New System.Drawing.Point(170, 89)
        Me.chkLamBHE.Name = "chkLamBHE"
        Me.chkLamBHE.Size = New System.Drawing.Size(165, 17)
        Me.chkLamBHE.TabIndex = 432
        Me.chkLamBHE.Text = "SHARPE EDGE"
        Me.chkLamBHE.UseVisualStyleBackColor = True
        '
        'Label131
        '
        Me.Label131.AutoSize = True
        Me.Label131.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label131.ForeColor = System.Drawing.Color.Black
        Me.Label131.Location = New System.Drawing.Point(7, 5)
        Me.Label131.Name = "Label131"
        Me.Label131.Size = New System.Drawing.Size(199, 20)
        Me.Label131.TabIndex = 377
        Me.Label131.Text = "PLATE WORKING SPECS"
        '
        'cboLamSFR
        '
        Me.cboLamSFR.Enabled = False
        Me.cboLamSFR.FormattingEnabled = True
        Me.cboLamSFR.Items.AddRange(New Object() {"1,20", "0,80", "1,50"})
        Me.cboLamSFR.Location = New System.Drawing.Point(225, 353)
        Me.cboLamSFR.Name = "cboLamSFR"
        Me.cboLamSFR.Size = New System.Drawing.Size(68, 21)
        Me.cboLamSFR.TabIndex = 323
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(3, 515)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(61, 13)
        Me.Label82.TabIndex = 363
        Me.Label82.Text = "CLEANING"
        '
        'Label81
        '
        Me.Label81.Location = New System.Drawing.Point(3, 469)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(146, 16)
        Me.Label81.TabIndex = 361
        Me.Label81.Text = "SURFACE ""-- - -- - --"""
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(3, 421)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(126, 13)
        Me.Label80.TabIndex = 360
        Me.Label80.Text = "DIMENSION CHECKING"
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(3, 401)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(155, 13)
        Me.Label79.TabIndex = 359
        Me.Label79.Text = "DIMENSION NOT INDICATED"
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(3, 379)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(127, 13)
        Me.Label78.TabIndex = 358
        Me.Label78.Text = "LASER CUTTING MARK"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(3, 357)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(96, 13)
        Me.Label77.TabIndex = 357
        Me.Label77.Text = "SATIN-FINISHING"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(3, 289)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(54, 13)
        Me.Label73.TabIndex = 354
        Me.Label73.Text = "TAPPING"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(3, 243)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(109, 13)
        Me.Label70.TabIndex = 351
        Me.Label70.Text = "BLANKING PROFILE"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(3, 221)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(88, 13)
        Me.Label69.TabIndex = 350
        Me.Label69.Text = "BENDING AREA"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(3, 199)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(146, 13)
        Me.Label68.TabIndex = 349
        Me.Label68.Text = "CHAMFER NOT INDICATED"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(3, 174)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(123, 13)
        Me.Label67.TabIndex = 348
        Me.Label67.Text = "RADII NOT INDICATED"
        '
        'PictureBox32
        '
        Me.PictureBox32.BackColor = System.Drawing.Color.Black
        Me.PictureBox32.Location = New System.Drawing.Point(0, 508)
        Me.PictureBox32.Name = "PictureBox32"
        Me.PictureBox32.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox32.TabIndex = 346
        Me.PictureBox32.TabStop = False
        '
        'PictureBox31
        '
        Me.PictureBox31.BackColor = System.Drawing.Color.Black
        Me.PictureBox31.Location = New System.Drawing.Point(0, 464)
        Me.PictureBox31.Name = "PictureBox31"
        Me.PictureBox31.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox31.TabIndex = 345
        Me.PictureBox31.TabStop = False
        '
        'PictureBox30
        '
        Me.PictureBox30.BackColor = System.Drawing.Color.Black
        Me.PictureBox30.Location = New System.Drawing.Point(0, 418)
        Me.PictureBox30.Name = "PictureBox30"
        Me.PictureBox30.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox30.TabIndex = 344
        Me.PictureBox30.TabStop = False
        '
        'PictureBox29
        '
        Me.PictureBox29.BackColor = System.Drawing.Color.Black
        Me.PictureBox29.Location = New System.Drawing.Point(0, 396)
        Me.PictureBox29.Name = "PictureBox29"
        Me.PictureBox29.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox29.TabIndex = 343
        Me.PictureBox29.TabStop = False
        '
        'PictureBox28
        '
        Me.PictureBox28.BackColor = System.Drawing.Color.Black
        Me.PictureBox28.Location = New System.Drawing.Point(0, 350)
        Me.PictureBox28.Name = "PictureBox28"
        Me.PictureBox28.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox28.TabIndex = 342
        Me.PictureBox28.TabStop = False
        '
        'PictureBox24
        '
        Me.PictureBox24.BackColor = System.Drawing.Color.Black
        Me.PictureBox24.Location = New System.Drawing.Point(0, 374)
        Me.PictureBox24.Name = "PictureBox24"
        Me.PictureBox24.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox24.TabIndex = 339
        Me.PictureBox24.TabStop = False
        '
        'PictureBox21
        '
        Me.PictureBox21.BackColor = System.Drawing.Color.Black
        Me.PictureBox21.Location = New System.Drawing.Point(0, 282)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox21.TabIndex = 336
        Me.PictureBox21.TabStop = False
        '
        'PictureBox20
        '
        Me.PictureBox20.BackColor = System.Drawing.Color.Black
        Me.PictureBox20.Location = New System.Drawing.Point(1, 238)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox20.TabIndex = 335
        Me.PictureBox20.TabStop = False
        '
        'PictureBox19
        '
        Me.PictureBox19.BackColor = System.Drawing.Color.Black
        Me.PictureBox19.Location = New System.Drawing.Point(0, 215)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox19.TabIndex = 334
        Me.PictureBox19.TabStop = False
        '
        'PictureBox18
        '
        Me.PictureBox18.BackColor = System.Drawing.Color.Black
        Me.PictureBox18.Location = New System.Drawing.Point(0, 192)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox18.TabIndex = 333
        Me.PictureBox18.TabStop = False
        '
        'cboLamTTG
        '
        Me.cboLamTTG.Enabled = False
        Me.cboLamTTG.FormattingEnabled = True
        Me.cboLamTTG.Items.AddRange(New Object() {"6H"})
        Me.cboLamTTG.Location = New System.Drawing.Point(300, 328)
        Me.cboLamTTG.Name = "cboLamTTG"
        Me.cboLamTTG.Size = New System.Drawing.Size(48, 21)
        Me.cboLamTTG.TabIndex = 318
        '
        'Label65
        '
        Me.Label65.Location = New System.Drawing.Point(186, 264)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(174, 13)
        Me.Label65.TabIndex = 332
        Me.Label65.Text = "TWO PARALLEL CUTTING"
        '
        'PictureBox17
        '
        Me.PictureBox17.BackColor = System.Drawing.Color.Black
        Me.PictureBox17.Location = New System.Drawing.Point(0, 169)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox17.TabIndex = 331
        Me.PictureBox17.TabStop = False
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(3, 41)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(82, 13)
        Me.Label64.TabIndex = 330
        Me.Label64.Text = "BURR HEIGHT"
        '
        'chkLamTTG
        '
        Me.chkLamTTG.Checked = True
        Me.chkLamTTG.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamTTG.ForeColor = System.Drawing.Color.Black
        Me.chkLamTTG.Location = New System.Drawing.Point(170, 332)
        Me.chkLamTTG.Name = "chkLamTTG"
        Me.chkLamTTG.Size = New System.Drawing.Size(165, 17)
        Me.chkLamTTG.TabIndex = 317
        Me.chkLamTTG.Text = "TOLERANCE GRADE"
        Me.chkLamTTG.UseVisualStyleBackColor = True
        '
        'chkLamBPS
        '
        Me.chkLamBPS.Checked = True
        Me.chkLamBPS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLamBPS.Enabled = False
        Me.chkLamBPS.Location = New System.Drawing.Point(170, 242)
        Me.chkLamBPS.Name = "chkLamBPS"
        Me.chkLamBPS.Size = New System.Drawing.Size(303, 17)
        Me.chkLamBPS.TabIndex = 306
        Me.chkLamBPS.Text = "STEP NOT TOLERATED AT THE INTERFACE OF"
        Me.chkLamBPS.UseVisualStyleBackColor = True
        '
        'chkLamCLD
        '
        Me.chkLamCLD.Checked = True
        Me.chkLamCLD.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLamCLD.Enabled = False
        Me.chkLamCLD.Location = New System.Drawing.Point(170, 514)
        Me.chkLamCLD.Name = "chkLamCLD"
        Me.chkLamCLD.Size = New System.Drawing.Size(250, 17)
        Me.chkLamCLD.TabIndex = 329
        Me.chkLamCLD.Text = "DIRT, OIL, GREASE NOT PERMITTED"
        Me.chkLamCLD.UseVisualStyleBackColor = True
        '
        'chkLamHAD
        '
        Me.chkLamHAD.Checked = True
        Me.chkLamHAD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamHAD.ForeColor = System.Drawing.Color.Black
        Me.chkLamHAD.Location = New System.Drawing.Point(170, 468)
        Me.chkLamHAD.Name = "chkLamHAD"
        Me.chkLamHAD.Size = New System.Drawing.Size(336, 17)
        Me.chkLamHAD.TabIndex = 328
        Me.chkLamHAD.Text = "BURRS, NOTCH AND SCRATCH NICK NOT PERMITTED"
        Me.chkLamHAD.UseVisualStyleBackColor = True
        '
        'chkLamDCL
        '
        Me.chkLamDCL.Checked = True
        Me.chkLamDCL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLamDCL.Enabled = False
        Me.chkLamDCL.Location = New System.Drawing.Point(170, 444)
        Me.chkLamDCL.Name = "chkLamDCL"
        Me.chkLamDCL.Size = New System.Drawing.Size(234, 17)
        Me.chkLamDCL.TabIndex = 327
        Me.chkLamDCL.Text = "LEAVE AT 20� � 5�C FOR 4 HOURS"
        Me.chkLamDCL.UseVisualStyleBackColor = True
        '
        'chkLamDCA
        '
        Me.chkLamDCA.Checked = True
        Me.chkLamDCA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLamDCA.Enabled = False
        Me.chkLamDCA.Location = New System.Drawing.Point(170, 422)
        Me.chkLamDCA.Name = "chkLamDCA"
        Me.chkLamDCA.Size = New System.Drawing.Size(186, 17)
        Me.chkLamDCA.TabIndex = 326
        Me.chkLamDCA.Text = "ALL AFTER TREATMENT"
        Me.chkLamDCA.UseVisualStyleBackColor = True
        '
        'chkLamDNI
        '
        Me.chkLamDNI.Checked = True
        Me.chkLamDNI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLamDNI.Enabled = False
        Me.chkLamDNI.Location = New System.Drawing.Point(170, 400)
        Me.chkLamDNI.Name = "chkLamDNI"
        Me.chkLamDNI.Size = New System.Drawing.Size(211, 17)
        Me.chkLamDNI.TabIndex = 325
        Me.chkLamDNI.Text = "REFER TO THE 3D DRAWING"
        Me.chkLamDNI.UseVisualStyleBackColor = True
        '
        'chkLamLCM
        '
        Me.chkLamLCM.Checked = True
        Me.chkLamLCM.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLamLCM.Enabled = False
        Me.chkLamLCM.ForeColor = System.Drawing.Color.Black
        Me.chkLamLCM.Location = New System.Drawing.Point(170, 378)
        Me.chkLamLCM.Name = "chkLamLCM"
        Me.chkLamLCM.Size = New System.Drawing.Size(290, 17)
        Me.chkLamLCM.TabIndex = 324
        Me.chkLamLCM.Text = "FLUSH TO THE PIECE PROFILE OR INSIDE IT"
        Me.chkLamLCM.UseVisualStyleBackColor = True
        '
        'chkLamSFR
        '
        Me.chkLamSFR.Checked = True
        Me.chkLamSFR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamSFR.ForeColor = System.Drawing.Color.Black
        Me.chkLamSFR.Location = New System.Drawing.Point(170, 356)
        Me.chkLamSFR.Name = "chkLamSFR"
        Me.chkLamSFR.Size = New System.Drawing.Size(90, 17)
        Me.chkLamSFR.TabIndex = 322
        Me.chkLamSFR.Text = "Ra um"
        Me.chkLamSFR.UseVisualStyleBackColor = True
        '
        'chkLamTBN
        '
        Me.chkLamTBN.Checked = True
        Me.chkLamTBN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamTBN.ForeColor = System.Drawing.Color.Black
        Me.chkLamTBN.Location = New System.Drawing.Point(170, 310)
        Me.chkLamTBN.Name = "chkLamTBN"
        Me.chkLamTBN.Size = New System.Drawing.Size(182, 17)
        Me.chkLamTBN.TabIndex = 316
        Me.chkLamTBN.Text = "BURR NOT PERMITTED"
        Me.chkLamTBN.UseVisualStyleBackColor = True
        '
        'chkLamTRF
        '
        Me.chkLamTRF.Checked = True
        Me.chkLamTRF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamTRF.ForeColor = System.Drawing.Color.Black
        Me.chkLamTRF.Location = New System.Drawing.Point(170, 288)
        Me.chkLamTRF.Name = "chkLamTRF"
        Me.chkLamTRF.Size = New System.Drawing.Size(145, 17)
        Me.chkLamTRF.TabIndex = 315
        Me.chkLamTRF.Text = "ROLL FORM TAP"
        Me.chkLamTRF.UseVisualStyleBackColor = True
        '
        'txtLamBAI
        '
        Me.txtLamBAI.BackColor = System.Drawing.Color.White
        Me.txtLamBAI.Enabled = False
        Me.txtLamBAI.Location = New System.Drawing.Point(316, 217)
        Me.txtLamBAI.Name = "txtLamBAI"
        Me.txtLamBAI.Size = New System.Drawing.Size(48, 20)
        Me.txtLamBAI.TabIndex = 305
        '
        'chkLamBAI
        '
        Me.chkLamBAI.Checked = True
        Me.chkLamBAI.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamBAI.ForeColor = System.Drawing.Color.Black
        Me.chkLamBAI.Location = New System.Drawing.Point(170, 220)
        Me.chkLamBAI.Name = "chkLamBAI"
        Me.chkLamBAI.Size = New System.Drawing.Size(177, 17)
        Me.chkLamBAI.TabIndex = 304
        Me.chkLamBAI.Text = "BENDING INSIDE R mm"
        Me.chkLamBAI.UseVisualStyleBackColor = True
        '
        'txtLamCHA
        '
        Me.txtLamCHA.BackColor = System.Drawing.Color.White
        Me.txtLamCHA.Enabled = False
        Me.txtLamCHA.Location = New System.Drawing.Point(264, 194)
        Me.txtLamCHA.Name = "txtLamCHA"
        Me.txtLamCHA.Size = New System.Drawing.Size(36, 20)
        Me.txtLamCHA.TabIndex = 303
        '
        'chkLamCHA
        '
        Me.chkLamCHA.Checked = True
        Me.chkLamCHA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamCHA.ForeColor = System.Drawing.Color.Black
        Me.chkLamCHA.Location = New System.Drawing.Point(170, 197)
        Me.chkLamCHA.Name = "chkLamCHA"
        Me.chkLamCHA.Size = New System.Drawing.Size(130, 17)
        Me.chkLamCHA.TabIndex = 302
        Me.chkLamCHA.Text = "CHAMFER mm"
        Me.chkLamCHA.UseVisualStyleBackColor = True
        '
        'chkLamBHF
        '
        Me.chkLamBHF.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.chkLamBHF.Checked = True
        Me.chkLamBHF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamBHF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkLamBHF.Location = New System.Drawing.Point(170, 62)
        Me.chkLamBHF.Name = "chkLamBHF"
        Me.chkLamBHF.Size = New System.Drawing.Size(194, 17)
        Me.chkLamBHF.TabIndex = 299
        Me.chkLamBHF.Text = "BURR FREE"
        Me.chkLamBHF.UseVisualStyleBackColor = False
        '
        'txtLamRAD
        '
        Me.txtLamRAD.BackColor = System.Drawing.Color.White
        Me.txtLamRAD.Enabled = False
        Me.txtLamRAD.Location = New System.Drawing.Point(222, 171)
        Me.txtLamRAD.Name = "txtLamRAD"
        Me.txtLamRAD.Size = New System.Drawing.Size(36, 20)
        Me.txtLamRAD.TabIndex = 301
        '
        'chkLamRAD
        '
        Me.chkLamRAD.Checked = True
        Me.chkLamRAD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamRAD.ForeColor = System.Drawing.Color.Black
        Me.chkLamRAD.Location = New System.Drawing.Point(170, 174)
        Me.chkLamRAD.Name = "chkLamRAD"
        Me.chkLamRAD.Size = New System.Drawing.Size(86, 17)
        Me.chkLamRAD.TabIndex = 300
        Me.chkLamRAD.Text = "R mm"
        Me.chkLamRAD.UseVisualStyleBackColor = True
        '
        'chkLamBHM
        '
        Me.chkLamBHM.Checked = True
        Me.chkLamBHM.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkLamBHM.Location = New System.Drawing.Point(170, 40)
        Me.chkLamBHM.Name = "chkLamBHM"
        Me.chkLamBHM.Size = New System.Drawing.Size(250, 17)
        Me.chkLamBHM.TabIndex = 298
        Me.chkLamBHM.Text = "25% METAL THICKNESS TOLERANCE"
        Me.chkLamBHM.UseVisualStyleBackColor = True
        '
        'pnlPressofuso
        '
        Me.pnlPressofuso.AutoScroll = True
        Me.pnlPressofuso.AutoScrollMargin = New System.Drawing.Size(10, 10)
        Me.pnlPressofuso.AutoScrollMinSize = New System.Drawing.Size(10, 10)
        Me.pnlPressofuso.Controls.Add(Me.chkPreSAR)
        Me.pnlPressofuso.Controls.Add(Me.Label110)
        Me.pnlPressofuso.Controls.Add(Me.cboPreMAE)
        Me.pnlPressofuso.Controls.Add(Me.chkPreMAE)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox50)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox49)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox48)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox47)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox46)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox43)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox42)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox41)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox40)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox39)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox38)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox37)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox36)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox35)
        Me.pnlPressofuso.Controls.Add(Me.cboPrePLF)
        Me.pnlPressofuso.Controls.Add(Me.cboPreDRA)
        Me.pnlPressofuso.Controls.Add(Me.cboPreTTG)
        Me.pnlPressofuso.Controls.Add(Me.chkPreDNI)
        Me.pnlPressofuso.Controls.Add(Me.chkPreDAR)
        Me.pnlPressofuso.Controls.Add(Me.Label5)
        Me.pnlPressofuso.Controls.Add(Me.Label86)
        Me.pnlPressofuso.Controls.Add(Me.Label103)
        Me.pnlPressofuso.Controls.Add(Me.Label102)
        Me.pnlPressofuso.Controls.Add(Me.Label101)
        Me.pnlPressofuso.Controls.Add(Me.Label100)
        Me.pnlPressofuso.Controls.Add(Me.Label99)
        Me.pnlPressofuso.Controls.Add(Me.Label96)
        Me.pnlPressofuso.Controls.Add(Me.Label95)
        Me.pnlPressofuso.Controls.Add(Me.Label94)
        Me.pnlPressofuso.Controls.Add(Me.Label93)
        Me.pnlPressofuso.Controls.Add(Me.Label92)
        Me.pnlPressofuso.Controls.Add(Me.Label91)
        Me.pnlPressofuso.Controls.Add(Me.Label90)
        Me.pnlPressofuso.Controls.Add(Me.Label89)
        Me.pnlPressofuso.Controls.Add(Me.Label88)
        Me.pnlPressofuso.Controls.Add(Me.Label87)
        Me.pnlPressofuso.Controls.Add(Me.Label85)
        Me.pnlPressofuso.Controls.Add(Me.PictureBox34)
        Me.pnlPressofuso.Controls.Add(Me.chkPreTTG)
        Me.pnlPressofuso.Controls.Add(Me.txtPreIPJ)
        Me.pnlPressofuso.Controls.Add(Me.chkPreIPJ)
        Me.pnlPressofuso.Controls.Add(Me.chkPreDCL)
        Me.pnlPressofuso.Controls.Add(Me.chkPreFSA)
        Me.pnlPressofuso.Controls.Add(Me.chkPreCLH)
        Me.pnlPressofuso.Controls.Add(Me.chkPreTBN)
        Me.pnlPressofuso.Controls.Add(Me.chkPreTRF)
        Me.pnlPressofuso.Controls.Add(Me.chkPreMNR)
        Me.pnlPressofuso.Controls.Add(Me.chkPreMRA)
        Me.pnlPressofuso.Controls.Add(Me.chkPreDCA)
        Me.pnlPressofuso.Controls.Add(Me.chkPreFSH)
        Me.pnlPressofuso.Controls.Add(Me.txtPreMAH)
        Me.pnlPressofuso.Controls.Add(Me.chkPreMAR)
        Me.pnlPressofuso.Controls.Add(Me.chkPreHOS)
        Me.pnlPressofuso.Controls.Add(Me.txtPreIPP)
        Me.pnlPressofuso.Controls.Add(Me.chkPreIPP)
        Me.pnlPressofuso.Controls.Add(Me.txtPreIPT)
        Me.pnlPressofuso.Controls.Add(Me.chkPreIPT)
        Me.pnlPressofuso.Controls.Add(Me.chkPreSCB)
        Me.pnlPressofuso.Controls.Add(Me.txtPreMEP)
        Me.pnlPressofuso.Controls.Add(Me.txtPreMOF)
        Me.pnlPressofuso.Controls.Add(Me.chkPreMEP)
        Me.pnlPressofuso.Controls.Add(Me.chkPreMOF)
        Me.pnlPressofuso.Controls.Add(Me.txtPreMGM)
        Me.pnlPressofuso.Controls.Add(Me.chkPreMGM)
        Me.pnlPressofuso.Controls.Add(Me.chkPrePLF)
        Me.pnlPressofuso.Controls.Add(Me.txtPreCHA)
        Me.pnlPressofuso.Controls.Add(Me.chkPreCHA)
        Me.pnlPressofuso.Controls.Add(Me.txtPreRAD)
        Me.pnlPressofuso.Controls.Add(Me.chkPreRAD)
        Me.pnlPressofuso.Controls.Add(Me.chkPreDRA)
        Me.pnlPressofuso.Location = New System.Drawing.Point(1222, 231)
        Me.pnlPressofuso.Name = "pnlPressofuso"
        Me.pnlPressofuso.Size = New System.Drawing.Size(540, 664)
        Me.pnlPressofuso.TabIndex = 316
        Me.pnlPressofuso.Visible = False
        '
        'chkPreSAR
        '
        Me.chkPreSAR.Checked = True
        Me.chkPreSAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreSAR.ForeColor = System.Drawing.Color.Black
        Me.chkPreSAR.Location = New System.Drawing.Point(174, 489)
        Me.chkPreSAR.Name = "chkPreSAR"
        Me.chkPreSAR.Size = New System.Drawing.Size(251, 17)
        Me.chkPreSAR.TabIndex = 358
        Me.chkPreSAR.Text = "ARCA 'RT0041' COSMETIC QUALITY"
        Me.chkPreSAR.UseVisualStyleBackColor = True
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label110.ForeColor = System.Drawing.Color.Black
        Me.Label110.Location = New System.Drawing.Point(5, 14)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(248, 20)
        Me.Label110.TabIndex = 357
        Me.Label110.Text = "DIECASTING WORKING SPECS"
        '
        'cboPreMAE
        '
        Me.cboPreMAE.Enabled = False
        Me.cboPreMAE.FormattingEnabled = True
        Me.cboPreMAE.Items.AddRange(New Object() {"0,05", "0,10"})
        Me.cboPreMAE.Location = New System.Drawing.Point(321, 241)
        Me.cboPreMAE.Name = "cboPreMAE"
        Me.cboPreMAE.Size = New System.Drawing.Size(57, 21)
        Me.cboPreMAE.TabIndex = 85
        '
        'chkPreMAE
        '
        Me.chkPreMAE.Checked = True
        Me.chkPreMAE.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreMAE.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkPreMAE.Location = New System.Drawing.Point(173, 244)
        Me.chkPreMAE.Name = "chkPreMAE"
        Me.chkPreMAE.Size = New System.Drawing.Size(204, 17)
        Me.chkPreMAE.TabIndex = 84
        Me.chkPreMAE.Text = "ALIGNMENT ERROR mm"
        Me.chkPreMAE.UseVisualStyleBackColor = True
        '
        'PictureBox50
        '
        Me.PictureBox50.BackColor = System.Drawing.Color.Black
        Me.PictureBox50.Location = New System.Drawing.Point(3, 604)
        Me.PictureBox50.Name = "PictureBox50"
        Me.PictureBox50.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox50.TabIndex = 138
        Me.PictureBox50.TabStop = False
        '
        'PictureBox49
        '
        Me.PictureBox49.BackColor = System.Drawing.Color.Black
        Me.PictureBox49.Location = New System.Drawing.Point(3, 560)
        Me.PictureBox49.Name = "PictureBox49"
        Me.PictureBox49.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox49.TabIndex = 137
        Me.PictureBox49.TabStop = False
        '
        'PictureBox48
        '
        Me.PictureBox48.BackColor = System.Drawing.Color.Black
        Me.PictureBox48.Location = New System.Drawing.Point(3, 538)
        Me.PictureBox48.Name = "PictureBox48"
        Me.PictureBox48.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox48.TabIndex = 136
        Me.PictureBox48.TabStop = False
        '
        'PictureBox47
        '
        Me.PictureBox47.BackColor = System.Drawing.Color.Black
        Me.PictureBox47.Location = New System.Drawing.Point(4, 510)
        Me.PictureBox47.Name = "PictureBox47"
        Me.PictureBox47.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox47.TabIndex = 135
        Me.PictureBox47.TabStop = False
        '
        'PictureBox46
        '
        Me.PictureBox46.BackColor = System.Drawing.Color.Black
        Me.PictureBox46.Location = New System.Drawing.Point(3, 419)
        Me.PictureBox46.Name = "PictureBox46"
        Me.PictureBox46.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox46.TabIndex = 134
        Me.PictureBox46.TabStop = False
        '
        'PictureBox43
        '
        Me.PictureBox43.BackColor = System.Drawing.Color.Black
        Me.PictureBox43.Location = New System.Drawing.Point(3, 350)
        Me.PictureBox43.Name = "PictureBox43"
        Me.PictureBox43.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox43.TabIndex = 133
        Me.PictureBox43.TabStop = False
        '
        'PictureBox42
        '
        Me.PictureBox42.BackColor = System.Drawing.Color.Black
        Me.PictureBox42.Location = New System.Drawing.Point(3, 284)
        Me.PictureBox42.Name = "PictureBox42"
        Me.PictureBox42.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox42.TabIndex = 132
        Me.PictureBox42.TabStop = False
        '
        'PictureBox41
        '
        Me.PictureBox41.BackColor = System.Drawing.Color.Black
        Me.PictureBox41.Location = New System.Drawing.Point(3, 262)
        Me.PictureBox41.Name = "PictureBox41"
        Me.PictureBox41.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox41.TabIndex = 131
        Me.PictureBox41.TabStop = False
        '
        'PictureBox40
        '
        Me.PictureBox40.BackColor = System.Drawing.Color.Black
        Me.PictureBox40.Location = New System.Drawing.Point(3, 240)
        Me.PictureBox40.Name = "PictureBox40"
        Me.PictureBox40.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox40.TabIndex = 130
        Me.PictureBox40.TabStop = False
        '
        'PictureBox39
        '
        Me.PictureBox39.BackColor = System.Drawing.Color.Black
        Me.PictureBox39.Location = New System.Drawing.Point(3, 174)
        Me.PictureBox39.Name = "PictureBox39"
        Me.PictureBox39.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox39.TabIndex = 129
        Me.PictureBox39.TabStop = False
        '
        'PictureBox38
        '
        Me.PictureBox38.BackColor = System.Drawing.Color.Black
        Me.PictureBox38.Location = New System.Drawing.Point(3, 152)
        Me.PictureBox38.Name = "PictureBox38"
        Me.PictureBox38.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox38.TabIndex = 128
        Me.PictureBox38.TabStop = False
        '
        'PictureBox37
        '
        Me.PictureBox37.BackColor = System.Drawing.Color.Black
        Me.PictureBox37.Location = New System.Drawing.Point(3, 130)
        Me.PictureBox37.Name = "PictureBox37"
        Me.PictureBox37.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox37.TabIndex = 127
        Me.PictureBox37.TabStop = False
        '
        'PictureBox36
        '
        Me.PictureBox36.BackColor = System.Drawing.Color.Black
        Me.PictureBox36.Location = New System.Drawing.Point(3, 108)
        Me.PictureBox36.Name = "PictureBox36"
        Me.PictureBox36.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox36.TabIndex = 126
        Me.PictureBox36.TabStop = False
        '
        'PictureBox35
        '
        Me.PictureBox35.BackColor = System.Drawing.Color.Black
        Me.PictureBox35.Location = New System.Drawing.Point(3, 86)
        Me.PictureBox35.Name = "PictureBox35"
        Me.PictureBox35.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox35.TabIndex = 125
        Me.PictureBox35.TabStop = False
        '
        'cboPrePLF
        '
        Me.cboPrePLF.Enabled = False
        Me.cboPrePLF.FormattingEnabled = True
        Me.cboPrePLF.Items.AddRange(New Object() {"0,05", "0,10"})
        Me.cboPrePLF.Location = New System.Drawing.Point(260, 153)
        Me.cboPrePLF.Name = "cboPrePLF"
        Me.cboPrePLF.Size = New System.Drawing.Size(58, 21)
        Me.cboPrePLF.TabIndex = 77
        '
        'cboPreDRA
        '
        Me.cboPreDRA.Enabled = False
        Me.cboPreDRA.FormattingEnabled = True
        Me.cboPreDRA.Items.AddRange(New Object() {"0,5�", "1,0�", "1,5�", "2,0�", "2,5�", "3,0�"})
        Me.cboPreDRA.Location = New System.Drawing.Point(230, 42)
        Me.cboPreDRA.Name = "cboPreDRA"
        Me.cboPreDRA.Size = New System.Drawing.Size(62, 21)
        Me.cboPreDRA.TabIndex = 69
        '
        'cboPreTTG
        '
        Me.cboPreTTG.Enabled = False
        Me.cboPreTTG.FormattingEnabled = True
        Me.cboPreTTG.Items.AddRange(New Object() {"6H"})
        Me.cboPreTTG.Location = New System.Drawing.Point(304, 395)
        Me.cboPreTTG.Name = "cboPreTTG"
        Me.cboPreTTG.Size = New System.Drawing.Size(61, 21)
        Me.cboPreTTG.TabIndex = 96
        '
        'chkPreDNI
        '
        Me.chkPreDNI.Checked = True
        Me.chkPreDNI.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreDNI.Location = New System.Drawing.Point(173, 90)
        Me.chkPreDNI.Name = "chkPreDNI"
        Me.chkPreDNI.Size = New System.Drawing.Size(210, 17)
        Me.chkPreDNI.TabIndex = 71
        Me.chkPreDNI.Text = "REFER TO THE 3D DRAWING"
        Me.chkPreDNI.UseVisualStyleBackColor = True
        '
        'chkPreDAR
        '
        Me.chkPreDAR.Checked = True
        Me.chkPreDAR.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPreDAR.Enabled = False
        Me.chkPreDAR.Location = New System.Drawing.Point(173, 68)
        Me.chkPreDAR.Name = "chkPreDAR"
        Me.chkPreDAR.Size = New System.Drawing.Size(233, 17)
        Me.chkPreDAR.TabIndex = 70
        Me.chkPreDAR.Text = "ALWAYS REFERED AT THE ROOT"
        Me.chkPreDAR.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(190, 467)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(320, 13)
        Me.Label5.TabIndex = 113
        Me.Label5.Text = "SCRATCH NICK NOT PERMITTED"
        '
        'Label86
        '
        Me.Label86.Location = New System.Drawing.Point(190, 445)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(320, 13)
        Me.Label86.TabIndex = 113
        Me.Label86.Text = "DEPRESSION, FLASH, NOTCH, ABRASION AND"
        '
        'Label103
        '
        Me.Label103.Location = New System.Drawing.Point(6, 609)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(150, 13)
        Me.Label103.TabIndex = 114
        Me.Label103.Text = "DIMENSIN CHECKING"
        '
        'Label102
        '
        Me.Label102.Location = New System.Drawing.Point(6, 565)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(159, 13)
        Me.Label102.TabIndex = 115
        Me.Label102.Text = "FINISHING OPERATION"
        '
        'Label101
        '
        Me.Label101.Location = New System.Drawing.Point(6, 543)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(93, 13)
        Me.Label101.TabIndex = 118
        Me.Label101.Text = "CLEANING"
        '
        'Label100
        '
        Me.Label100.Location = New System.Drawing.Point(6, 521)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(89, 13)
        Me.Label100.TabIndex = 116
        Me.Label100.Text = "SCRIPT MARK"
        '
        'Label99
        '
        Me.Label99.Location = New System.Drawing.Point(6, 424)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(150, 40)
        Me.Label99.TabIndex = 117
        Me.Label99.Text = "ON SURFACE ""- --- -"""
        '
        'Label96
        '
        Me.Label96.Location = New System.Drawing.Point(6, 355)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(86, 13)
        Me.Label96.TabIndex = 123
        Me.Label96.Text = "TAPPING"
        '
        'Label95
        '
        Me.Label95.Location = New System.Drawing.Point(6, 289)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(103, 13)
        Me.Label95.TabIndex = 119
        Me.Label95.Text = "INSERT, PIN"
        '
        'Label94
        '
        Me.Label94.Location = New System.Drawing.Point(6, 267)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(166, 13)
        Me.Label94.TabIndex = 120
        Me.Label94.Text = "SHAPE, RIB, THICKNESS"
        '
        'Label93
        '
        Me.Label93.Location = New System.Drawing.Point(6, 244)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(182, 13)
        Me.Label93.TabIndex = 111
        Me.Label93.Text = "MATRIX, SAND ALIGNMENT"
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(6, 179)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(38, 13)
        Me.Label92.TabIndex = 121
        Me.Label92.Text = "MARK"
        '
        'Label91
        '
        Me.Label91.Location = New System.Drawing.Point(6, 157)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(151, 13)
        Me.Label91.TabIndex = 122
        Me.Label91.Text = "PARTING LINE FLASH"
        '
        'Label90
        '
        Me.Label90.Location = New System.Drawing.Point(6, 135)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(91, 13)
        Me.Label90.TabIndex = 112
        Me.Label90.Text = "CHAMFER"
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(6, 113)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(36, 13)
        Me.Label89.TabIndex = 124
        Me.Label89.Text = "RADII"
        '
        'Label88
        '
        Me.Label88.Location = New System.Drawing.Point(6, 91)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(187, 13)
        Me.Label88.TabIndex = 110
        Me.Label88.Text = "DIMENSION NOT INDICATED"
        '
        'Label87
        '
        Me.Label87.Location = New System.Drawing.Point(6, 69)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(100, 13)
        Me.Label87.TabIndex = 108
        Me.Label87.Text = "DIMENSION"
        '
        'Label85
        '
        Me.Label85.Location = New System.Drawing.Point(6, 47)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(114, 13)
        Me.Label85.TabIndex = 109
        Me.Label85.Text = "DRAFT ANGLE"
        '
        'PictureBox34
        '
        Me.PictureBox34.BackColor = System.Drawing.Color.Black
        Me.PictureBox34.Location = New System.Drawing.Point(3, 64)
        Me.PictureBox34.Name = "PictureBox34"
        Me.PictureBox34.Size = New System.Drawing.Size(510, 1)
        Me.PictureBox34.TabIndex = 107
        Me.PictureBox34.TabStop = False
        '
        'chkPreTTG
        '
        Me.chkPreTTG.Checked = True
        Me.chkPreTTG.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreTTG.ForeColor = System.Drawing.Color.Black
        Me.chkPreTTG.Location = New System.Drawing.Point(173, 398)
        Me.chkPreTTG.Name = "chkPreTTG"
        Me.chkPreTTG.Size = New System.Drawing.Size(189, 17)
        Me.chkPreTTG.TabIndex = 95
        Me.chkPreTTG.Text = "TOLERANCE GRADE"
        Me.chkPreTTG.UseVisualStyleBackColor = True
        '
        'txtPreIPJ
        '
        Me.txtPreIPJ.BackColor = System.Drawing.Color.White
        Me.txtPreIPJ.Enabled = False
        Me.txtPreIPJ.Location = New System.Drawing.Point(341, 329)
        Me.txtPreIPJ.Name = "txtPreIPJ"
        Me.txtPreIPJ.Size = New System.Drawing.Size(36, 20)
        Me.txtPreIPJ.TabIndex = 92
        '
        'chkPreIPJ
        '
        Me.chkPreIPJ.Checked = True
        Me.chkPreIPJ.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreIPJ.ForeColor = System.Drawing.Color.Black
        Me.chkPreIPJ.Location = New System.Drawing.Point(173, 332)
        Me.chkPreIPJ.Name = "chkPreIPJ"
        Me.chkPreIPJ.Size = New System.Drawing.Size(204, 17)
        Me.chkPreIPJ.TabIndex = 91
        Me.chkPreIPJ.Text = "JACK OUT TORQUE Nm"
        Me.chkPreIPJ.UseVisualStyleBackColor = True
        '
        'chkPreDCL
        '
        Me.chkPreDCL.Checked = True
        Me.chkPreDCL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPreDCL.Enabled = False
        Me.chkPreDCL.Location = New System.Drawing.Point(172, 630)
        Me.chkPreDCL.Name = "chkPreDCL"
        Me.chkPreDCL.Size = New System.Drawing.Size(233, 17)
        Me.chkPreDCL.TabIndex = 106
        Me.chkPreDCL.Text = "LEAVE AT 20� � 5�C FOR 4 HOURS"
        Me.chkPreDCL.UseVisualStyleBackColor = True
        '
        'chkPreFSA
        '
        Me.chkPreFSA.Checked = True
        Me.chkPreFSA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreFSA.ForeColor = System.Drawing.Color.Black
        Me.chkPreFSA.Location = New System.Drawing.Point(173, 564)
        Me.chkPreFSA.Name = "chkPreFSA"
        Me.chkPreFSA.Size = New System.Drawing.Size(141, 17)
        Me.chkPreFSA.TabIndex = 103
        Me.chkPreFSA.Text = "SANDBLASTING"
        Me.chkPreFSA.UseVisualStyleBackColor = True
        '
        'chkPreCLH
        '
        Me.chkPreCLH.Checked = True
        Me.chkPreCLH.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPreCLH.Enabled = False
        Me.chkPreCLH.Location = New System.Drawing.Point(173, 542)
        Me.chkPreCLH.Name = "chkPreCLH"
        Me.chkPreCLH.Size = New System.Drawing.Size(340, 17)
        Me.chkPreCLH.TabIndex = 102
        Me.chkPreCLH.Text = "HALO,DIRT,OIL,GREASE AND CAVITATION NOT PERMITTED"
        Me.chkPreCLH.UseVisualStyleBackColor = True
        '
        'chkPreTBN
        '
        Me.chkPreTBN.Checked = True
        Me.chkPreTBN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreTBN.ForeColor = System.Drawing.Color.Black
        Me.chkPreTBN.Location = New System.Drawing.Point(173, 376)
        Me.chkPreTBN.Name = "chkPreTBN"
        Me.chkPreTBN.Size = New System.Drawing.Size(181, 17)
        Me.chkPreTBN.TabIndex = 94
        Me.chkPreTBN.Text = "BURR NOT PERMITTED"
        Me.chkPreTBN.UseVisualStyleBackColor = True
        '
        'chkPreTRF
        '
        Me.chkPreTRF.Checked = True
        Me.chkPreTRF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreTRF.ForeColor = System.Drawing.Color.Black
        Me.chkPreTRF.Location = New System.Drawing.Point(173, 354)
        Me.chkPreTRF.Name = "chkPreTRF"
        Me.chkPreTRF.Size = New System.Drawing.Size(144, 17)
        Me.chkPreTRF.TabIndex = 93
        Me.chkPreTRF.Text = "ROLL FORM TAP"
        Me.chkPreTRF.UseVisualStyleBackColor = True
        '
        'chkPreMNR
        '
        Me.chkPreMNR.Checked = True
        Me.chkPreMNR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreMNR.ForeColor = System.Drawing.Color.Black
        Me.chkPreMNR.Location = New System.Drawing.Point(367, 520)
        Me.chkPreMNR.Name = "chkPreMNR"
        Me.chkPreMNR.Size = New System.Drawing.Size(111, 17)
        Me.chkPreMNR.TabIndex = 101
        Me.chkPreMNR.Text = "Not Raised"
        Me.chkPreMNR.UseVisualStyleBackColor = True
        '
        'chkPreMRA
        '
        Me.chkPreMRA.Checked = True
        Me.chkPreMRA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreMRA.ForeColor = System.Drawing.Color.Black
        Me.chkPreMRA.Location = New System.Drawing.Point(302, 520)
        Me.chkPreMRA.Name = "chkPreMRA"
        Me.chkPreMRA.Size = New System.Drawing.Size(91, 17)
        Me.chkPreMRA.TabIndex = 100
        Me.chkPreMRA.Text = "Raised"
        Me.chkPreMRA.UseVisualStyleBackColor = True
        '
        'chkPreDCA
        '
        Me.chkPreDCA.Checked = True
        Me.chkPreDCA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPreDCA.Enabled = False
        Me.chkPreDCA.Location = New System.Drawing.Point(172, 608)
        Me.chkPreDCA.Name = "chkPreDCA"
        Me.chkPreDCA.Size = New System.Drawing.Size(185, 17)
        Me.chkPreDCA.TabIndex = 105
        Me.chkPreDCA.Text = "ALL AFTER TREATMENT"
        Me.chkPreDCA.UseVisualStyleBackColor = True
        '
        'chkPreFSH
        '
        Me.chkPreFSH.Checked = True
        Me.chkPreFSH.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreFSH.ForeColor = System.Drawing.Color.Black
        Me.chkPreFSH.Location = New System.Drawing.Point(173, 586)
        Me.chkPreFSH.Name = "chkPreFSH"
        Me.chkPreFSH.Size = New System.Drawing.Size(139, 17)
        Me.chkPreFSH.TabIndex = 104
        Me.chkPreFSH.Text = "SHOT PEENING"
        Me.chkPreFSH.UseVisualStyleBackColor = True
        '
        'txtPreMAH
        '
        Me.txtPreMAH.BackColor = System.Drawing.Color.White
        Me.txtPreMAH.Enabled = False
        Me.txtPreMAH.Location = New System.Drawing.Point(247, 517)
        Me.txtPreMAH.Name = "txtPreMAH"
        Me.txtPreMAH.Size = New System.Drawing.Size(49, 20)
        Me.txtPreMAH.TabIndex = 99
        '
        'chkPreMAR
        '
        Me.chkPreMAR.Checked = True
        Me.chkPreMAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreMAR.ForeColor = System.Drawing.Color.Black
        Me.chkPreMAR.Location = New System.Drawing.Point(173, 520)
        Me.chkPreMAR.Name = "chkPreMAR"
        Me.chkPreMAR.Size = New System.Drawing.Size(122, 17)
        Me.chkPreMAR.TabIndex = 98
        Me.chkPreMAR.Text = "H mm"
        Me.chkPreMAR.UseVisualStyleBackColor = True
        '
        'chkPreHOS
        '
        Me.chkPreHOS.Checked = True
        Me.chkPreHOS.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreHOS.ForeColor = System.Drawing.Color.Black
        Me.chkPreHOS.Location = New System.Drawing.Point(173, 423)
        Me.chkPreHOS.Name = "chkPreHOS"
        Me.chkPreHOS.Size = New System.Drawing.Size(340, 17)
        Me.chkPreHOS.TabIndex = 97
        Me.chkPreHOS.Text = "PARTING LINES, GATES, EJECTORS MARKS,"
        Me.chkPreHOS.UseVisualStyleBackColor = True
        '
        'txtPreIPP
        '
        Me.txtPreIPP.BackColor = System.Drawing.Color.White
        Me.txtPreIPP.Enabled = False
        Me.txtPreIPP.Location = New System.Drawing.Point(341, 308)
        Me.txtPreIPP.Name = "txtPreIPP"
        Me.txtPreIPP.Size = New System.Drawing.Size(36, 20)
        Me.txtPreIPP.TabIndex = 90
        '
        'chkPreIPP
        '
        Me.chkPreIPP.Checked = True
        Me.chkPreIPP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreIPP.ForeColor = System.Drawing.Color.Black
        Me.chkPreIPP.Location = New System.Drawing.Point(173, 310)
        Me.chkPreIPP.Name = "chkPreIPP"
        Me.chkPreIPP.Size = New System.Drawing.Size(204, 17)
        Me.chkPreIPP.TabIndex = 89
        Me.chkPreIPP.Text = "PULL-OUT STRENGTH N min"
        Me.chkPreIPP.UseVisualStyleBackColor = True
        '
        'txtPreIPT
        '
        Me.txtPreIPT.BackColor = System.Drawing.Color.White
        Me.txtPreIPT.Enabled = False
        Me.txtPreIPT.Location = New System.Drawing.Point(341, 287)
        Me.txtPreIPT.Name = "txtPreIPT"
        Me.txtPreIPT.Size = New System.Drawing.Size(36, 20)
        Me.txtPreIPT.TabIndex = 88
        '
        'chkPreIPT
        '
        Me.chkPreIPT.Checked = True
        Me.chkPreIPT.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreIPT.ForeColor = System.Drawing.Color.Black
        Me.chkPreIPT.Location = New System.Drawing.Point(173, 288)
        Me.chkPreIPT.Name = "chkPreIPT"
        Me.chkPreIPT.Size = New System.Drawing.Size(204, 17)
        Me.chkPreIPT.TabIndex = 87
        Me.chkPreIPT.Text = "TORSION STRENGTH Nm"
        Me.chkPreIPT.UseVisualStyleBackColor = True
        '
        'chkPreSCB
        '
        Me.chkPreSCB.Checked = True
        Me.chkPreSCB.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPreSCB.Enabled = False
        Me.chkPreSCB.Location = New System.Drawing.Point(173, 266)
        Me.chkPreSCB.Name = "chkPreSCB"
        Me.chkPreSCB.Size = New System.Drawing.Size(278, 17)
        Me.chkPreSCB.TabIndex = 86
        Me.chkPreSCB.Text = "CAN BE CHANGE AFTER ARCA AGREEMENT"
        Me.chkPreSCB.UseVisualStyleBackColor = True
        '
        'txtPreMEP
        '
        Me.txtPreMEP.BackColor = System.Drawing.Color.White
        Me.txtPreMEP.Enabled = False
        Me.txtPreMEP.Location = New System.Drawing.Point(329, 218)
        Me.txtPreMEP.Name = "txtPreMEP"
        Me.txtPreMEP.Size = New System.Drawing.Size(36, 20)
        Me.txtPreMEP.TabIndex = 83
        '
        'txtPreMOF
        '
        Me.txtPreMOF.BackColor = System.Drawing.Color.White
        Me.txtPreMOF.Enabled = False
        Me.txtPreMOF.Location = New System.Drawing.Point(329, 197)
        Me.txtPreMOF.Name = "txtPreMOF"
        Me.txtPreMOF.Size = New System.Drawing.Size(36, 20)
        Me.txtPreMOF.TabIndex = 81
        '
        'chkPreMEP
        '
        Me.chkPreMEP.Checked = True
        Me.chkPreMEP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreMEP.Location = New System.Drawing.Point(173, 222)
        Me.chkPreMEP.Name = "chkPreMEP"
        Me.chkPreMEP.Size = New System.Drawing.Size(192, 17)
        Me.chkPreMEP.TabIndex = 82
        Me.chkPreMEP.Text = "EJECTOR PIN MARK H mm"
        Me.chkPreMEP.UseVisualStyleBackColor = True
        '
        'chkPreMOF
        '
        Me.chkPreMOF.Checked = True
        Me.chkPreMOF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreMOF.Location = New System.Drawing.Point(173, 200)
        Me.chkPreMOF.Name = "chkPreMOF"
        Me.chkPreMOF.Size = New System.Drawing.Size(186, 17)
        Me.chkPreMOF.TabIndex = 80
        Me.chkPreMOF.Text = "OVER FLOW MARK H mm"
        Me.chkPreMOF.UseVisualStyleBackColor = True
        '
        'txtPreMGM
        '
        Me.txtPreMGM.BackColor = System.Drawing.Color.White
        Me.txtPreMGM.Enabled = False
        Me.txtPreMGM.Location = New System.Drawing.Point(288, 175)
        Me.txtPreMGM.Name = "txtPreMGM"
        Me.txtPreMGM.Size = New System.Drawing.Size(36, 20)
        Me.txtPreMGM.TabIndex = 79
        '
        'chkPreMGM
        '
        Me.chkPreMGM.Checked = True
        Me.chkPreMGM.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreMGM.Location = New System.Drawing.Point(173, 178)
        Me.chkPreMGM.Name = "chkPreMGM"
        Me.chkPreMGM.Size = New System.Drawing.Size(151, 17)
        Me.chkPreMGM.TabIndex = 78
        Me.chkPreMGM.Text = "GATE MARK H mm"
        Me.chkPreMGM.UseVisualStyleBackColor = True
        '
        'chkPrePLF
        '
        Me.chkPrePLF.Checked = True
        Me.chkPrePLF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPrePLF.Location = New System.Drawing.Point(173, 156)
        Me.chkPrePLF.Name = "chkPrePLF"
        Me.chkPrePLF.Size = New System.Drawing.Size(145, 17)
        Me.chkPrePLF.TabIndex = 76
        Me.chkPrePLF.Text = "FLASH H mm"
        Me.chkPrePLF.UseVisualStyleBackColor = True
        '
        'txtPreCHA
        '
        Me.txtPreCHA.BackColor = System.Drawing.Color.White
        Me.txtPreCHA.Enabled = False
        Me.txtPreCHA.Location = New System.Drawing.Point(295, 132)
        Me.txtPreCHA.Name = "txtPreCHA"
        Me.txtPreCHA.Size = New System.Drawing.Size(36, 20)
        Me.txtPreCHA.TabIndex = 75
        '
        'chkPreCHA
        '
        Me.chkPreCHA.Checked = True
        Me.chkPreCHA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreCHA.ForeColor = System.Drawing.Color.Black
        Me.chkPreCHA.Location = New System.Drawing.Point(173, 134)
        Me.chkPreCHA.Name = "chkPreCHA"
        Me.chkPreCHA.Size = New System.Drawing.Size(156, 17)
        Me.chkPreCHA.TabIndex = 74
        Me.chkPreCHA.Text = "CHAMFER mm x 45�"
        Me.chkPreCHA.UseVisualStyleBackColor = True
        '
        'txtPreRAD
        '
        Me.txtPreRAD.BackColor = System.Drawing.Color.White
        Me.txtPreRAD.Enabled = False
        Me.txtPreRAD.Location = New System.Drawing.Point(225, 109)
        Me.txtPreRAD.Name = "txtPreRAD"
        Me.txtPreRAD.Size = New System.Drawing.Size(36, 20)
        Me.txtPreRAD.TabIndex = 73
        '
        'chkPreRAD
        '
        Me.chkPreRAD.Checked = True
        Me.chkPreRAD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreRAD.ForeColor = System.Drawing.Color.Black
        Me.chkPreRAD.Location = New System.Drawing.Point(173, 112)
        Me.chkPreRAD.Name = "chkPreRAD"
        Me.chkPreRAD.Size = New System.Drawing.Size(85, 17)
        Me.chkPreRAD.TabIndex = 72
        Me.chkPreRAD.Text = "R mm"
        Me.chkPreRAD.UseVisualStyleBackColor = True
        '
        'chkPreDRA
        '
        Me.chkPreDRA.Checked = True
        Me.chkPreDRA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkPreDRA.Location = New System.Drawing.Point(173, 46)
        Me.chkPreDRA.Name = "chkPreDRA"
        Me.chkPreDRA.Size = New System.Drawing.Size(119, 17)
        Me.chkPreDRA.TabIndex = 68
        Me.chkPreDRA.Text = "DRAFT"
        Me.chkPreDRA.UseVisualStyleBackColor = True
        '
        'pnlResina
        '
        Me.pnlResina.AutoScroll = True
        Me.pnlResina.Controls.Add(Me.chkResSAR)
        Me.pnlResina.Controls.Add(Me.chkResSGL)
        Me.pnlResina.Controls.Add(Me.cboResSRA)
        Me.pnlResina.Controls.Add(Me.cboResSVD)
        Me.pnlResina.Controls.Add(Me.cboResMSA)
        Me.pnlResina.Controls.Add(Me.chkResMSA)
        Me.pnlResina.Controls.Add(Me.chkResDNI)
        Me.pnlResina.Controls.Add(Me.cboResMEM)
        Me.pnlResina.Controls.Add(Me.cboResMGM)
        Me.pnlResina.Controls.Add(Me.cboResPLF)
        Me.pnlResina.Controls.Add(Me.cboResDRA)
        Me.pnlResina.Controls.Add(Me.PictureBox68)
        Me.pnlResina.Controls.Add(Me.PictureBox67)
        Me.pnlResina.Controls.Add(Me.chkResDAR)
        Me.pnlResina.Controls.Add(Me.PictureBox63)
        Me.pnlResina.Controls.Add(Me.PictureBox62)
        Me.pnlResina.Controls.Add(Me.PictureBox60)
        Me.pnlResina.Controls.Add(Me.PictureBox58)
        Me.pnlResina.Controls.Add(Me.PictureBox56)
        Me.pnlResina.Controls.Add(Me.PictureBox55)
        Me.pnlResina.Controls.Add(Me.PictureBox54)
        Me.pnlResina.Controls.Add(Me.PictureBox53)
        Me.pnlResina.Controls.Add(Me.PictureBox52)
        Me.pnlResina.Controls.Add(Me.PictureBox51)
        Me.pnlResina.Controls.Add(Me.PictureBox45)
        Me.pnlResina.Controls.Add(Me.PictureBox44)
        Me.pnlResina.Controls.Add(Me.Label116)
        Me.pnlResina.Controls.Add(Me.Label114)
        Me.pnlResina.Controls.Add(Me.Label113)
        Me.pnlResina.Controls.Add(Me.Label123)
        Me.pnlResina.Controls.Add(Me.Label111)
        Me.pnlResina.Controls.Add(Me.Label109)
        Me.pnlResina.Controls.Add(Me.Label108)
        Me.pnlResina.Controls.Add(Me.Label107)
        Me.pnlResina.Controls.Add(Me.chkResHAD)
        Me.pnlResina.Controls.Add(Me.Label106)
        Me.pnlResina.Controls.Add(Me.Label105)
        Me.pnlResina.Controls.Add(Me.Label104)
        Me.pnlResina.Controls.Add(Me.Label122)
        Me.pnlResina.Controls.Add(Me.Label121)
        Me.pnlResina.Controls.Add(Me.Label98)
        Me.pnlResina.Controls.Add(Me.txtResIPJ)
        Me.pnlResina.Controls.Add(Me.chkResIPJ)
        Me.pnlResina.Controls.Add(Me.chkResSVD)
        Me.pnlResina.Controls.Add(Me.chkResMAN)
        Me.pnlResina.Controls.Add(Me.chkResMAR)
        Me.pnlResina.Controls.Add(Me.chkResMEL)
        Me.pnlResina.Controls.Add(Me.txtResMAH)
        Me.pnlResina.Controls.Add(Me.chkResMAH)
        Me.pnlResina.Controls.Add(Me.txtResIPP)
        Me.pnlResina.Controls.Add(Me.chkResIPP)
        Me.pnlResina.Controls.Add(Me.txtResIPT)
        Me.pnlResina.Controls.Add(Me.chkResIPT)
        Me.pnlResina.Controls.Add(Me.chkResSRT)
        Me.pnlResina.Controls.Add(Me.chkResMEM)
        Me.pnlResina.Controls.Add(Me.chkResMGM)
        Me.pnlResina.Controls.Add(Me.chkResPLF)
        Me.pnlResina.Controls.Add(Me.txtResCHA)
        Me.pnlResina.Controls.Add(Me.chkResCHA)
        Me.pnlResina.Controls.Add(Me.txtResRAD)
        Me.pnlResina.Controls.Add(Me.chkResRAD)
        Me.pnlResina.Controls.Add(Me.chkResDRA)
        Me.pnlResina.Controls.Add(Me.Label23)
        Me.pnlResina.Location = New System.Drawing.Point(1260, 200)
        Me.pnlResina.Name = "pnlResina"
        Me.pnlResina.Size = New System.Drawing.Size(540, 576)
        Me.pnlResina.TabIndex = 317
        Me.pnlResina.Visible = False
        '
        'chkResSAR
        '
        Me.chkResSAR.Checked = True
        Me.chkResSAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResSAR.Location = New System.Drawing.Point(181, 437)
        Me.chkResSAR.Name = "chkResSAR"
        Me.chkResSAR.Size = New System.Drawing.Size(220, 17)
        Me.chkResSAR.TabIndex = 361
        Me.chkResSAR.Text = "ARCA 'RT0041' COESMETIC QUALITY"
        Me.chkResSAR.UseVisualStyleBackColor = True
        '
        'chkResSGL
        '
        Me.chkResSGL.Checked = True
        Me.chkResSGL.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResSGL.ForeColor = System.Drawing.Color.Black
        Me.chkResSGL.Location = New System.Drawing.Point(181, 370)
        Me.chkResSGL.Name = "chkResSGL"
        Me.chkResSGL.Size = New System.Drawing.Size(157, 17)
        Me.chkResSGL.TabIndex = 360
        Me.chkResSGL.Text = "GLOSSED"
        Me.chkResSGL.UseVisualStyleBackColor = True
        '
        'cboResSRA
        '
        Me.cboResSRA.Enabled = False
        Me.cboResSRA.FormattingEnabled = True
        Me.cboResSRA.Location = New System.Drawing.Point(323, 345)
        Me.cboResSRA.Name = "cboResSRA"
        Me.cboResSRA.Size = New System.Drawing.Size(55, 21)
        Me.cboResSRA.TabIndex = 168
        '
        'cboResSVD
        '
        Me.cboResSVD.Enabled = False
        Me.cboResSVD.FormattingEnabled = True
        Me.cboResSVD.Location = New System.Drawing.Point(224, 345)
        Me.cboResSVD.Name = "cboResSVD"
        Me.cboResSVD.Size = New System.Drawing.Size(55, 21)
        Me.cboResSVD.TabIndex = 167
        '
        'cboResMSA
        '
        Me.cboResMSA.Enabled = False
        Me.cboResMSA.FormattingEnabled = True
        Me.cboResMSA.Items.AddRange(New Object() {"0,05", "0,10", "0,15"})
        Me.cboResMSA.Location = New System.Drawing.Point(328, 211)
        Me.cboResMSA.Name = "cboResMSA"
        Me.cboResMSA.Size = New System.Drawing.Size(58, 21)
        Me.cboResMSA.TabIndex = 112
        '
        'chkResMSA
        '
        Me.chkResMSA.Checked = True
        Me.chkResMSA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResMSA.Location = New System.Drawing.Point(181, 214)
        Me.chkResMSA.Name = "chkResMSA"
        Me.chkResMSA.Size = New System.Drawing.Size(169, 17)
        Me.chkResMSA.TabIndex = 111
        Me.chkResMSA.Text = "ALIGNMENT ERROR mm"
        Me.chkResMSA.UseVisualStyleBackColor = True
        '
        'chkResDNI
        '
        Me.chkResDNI.Checked = True
        Me.chkResDNI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkResDNI.Enabled = False
        Me.chkResDNI.Location = New System.Drawing.Point(181, 82)
        Me.chkResDNI.Name = "chkResDNI"
        Me.chkResDNI.Size = New System.Drawing.Size(197, 17)
        Me.chkResDNI.TabIndex = 100
        Me.chkResDNI.Text = "REFER TO THE 3D DRAWING"
        Me.chkResDNI.UseVisualStyleBackColor = True
        '
        'cboResMEM
        '
        Me.cboResMEM.Enabled = False
        Me.cboResMEM.FormattingEnabled = True
        Me.cboResMEM.Items.AddRange(New Object() {"0,30"})
        Me.cboResMEM.Location = New System.Drawing.Point(284, 188)
        Me.cboResMEM.Name = "cboResMEM"
        Me.cboResMEM.Size = New System.Drawing.Size(58, 21)
        Me.cboResMEM.TabIndex = 110
        '
        'cboResMGM
        '
        Me.cboResMGM.Enabled = False
        Me.cboResMGM.FormattingEnabled = True
        Me.cboResMGM.Items.AddRange(New Object() {"0,30"})
        Me.cboResMGM.Location = New System.Drawing.Point(284, 167)
        Me.cboResMGM.Name = "cboResMGM"
        Me.cboResMGM.Size = New System.Drawing.Size(58, 21)
        Me.cboResMGM.TabIndex = 108
        '
        'cboResPLF
        '
        Me.cboResPLF.Enabled = False
        Me.cboResPLF.FormattingEnabled = True
        Me.cboResPLF.Items.AddRange(New Object() {"0,05", "0,10"})
        Me.cboResPLF.Location = New System.Drawing.Point(258, 145)
        Me.cboResPLF.Name = "cboResPLF"
        Me.cboResPLF.Size = New System.Drawing.Size(58, 21)
        Me.cboResPLF.TabIndex = 106
        '
        'cboResDRA
        '
        Me.cboResDRA.Enabled = False
        Me.cboResDRA.FormattingEnabled = True
        Me.cboResDRA.Items.AddRange(New Object() {"1,0�", "0,5�", "1,5�", "2,0�", "2,5�", "3,0�"})
        Me.cboResDRA.Location = New System.Drawing.Point(241, 34)
        Me.cboResDRA.Name = "cboResDRA"
        Me.cboResDRA.Size = New System.Drawing.Size(58, 21)
        Me.cboResDRA.TabIndex = 98
        '
        'PictureBox68
        '
        Me.PictureBox68.BackColor = System.Drawing.Color.Black
        Me.PictureBox68.Location = New System.Drawing.Point(11, 100)
        Me.PictureBox68.Name = "PictureBox68"
        Me.PictureBox68.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox68.TabIndex = 166
        Me.PictureBox68.TabStop = False
        '
        'PictureBox67
        '
        Me.PictureBox67.BackColor = System.Drawing.Color.Black
        Me.PictureBox67.Location = New System.Drawing.Point(11, 78)
        Me.PictureBox67.Name = "PictureBox67"
        Me.PictureBox67.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox67.TabIndex = 165
        Me.PictureBox67.TabStop = False
        '
        'chkResDAR
        '
        Me.chkResDAR.Checked = True
        Me.chkResDAR.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkResDAR.Enabled = False
        Me.chkResDAR.Location = New System.Drawing.Point(181, 60)
        Me.chkResDAR.Name = "chkResDAR"
        Me.chkResDAR.Size = New System.Drawing.Size(220, 17)
        Me.chkResDAR.TabIndex = 99
        Me.chkResDAR.Text = "ALWAYS REFERED AT THE ROOT"
        Me.chkResDAR.UseVisualStyleBackColor = True
        '
        'PictureBox63
        '
        Me.PictureBox63.BackColor = System.Drawing.Color.Black
        Me.PictureBox63.Location = New System.Drawing.Point(11, 478)
        Me.PictureBox63.Name = "PictureBox63"
        Me.PictureBox63.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox63.TabIndex = 163
        Me.PictureBox63.TabStop = False
        '
        'PictureBox62
        '
        Me.PictureBox62.BackColor = System.Drawing.Color.Black
        Me.PictureBox62.Location = New System.Drawing.Point(11, 456)
        Me.PictureBox62.Name = "PictureBox62"
        Me.PictureBox62.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox62.TabIndex = 162
        Me.PictureBox62.TabStop = False
        '
        'PictureBox60
        '
        Me.PictureBox60.BackColor = System.Drawing.Color.Black
        Me.PictureBox60.Location = New System.Drawing.Point(11, 343)
        Me.PictureBox60.Name = "PictureBox60"
        Me.PictureBox60.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox60.TabIndex = 161
        Me.PictureBox60.TabStop = False
        '
        'PictureBox58
        '
        Me.PictureBox58.BackColor = System.Drawing.Color.Black
        Me.PictureBox58.Location = New System.Drawing.Point(11, 320)
        Me.PictureBox58.Name = "PictureBox58"
        Me.PictureBox58.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox58.TabIndex = 160
        Me.PictureBox58.TabStop = False
        '
        'PictureBox56
        '
        Me.PictureBox56.BackColor = System.Drawing.Color.Black
        Me.PictureBox56.Location = New System.Drawing.Point(11, 254)
        Me.PictureBox56.Name = "PictureBox56"
        Me.PictureBox56.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox56.TabIndex = 159
        Me.PictureBox56.TabStop = False
        '
        'PictureBox55
        '
        Me.PictureBox55.BackColor = System.Drawing.Color.Black
        Me.PictureBox55.Location = New System.Drawing.Point(11, 232)
        Me.PictureBox55.Name = "PictureBox55"
        Me.PictureBox55.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox55.TabIndex = 158
        Me.PictureBox55.TabStop = False
        '
        'PictureBox54
        '
        Me.PictureBox54.BackColor = System.Drawing.Color.Black
        Me.PictureBox54.Location = New System.Drawing.Point(11, 210)
        Me.PictureBox54.Name = "PictureBox54"
        Me.PictureBox54.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox54.TabIndex = 157
        Me.PictureBox54.TabStop = False
        '
        'PictureBox53
        '
        Me.PictureBox53.BackColor = System.Drawing.Color.Black
        Me.PictureBox53.Location = New System.Drawing.Point(11, 166)
        Me.PictureBox53.Name = "PictureBox53"
        Me.PictureBox53.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox53.TabIndex = 156
        Me.PictureBox53.TabStop = False
        '
        'PictureBox52
        '
        Me.PictureBox52.BackColor = System.Drawing.Color.Black
        Me.PictureBox52.Location = New System.Drawing.Point(11, 144)
        Me.PictureBox52.Name = "PictureBox52"
        Me.PictureBox52.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox52.TabIndex = 155
        Me.PictureBox52.TabStop = False
        '
        'PictureBox51
        '
        Me.PictureBox51.BackColor = System.Drawing.Color.Black
        Me.PictureBox51.Location = New System.Drawing.Point(11, 144)
        Me.PictureBox51.Name = "PictureBox51"
        Me.PictureBox51.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox51.TabIndex = 154
        Me.PictureBox51.TabStop = False
        '
        'PictureBox45
        '
        Me.PictureBox45.BackColor = System.Drawing.Color.Black
        Me.PictureBox45.Location = New System.Drawing.Point(11, 122)
        Me.PictureBox45.Name = "PictureBox45"
        Me.PictureBox45.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox45.TabIndex = 153
        Me.PictureBox45.TabStop = False
        '
        'PictureBox44
        '
        Me.PictureBox44.BackColor = System.Drawing.Color.Black
        Me.PictureBox44.Location = New System.Drawing.Point(11, 56)
        Me.PictureBox44.Name = "PictureBox44"
        Me.PictureBox44.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox44.TabIndex = 152
        Me.PictureBox44.TabStop = False
        '
        'Label116
        '
        Me.Label116.Location = New System.Drawing.Point(14, 461)
        Me.Label116.Name = "Label116"
        Me.Label116.Size = New System.Drawing.Size(91, 13)
        Me.Label116.TabIndex = 146
        Me.Label116.Text = "MEASURING"
        '
        'Label114
        '
        Me.Label114.AutoSize = True
        Me.Label114.Location = New System.Drawing.Point(14, 348)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(57, 13)
        Me.Label114.TabIndex = 145
        Me.Label114.Text = "SURFACE"
        '
        'Label113
        '
        Me.Label113.AutoSize = True
        Me.Label113.Location = New System.Drawing.Point(14, 326)
        Me.Label113.Name = "Label113"
        Me.Label113.Size = New System.Drawing.Size(80, 13)
        Me.Label113.TabIndex = 151
        Me.Label113.Text = "SCRIPT MARK"
        '
        'Label123
        '
        Me.Label123.Location = New System.Drawing.Point(197, 418)
        Me.Label123.Name = "Label123"
        Me.Label123.Size = New System.Drawing.Size(335, 13)
        Me.Label123.TabIndex = 143
        Me.Label123.Text = "FLASH, NOTCH AND SCRATCH NICK NOT PERMITTED"
        '
        'Label111
        '
        Me.Label111.Location = New System.Drawing.Point(14, 259)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(90, 13)
        Me.Label111.TabIndex = 138
        Me.Label111.Text = "INSERT, PIN"
        '
        'Label109
        '
        Me.Label109.Location = New System.Drawing.Point(14, 237)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(153, 13)
        Me.Label109.TabIndex = 141
        Me.Label109.Text = "SHAPE, RIB, THICKNESS"
        '
        'Label108
        '
        Me.Label108.Location = New System.Drawing.Point(14, 215)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(182, 13)
        Me.Label108.TabIndex = 140
        Me.Label108.Text = "MATRIX, SADDLE ALIGNMENT"
        '
        'Label107
        '
        Me.Label107.Location = New System.Drawing.Point(14, 171)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(57, 13)
        Me.Label107.TabIndex = 139
        Me.Label107.Text = "MARK"
        '
        'chkResHAD
        '
        Me.chkResHAD.Checked = True
        Me.chkResHAD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResHAD.ForeColor = System.Drawing.Color.Black
        Me.chkResHAD.Location = New System.Drawing.Point(181, 395)
        Me.chkResHAD.Name = "chkResHAD"
        Me.chkResHAD.Size = New System.Drawing.Size(340, 17)
        Me.chkResHAD.TabIndex = 120
        Me.chkResHAD.Text = "PARTING LINES, GATES, EJECTORS, MARKS, DEPRESSION,"
        Me.chkResHAD.UseVisualStyleBackColor = True
        '
        'Label106
        '
        Me.Label106.Location = New System.Drawing.Point(14, 149)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(138, 13)
        Me.Label106.TabIndex = 137
        Me.Label106.Text = "PARTING LINE FLASH"
        '
        'Label105
        '
        Me.Label105.Location = New System.Drawing.Point(14, 127)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(78, 13)
        Me.Label105.TabIndex = 136
        Me.Label105.Text = "CHAMFER"
        '
        'Label104
        '
        Me.Label104.Location = New System.Drawing.Point(14, 105)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(55, 13)
        Me.Label104.TabIndex = 134
        Me.Label104.Text = "RADII"
        '
        'Label122
        '
        Me.Label122.Location = New System.Drawing.Point(14, 83)
        Me.Label122.Name = "Label122"
        Me.Label122.Size = New System.Drawing.Size(174, 13)
        Me.Label122.TabIndex = 133
        Me.Label122.Text = "DIMENSION NOT INDICATED"
        '
        'Label121
        '
        Me.Label121.Location = New System.Drawing.Point(14, 61)
        Me.Label121.Name = "Label121"
        Me.Label121.Size = New System.Drawing.Size(87, 13)
        Me.Label121.TabIndex = 142
        Me.Label121.Text = "DIMENSION"
        '
        'Label98
        '
        Me.Label98.Location = New System.Drawing.Point(14, 39)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(101, 13)
        Me.Label98.TabIndex = 135
        Me.Label98.Text = "DRAFT ANGLE"
        '
        'txtResIPJ
        '
        Me.txtResIPJ.BackColor = System.Drawing.Color.White
        Me.txtResIPJ.Enabled = False
        Me.txtResIPJ.Location = New System.Drawing.Point(355, 299)
        Me.txtResIPJ.Name = "txtResIPJ"
        Me.txtResIPJ.Size = New System.Drawing.Size(36, 20)
        Me.txtResIPJ.TabIndex = 119
        '
        'chkResIPJ
        '
        Me.chkResIPJ.Checked = True
        Me.chkResIPJ.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResIPJ.ForeColor = System.Drawing.Color.Black
        Me.chkResIPJ.Location = New System.Drawing.Point(181, 302)
        Me.chkResIPJ.Name = "chkResIPJ"
        Me.chkResIPJ.Size = New System.Drawing.Size(165, 17)
        Me.chkResIPJ.TabIndex = 118
        Me.chkResIPJ.Text = "JACK OUT TORQUE > Nm"
        Me.chkResIPJ.UseVisualStyleBackColor = True
        '
        'chkResSVD
        '
        Me.chkResSVD.Checked = True
        Me.chkResSVD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResSVD.ForeColor = System.Drawing.Color.Black
        Me.chkResSVD.Location = New System.Drawing.Point(181, 347)
        Me.chkResSVD.Name = "chkResSVD"
        Me.chkResSVD.Size = New System.Drawing.Size(160, 17)
        Me.chkResSVD.TabIndex = 125
        Me.chkResSVD.Text = "VDI                      Ra um"
        Me.chkResSVD.UseVisualStyleBackColor = True
        '
        'chkResMAN
        '
        Me.chkResMAN.Checked = True
        Me.chkResMAN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResMAN.ForeColor = System.Drawing.Color.Black
        Me.chkResMAN.Location = New System.Drawing.Point(350, 325)
        Me.chkResMAN.Name = "chkResMAN"
        Me.chkResMAN.Size = New System.Drawing.Size(98, 17)
        Me.chkResMAN.TabIndex = 124
        Me.chkResMAN.Text = "Not Raised"
        Me.chkResMAN.UseVisualStyleBackColor = True
        '
        'chkResMAR
        '
        Me.chkResMAR.Checked = True
        Me.chkResMAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResMAR.ForeColor = System.Drawing.Color.Black
        Me.chkResMAR.Location = New System.Drawing.Point(285, 325)
        Me.chkResMAR.Name = "chkResMAR"
        Me.chkResMAR.Size = New System.Drawing.Size(78, 17)
        Me.chkResMAR.TabIndex = 123
        Me.chkResMAR.Text = "Raised"
        Me.chkResMAR.UseVisualStyleBackColor = True
        '
        'chkResMEL
        '
        Me.chkResMEL.Checked = True
        Me.chkResMEL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkResMEL.Enabled = False
        Me.chkResMEL.Location = New System.Drawing.Point(181, 460)
        Me.chkResMEL.Name = "chkResMEL"
        Me.chkResMEL.Size = New System.Drawing.Size(220, 17)
        Me.chkResMEL.TabIndex = 127
        Me.chkResMEL.Text = "LEAVE AT 20� � 5�C FOR 4 HOURS"
        Me.chkResMEL.UseVisualStyleBackColor = True
        '
        'txtResMAH
        '
        Me.txtResMAH.BackColor = System.Drawing.Color.White
        Me.txtResMAH.Enabled = False
        Me.txtResMAH.Location = New System.Drawing.Point(233, 322)
        Me.txtResMAH.Name = "txtResMAH"
        Me.txtResMAH.Size = New System.Drawing.Size(36, 20)
        Me.txtResMAH.TabIndex = 122
        '
        'chkResMAH
        '
        Me.chkResMAH.Checked = True
        Me.chkResMAH.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResMAH.ForeColor = System.Drawing.Color.Black
        Me.chkResMAH.Location = New System.Drawing.Point(181, 325)
        Me.chkResMAH.Name = "chkResMAH"
        Me.chkResMAH.Size = New System.Drawing.Size(72, 17)
        Me.chkResMAH.TabIndex = 121
        Me.chkResMAH.Text = "H mm"
        Me.chkResMAH.UseVisualStyleBackColor = True
        '
        'txtResIPP
        '
        Me.txtResIPP.BackColor = System.Drawing.Color.White
        Me.txtResIPP.Enabled = False
        Me.txtResIPP.Location = New System.Drawing.Point(355, 278)
        Me.txtResIPP.Name = "txtResIPP"
        Me.txtResIPP.Size = New System.Drawing.Size(36, 20)
        Me.txtResIPP.TabIndex = 117
        '
        'chkResIPP
        '
        Me.chkResIPP.Checked = True
        Me.chkResIPP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResIPP.ForeColor = System.Drawing.Color.Black
        Me.chkResIPP.Location = New System.Drawing.Point(181, 280)
        Me.chkResIPP.Name = "chkResIPP"
        Me.chkResIPP.Size = New System.Drawing.Size(172, 17)
        Me.chkResIPP.TabIndex = 116
        Me.chkResIPP.Text = "PULL-OUT STRENGTH > N min"
        Me.chkResIPP.UseVisualStyleBackColor = True
        '
        'txtResIPT
        '
        Me.txtResIPT.BackColor = System.Drawing.Color.White
        Me.txtResIPT.Enabled = False
        Me.txtResIPT.Location = New System.Drawing.Point(355, 257)
        Me.txtResIPT.Name = "txtResIPT"
        Me.txtResIPT.Size = New System.Drawing.Size(36, 20)
        Me.txtResIPT.TabIndex = 115
        '
        'chkResIPT
        '
        Me.chkResIPT.Checked = True
        Me.chkResIPT.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResIPT.ForeColor = System.Drawing.Color.Black
        Me.chkResIPT.Location = New System.Drawing.Point(181, 258)
        Me.chkResIPT.Name = "chkResIPT"
        Me.chkResIPT.Size = New System.Drawing.Size(176, 17)
        Me.chkResIPT.TabIndex = 114
        Me.chkResIPT.Text = "TORSION STRENGTH > Nm"
        Me.chkResIPT.UseVisualStyleBackColor = True
        '
        'chkResSRT
        '
        Me.chkResSRT.Checked = True
        Me.chkResSRT.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkResSRT.Enabled = False
        Me.chkResSRT.Location = New System.Drawing.Point(181, 236)
        Me.chkResSRT.Name = "chkResSRT"
        Me.chkResSRT.Size = New System.Drawing.Size(265, 17)
        Me.chkResSRT.TabIndex = 113
        Me.chkResSRT.Text = "CAN BE CHANGE AFTER ARCA AGREEMENT"
        Me.chkResSRT.UseVisualStyleBackColor = True
        '
        'chkResMEM
        '
        Me.chkResMEM.Checked = True
        Me.chkResMEM.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResMEM.Location = New System.Drawing.Point(181, 192)
        Me.chkResMEM.Name = "chkResMEM"
        Me.chkResMEM.Size = New System.Drawing.Size(123, 17)
        Me.chkResMEM.TabIndex = 109
        Me.chkResMEM.Text = "EP MARK H mm"
        Me.chkResMEM.UseVisualStyleBackColor = True
        '
        'chkResMGM
        '
        Me.chkResMGM.Checked = True
        Me.chkResMGM.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResMGM.Location = New System.Drawing.Point(181, 170)
        Me.chkResMGM.Name = "chkResMGM"
        Me.chkResMGM.Size = New System.Drawing.Size(124, 17)
        Me.chkResMGM.TabIndex = 107
        Me.chkResMGM.Text = "GT MARK H mm"
        Me.chkResMGM.UseVisualStyleBackColor = True
        '
        'chkResPLF
        '
        Me.chkResPLF.Checked = True
        Me.chkResPLF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResPLF.Location = New System.Drawing.Point(181, 148)
        Me.chkResPLF.Name = "chkResPLF"
        Me.chkResPLF.Size = New System.Drawing.Size(98, 17)
        Me.chkResPLF.TabIndex = 105
        Me.chkResPLF.Text = "FLASH mm"
        Me.chkResPLF.UseVisualStyleBackColor = True
        '
        'txtResCHA
        '
        Me.txtResCHA.BackColor = System.Drawing.Color.White
        Me.txtResCHA.Enabled = False
        Me.txtResCHA.Location = New System.Drawing.Point(302, 123)
        Me.txtResCHA.Name = "txtResCHA"
        Me.txtResCHA.Size = New System.Drawing.Size(36, 20)
        Me.txtResCHA.TabIndex = 104
        '
        'chkResCHA
        '
        Me.chkResCHA.Checked = True
        Me.chkResCHA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResCHA.ForeColor = System.Drawing.Color.Black
        Me.chkResCHA.Location = New System.Drawing.Point(181, 126)
        Me.chkResCHA.Name = "chkResCHA"
        Me.chkResCHA.Size = New System.Drawing.Size(157, 17)
        Me.chkResCHA.TabIndex = 103
        Me.chkResCHA.Text = "CHAMFER mm x45�"
        Me.chkResCHA.UseVisualStyleBackColor = True
        '
        'txtResRAD
        '
        Me.txtResRAD.BackColor = System.Drawing.Color.White
        Me.txtResRAD.Enabled = False
        Me.txtResRAD.Location = New System.Drawing.Point(302, 101)
        Me.txtResRAD.Name = "txtResRAD"
        Me.txtResRAD.Size = New System.Drawing.Size(36, 20)
        Me.txtResRAD.TabIndex = 102
        '
        'chkResRAD
        '
        Me.chkResRAD.Checked = True
        Me.chkResRAD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResRAD.ForeColor = System.Drawing.Color.Black
        Me.chkResRAD.Location = New System.Drawing.Point(181, 104)
        Me.chkResRAD.Name = "chkResRAD"
        Me.chkResRAD.Size = New System.Drawing.Size(157, 17)
        Me.chkResRAD.TabIndex = 101
        Me.chkResRAD.Text = "R mm"
        Me.chkResRAD.UseVisualStyleBackColor = True
        '
        'chkResDRA
        '
        Me.chkResDRA.Checked = True
        Me.chkResDRA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkResDRA.Location = New System.Drawing.Point(181, 38)
        Me.chkResDRA.Name = "chkResDRA"
        Me.chkResDRA.Size = New System.Drawing.Size(84, 17)
        Me.chkResDRA.TabIndex = 97
        Me.chkResDRA.Text = "DRAFT "
        Me.chkResDRA.UseVisualStyleBackColor = True
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.Black
        Me.Label23.Location = New System.Drawing.Point(13, 3)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(224, 20)
        Me.Label23.TabIndex = 357
        Me.Label23.Text = "MOLDING WORKING SPECS"
        '
        'pnlGomma
        '
        Me.pnlGomma.AutoScroll = True
        Me.pnlGomma.Controls.Add(Me.chkGomSAR)
        Me.pnlGomma.Controls.Add(Me.Label60)
        Me.pnlGomma.Controls.Add(Me.chkGomDNI)
        Me.pnlGomma.Controls.Add(Me.cboGomFLH)
        Me.pnlGomma.Controls.Add(Me.PictureBox15)
        Me.pnlGomma.Controls.Add(Me.PictureBox14)
        Me.pnlGomma.Controls.Add(Me.PictureBox13)
        Me.pnlGomma.Controls.Add(Me.PictureBox12)
        Me.pnlGomma.Controls.Add(Me.PictureBox11)
        Me.pnlGomma.Controls.Add(Me.PictureBox10)
        Me.pnlGomma.Controls.Add(Me.Label57)
        Me.pnlGomma.Controls.Add(Me.Label59)
        Me.pnlGomma.Controls.Add(Me.Label58)
        Me.pnlGomma.Controls.Add(Me.Label55)
        Me.pnlGomma.Controls.Add(Me.Label54)
        Me.pnlGomma.Controls.Add(Me.Label53)
        Me.pnlGomma.Controls.Add(Me.Label52)
        Me.pnlGomma.Controls.Add(Me.Label51)
        Me.pnlGomma.Controls.Add(Me.Label50)
        Me.pnlGomma.Controls.Add(Me.Label49)
        Me.pnlGomma.Controls.Add(Me.PictureBox9)
        Me.pnlGomma.Controls.Add(Me.PictureBox7)
        Me.pnlGomma.Controls.Add(Me.txtGomHRA)
        Me.pnlGomma.Controls.Add(Me.chkGomHGR)
        Me.pnlGomma.Controls.Add(Me.chkGomHAI)
        Me.pnlGomma.Controls.Add(Me.txtGomSHA)
        Me.pnlGomma.Controls.Add(Me.txtGomSHD)
        Me.pnlGomma.Controls.Add(Me.chkGomHAR)
        Me.pnlGomma.Controls.Add(Me.chkGomMEA)
        Me.pnlGomma.Controls.Add(Me.chkGomCLE)
        Me.pnlGomma.Controls.Add(Me.chkGomHOS)
        Me.pnlGomma.Controls.Add(Me.txtGomTRA)
        Me.pnlGomma.Controls.Add(Me.chkGomGTR)
        Me.pnlGomma.Controls.Add(Me.txtGomTOR)
        Me.pnlGomma.Controls.Add(Me.chkGomGTO)
        Me.pnlGomma.Controls.Add(Me.txtGomAPU)
        Me.pnlGomma.Controls.Add(Me.chkGomASP)
        Me.pnlGomma.Controls.Add(Me.txtGomATO)
        Me.pnlGomma.Controls.Add(Me.chkGomAST)
        Me.pnlGomma.Controls.Add(Me.txtGomMGT)
        Me.pnlGomma.Controls.Add(Me.chkGomMAR)
        Me.pnlGomma.Controls.Add(Me.chkGomFLA)
        Me.pnlGomma.Location = New System.Drawing.Point(1305, 158)
        Me.pnlGomma.Name = "pnlGomma"
        Me.pnlGomma.Size = New System.Drawing.Size(540, 488)
        Me.pnlGomma.TabIndex = 318
        Me.pnlGomma.Visible = False
        '
        'chkGomSAR
        '
        Me.chkGomSAR.Checked = True
        Me.chkGomSAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomSAR.ForeColor = System.Drawing.Color.Black
        Me.chkGomSAR.Location = New System.Drawing.Point(179, 327)
        Me.chkGomSAR.Name = "chkGomSAR"
        Me.chkGomSAR.Size = New System.Drawing.Size(192, 17)
        Me.chkGomSAR.TabIndex = 358
        Me.chkGomSAR.Text = "ARCA 'RT0041' COSMETIC QUALITY"
        Me.chkGomSAR.UseVisualStyleBackColor = True
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.ForeColor = System.Drawing.Color.Black
        Me.Label60.Location = New System.Drawing.Point(12, 11)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(218, 20)
        Me.Label60.TabIndex = 357
        Me.Label60.Text = "RUBBER WORKING SPECS"
        '
        'chkGomDNI
        '
        Me.chkGomDNI.Checked = True
        Me.chkGomDNI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGomDNI.Enabled = False
        Me.chkGomDNI.Location = New System.Drawing.Point(179, 139)
        Me.chkGomDNI.Name = "chkGomDNI"
        Me.chkGomDNI.Size = New System.Drawing.Size(199, 17)
        Me.chkGomDNI.TabIndex = 68
        Me.chkGomDNI.Text = "REFER TO THE 3D DRAWING"
        Me.chkGomDNI.UseVisualStyleBackColor = True
        '
        'cboGomFLH
        '
        Me.cboGomFLH.AutoCompleteCustomSource.AddRange(New String() {"0,05", "0,10", "0,15", "0,20"})
        Me.cboGomFLH.Enabled = False
        Me.cboGomFLH.FormattingEnabled = True
        Me.cboGomFLH.Items.AddRange(New Object() {"0,05", "0,10", "0,15", "0,20"})
        Me.cboGomFLH.Location = New System.Drawing.Point(271, 47)
        Me.cboGomFLH.Name = "cboGomFLH"
        Me.cboGomFLH.Size = New System.Drawing.Size(65, 21)
        Me.cboGomFLH.TabIndex = 61
        '
        'PictureBox15
        '
        Me.PictureBox15.BackColor = System.Drawing.Color.Black
        Me.PictureBox15.Location = New System.Drawing.Point(9, 370)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox15.TabIndex = 106
        Me.PictureBox15.TabStop = False
        '
        'PictureBox14
        '
        Me.PictureBox14.BackColor = System.Drawing.Color.Black
        Me.PictureBox14.Location = New System.Drawing.Point(9, 348)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox14.TabIndex = 105
        Me.PictureBox14.TabStop = False
        '
        'PictureBox13
        '
        Me.PictureBox13.BackColor = System.Drawing.Color.Black
        Me.PictureBox13.Location = New System.Drawing.Point(9, 226)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox13.TabIndex = 104
        Me.PictureBox13.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Black
        Me.PictureBox12.Location = New System.Drawing.Point(9, 203)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox12.TabIndex = 103
        Me.PictureBox12.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Black
        Me.PictureBox11.Location = New System.Drawing.Point(9, 157)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox11.TabIndex = 102
        Me.PictureBox11.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.Black
        Me.PictureBox10.Location = New System.Drawing.Point(9, 135)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox10.TabIndex = 101
        Me.PictureBox10.TabStop = False
        '
        'Label57
        '
        Me.Label57.Location = New System.Drawing.Point(195, 254)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(335, 13)
        Me.Label57.TabIndex = 91
        Me.Label57.Text = "SCRATCH NOCK NOT PERMITTED"
        '
        'Label59
        '
        Me.Label59.Location = New System.Drawing.Point(12, 376)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(93, 13)
        Me.Label59.TabIndex = 93
        Me.Label59.Text = "MEASURING"
        '
        'Label58
        '
        Me.Label58.Location = New System.Drawing.Point(12, 354)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(82, 13)
        Me.Label58.TabIndex = 100
        Me.Label58.Text = "CLEANING"
        '
        'Label55
        '
        Me.Label55.Location = New System.Drawing.Point(12, 231)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(144, 33)
        Me.Label55.TabIndex = 96
        Me.Label55.Text = "SURFACE ""-- - -- -"""
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(12, 207)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(88, 13)
        Me.Label54.TabIndex = 97
        Me.Label54.Text = "HARDNESS"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(12, 163)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(33, 13)
        Me.Label53.TabIndex = 89
        Me.Label53.Text = "GRIP"
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(12, 140)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(176, 13)
        Me.Label52.TabIndex = 90
        Me.Label52.Text = "DIMENSION NOT INDICATED"
        '
        'Label51
        '
        Me.Label51.Location = New System.Drawing.Point(12, 96)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(97, 13)
        Me.Label51.TabIndex = 94
        Me.Label51.Text = "ASSEMBLING"
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(12, 74)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(59, 13)
        Me.Label50.TabIndex = 99
        Me.Label50.Text = "MARK"
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(12, 52)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(97, 13)
        Me.Label49.TabIndex = 98
        Me.Label49.Text = "FLASH"
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.Black
        Me.PictureBox9.Location = New System.Drawing.Point(9, 91)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox9.TabIndex = 87
        Me.PictureBox9.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.Black
        Me.PictureBox7.Location = New System.Drawing.Point(9, 69)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(530, 1)
        Me.PictureBox7.TabIndex = 88
        Me.PictureBox7.TabStop = False
        '
        'txtGomHRA
        '
        Me.txtGomHRA.BackColor = System.Drawing.Color.White
        Me.txtGomHRA.Enabled = False
        Me.txtGomHRA.Location = New System.Drawing.Point(293, 298)
        Me.txtGomHRA.Name = "txtGomHRA"
        Me.txtGomHRA.Size = New System.Drawing.Size(36, 20)
        Me.txtGomHRA.TabIndex = 79
        '
        'chkGomHGR
        '
        Me.chkGomHGR.Checked = True
        Me.chkGomHGR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomHGR.ForeColor = System.Drawing.Color.Black
        Me.chkGomHGR.Location = New System.Drawing.Point(179, 301)
        Me.chkGomHGR.Name = "chkGomHGR"
        Me.chkGomHGR.Size = New System.Drawing.Size(150, 17)
        Me.chkGomHGR.TabIndex = 78
        Me.chkGomHGR.Text = "GRINDING Ra um"
        Me.chkGomHGR.UseVisualStyleBackColor = True
        '
        'chkGomHAI
        '
        Me.chkGomHAI.Checked = True
        Me.chkGomHAI.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomHAI.ForeColor = System.Drawing.Color.Black
        Me.chkGomHAI.Location = New System.Drawing.Point(179, 278)
        Me.chkGomHAI.Name = "chkGomHAI"
        Me.chkGomHAI.Size = New System.Drawing.Size(228, 17)
        Me.chkGomHAI.TabIndex = 77
        Me.chkGomHAI.Text = "AIR ENTRAPPED NOT TOLERATED"
        Me.chkGomHAI.UseVisualStyleBackColor = True
        '
        'txtGomSHA
        '
        Me.txtGomSHA.BackColor = System.Drawing.Color.White
        Me.txtGomSHA.Enabled = False
        Me.txtGomSHA.Location = New System.Drawing.Point(198, 205)
        Me.txtGomSHA.Name = "txtGomSHA"
        Me.txtGomSHA.Size = New System.Drawing.Size(36, 20)
        Me.txtGomSHA.TabIndex = 74
        '
        'txtGomSHD
        '
        Me.txtGomSHD.BackColor = System.Drawing.Color.White
        Me.txtGomSHD.Enabled = False
        Me.txtGomSHD.Location = New System.Drawing.Point(288, 205)
        Me.txtGomSHD.Name = "txtGomSHD"
        Me.txtGomSHD.Size = New System.Drawing.Size(36, 20)
        Me.txtGomSHD.TabIndex = 75
        '
        'chkGomHAR
        '
        Me.chkGomHAR.Checked = True
        Me.chkGomHAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomHAR.ForeColor = System.Drawing.Color.Black
        Me.chkGomHAR.Location = New System.Drawing.Point(179, 208)
        Me.chkGomHAR.Name = "chkGomHAR"
        Me.chkGomHAR.Size = New System.Drawing.Size(213, 17)
        Me.chkGomHAR.TabIndex = 73
        Me.chkGomHAR.Text = "             ShA/1""                  ShD/1"""
        Me.chkGomHAR.UseVisualStyleBackColor = True
        '
        'chkGomMEA
        '
        Me.chkGomMEA.Checked = True
        Me.chkGomMEA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGomMEA.Enabled = False
        Me.chkGomMEA.Location = New System.Drawing.Point(179, 375)
        Me.chkGomMEA.Name = "chkGomMEA"
        Me.chkGomMEA.Size = New System.Drawing.Size(211, 17)
        Me.chkGomMEA.TabIndex = 81
        Me.chkGomMEA.Text = "LEAVE AT 20 � 5� FOR 4 HOURS"
        Me.chkGomMEA.UseVisualStyleBackColor = True
        '
        'chkGomCLE
        '
        Me.chkGomCLE.Checked = True
        Me.chkGomCLE.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGomCLE.Enabled = False
        Me.chkGomCLE.Location = New System.Drawing.Point(179, 352)
        Me.chkGomCLE.Name = "chkGomCLE"
        Me.chkGomCLE.Size = New System.Drawing.Size(238, 17)
        Me.chkGomCLE.TabIndex = 80
        Me.chkGomCLE.Text = "DIRT, OIL, GREASE NOT PERMITTED"
        Me.chkGomCLE.UseVisualStyleBackColor = True
        '
        'chkGomHOS
        '
        Me.chkGomHOS.Checked = True
        Me.chkGomHOS.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomHOS.ForeColor = System.Drawing.Color.Black
        Me.chkGomHOS.Location = New System.Drawing.Point(179, 230)
        Me.chkGomHOS.Name = "chkGomHOS"
        Me.chkGomHOS.Size = New System.Drawing.Size(351, 17)
        Me.chkGomHOS.TabIndex = 76
        Me.chkGomHOS.Text = "DEPRESSION, FLASH, INCOMPLETION, NOTCH,"
        Me.chkGomHOS.UseVisualStyleBackColor = True
        '
        'txtGomTRA
        '
        Me.txtGomTRA.BackColor = System.Drawing.Color.White
        Me.txtGomTRA.Enabled = False
        Me.txtGomTRA.Location = New System.Drawing.Point(350, 182)
        Me.txtGomTRA.Name = "txtGomTRA"
        Me.txtGomTRA.Size = New System.Drawing.Size(36, 20)
        Me.txtGomTRA.TabIndex = 72
        '
        'chkGomGTR
        '
        Me.chkGomGTR.Checked = True
        Me.chkGomGTR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomGTR.ForeColor = System.Drawing.Color.Black
        Me.chkGomGTR.Location = New System.Drawing.Point(179, 185)
        Me.chkGomGTR.Name = "chkGomGTR"
        Me.chkGomGTR.Size = New System.Drawing.Size(195, 17)
        Me.chkGomGTR.TabIndex = 71
        Me.chkGomGTR.Text = "TRACTION STRENGTH N min"
        Me.chkGomGTR.UseVisualStyleBackColor = True
        '
        'txtGomTOR
        '
        Me.txtGomTOR.BackColor = System.Drawing.Color.White
        Me.txtGomTOR.Enabled = False
        Me.txtGomTOR.Location = New System.Drawing.Point(350, 159)
        Me.txtGomTOR.Name = "txtGomTOR"
        Me.txtGomTOR.Size = New System.Drawing.Size(36, 20)
        Me.txtGomTOR.TabIndex = 70
        '
        'chkGomGTO
        '
        Me.chkGomGTO.Checked = True
        Me.chkGomGTO.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomGTO.ForeColor = System.Drawing.Color.Black
        Me.chkGomGTO.Location = New System.Drawing.Point(179, 162)
        Me.chkGomGTO.Name = "chkGomGTO"
        Me.chkGomGTO.Size = New System.Drawing.Size(195, 17)
        Me.chkGomGTO.TabIndex = 69
        Me.chkGomGTO.Text = "TORSION STRENGTH Nm"
        Me.chkGomGTO.UseVisualStyleBackColor = True
        '
        'txtGomAPU
        '
        Me.txtGomAPU.BackColor = System.Drawing.Color.White
        Me.txtGomAPU.Enabled = False
        Me.txtGomAPU.Location = New System.Drawing.Point(335, 114)
        Me.txtGomAPU.Name = "txtGomAPU"
        Me.txtGomAPU.Size = New System.Drawing.Size(36, 20)
        Me.txtGomAPU.TabIndex = 67
        '
        'chkGomASP
        '
        Me.chkGomASP.Checked = True
        Me.chkGomASP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomASP.ForeColor = System.Drawing.Color.Black
        Me.chkGomASP.Location = New System.Drawing.Point(179, 117)
        Me.chkGomASP.Name = "chkGomASP"
        Me.chkGomASP.Size = New System.Drawing.Size(192, 17)
        Me.chkGomASP.TabIndex = 66
        Me.chkGomASP.Text = "PULL-OUT STRENGTH N"
        Me.chkGomASP.UseVisualStyleBackColor = True
        '
        'txtGomATO
        '
        Me.txtGomATO.BackColor = System.Drawing.Color.White
        Me.txtGomATO.Enabled = False
        Me.txtGomATO.Location = New System.Drawing.Point(335, 92)
        Me.txtGomATO.Name = "txtGomATO"
        Me.txtGomATO.Size = New System.Drawing.Size(36, 20)
        Me.txtGomATO.TabIndex = 65
        '
        'chkGomAST
        '
        Me.chkGomAST.Checked = True
        Me.chkGomAST.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomAST.ForeColor = System.Drawing.Color.Black
        Me.chkGomAST.Location = New System.Drawing.Point(179, 95)
        Me.chkGomAST.Name = "chkGomAST"
        Me.chkGomAST.Size = New System.Drawing.Size(192, 17)
        Me.chkGomAST.TabIndex = 64
        Me.chkGomAST.Text = "TORSION STRENGTH Nm"
        Me.chkGomAST.UseVisualStyleBackColor = True
        '
        'txtGomMGT
        '
        Me.txtGomMGT.BackColor = System.Drawing.Color.White
        Me.txtGomMGT.Enabled = False
        Me.txtGomMGT.Location = New System.Drawing.Point(271, 70)
        Me.txtGomMGT.Name = "txtGomMGT"
        Me.txtGomMGT.Size = New System.Drawing.Size(36, 20)
        Me.txtGomMGT.TabIndex = 63
        '
        'chkGomMAR
        '
        Me.chkGomMAR.Checked = True
        Me.chkGomMAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomMAR.ForeColor = System.Drawing.Color.Black
        Me.chkGomMAR.Location = New System.Drawing.Point(179, 73)
        Me.chkGomMAR.Name = "chkGomMAR"
        Me.chkGomMAR.Size = New System.Drawing.Size(135, 17)
        Me.chkGomMAR.TabIndex = 62
        Me.chkGomMAR.Text = "GT MARK mm"
        Me.chkGomMAR.UseVisualStyleBackColor = True
        '
        'chkGomFLA
        '
        Me.chkGomFLA.Checked = True
        Me.chkGomFLA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkGomFLA.Location = New System.Drawing.Point(179, 51)
        Me.chkGomFLA.Name = "chkGomFLA"
        Me.chkGomFLA.Size = New System.Drawing.Size(157, 17)
        Me.chkGomFLA.TabIndex = 60
        Me.chkGomFLA.Text = "FLASH mm"
        Me.chkGomFLA.UseVisualStyleBackColor = True
        '
        'pnlFresatura
        '
        Me.pnlFresatura.AutoScroll = True
        Me.pnlFresatura.Controls.Add(Me.chkFreSAR)
        Me.pnlFresatura.Controls.Add(Me.cboFreSEL)
        Me.pnlFresatura.Controls.Add(Me.PictureBox33)
        Me.pnlFresatura.Controls.Add(Me.cboFreSEU)
        Me.pnlFresatura.Controls.Add(Me.Label44)
        Me.pnlFresatura.Controls.Add(Me.chkFreBEC)
        Me.pnlFresatura.Controls.Add(Me.chkFreBER)
        Me.pnlFresatura.Controls.Add(Me.PictureBox79)
        Me.pnlFresatura.Controls.Add(Me.PictureBox84)
        Me.pnlFresatura.Controls.Add(Me.PictureBox85)
        Me.pnlFresatura.Controls.Add(Me.PictureBox86)
        Me.pnlFresatura.Controls.Add(Me.chkFreSEN)
        Me.pnlFresatura.Controls.Add(Me.PictureBox26)
        Me.pnlFresatura.Controls.Add(Me.PictureBox16)
        Me.pnlFresatura.Controls.Add(Me.Label130)
        Me.pnlFresatura.Controls.Add(Me.Label24)
        Me.pnlFresatura.Controls.Add(Me.Label33)
        Me.pnlFresatura.Controls.Add(Me.Label34)
        Me.pnlFresatura.Controls.Add(Me.Label40)
        Me.pnlFresatura.Controls.Add(Me.Label61)
        Me.pnlFresatura.Controls.Add(Me.Label62)
        Me.pnlFresatura.Controls.Add(Me.Label63)
        Me.pnlFresatura.Controls.Add(Me.Label84)
        Me.pnlFresatura.Controls.Add(Me.Label124)
        Me.pnlFresatura.Controls.Add(Me.Label125)
        Me.pnlFresatura.Controls.Add(Me.PictureBox65)
        Me.pnlFresatura.Controls.Add(Me.PictureBox69)
        Me.pnlFresatura.Controls.Add(Me.PictureBox71)
        Me.pnlFresatura.Controls.Add(Me.PictureBox72)
        Me.pnlFresatura.Controls.Add(Me.PictureBox76)
        Me.pnlFresatura.Controls.Add(Me.PictureBox80)
        Me.pnlFresatura.Controls.Add(Me.PictureBox81)
        Me.pnlFresatura.Controls.Add(Me.cboFreTTG)
        Me.pnlFresatura.Controls.Add(Me.PictureBox82)
        Me.pnlFresatura.Controls.Add(Me.Label128)
        Me.pnlFresatura.Controls.Add(Me.chkFreTTG)
        Me.pnlFresatura.Controls.Add(Me.chkFreCLD)
        Me.pnlFresatura.Controls.Add(Me.chkFreHAD)
        Me.pnlFresatura.Controls.Add(Me.chkFreDCL)
        Me.pnlFresatura.Controls.Add(Me.chkFreDCA)
        Me.pnlFresatura.Controls.Add(Me.chkFreDNI)
        Me.pnlFresatura.Controls.Add(Me.chkFrePFD)
        Me.pnlFresatura.Controls.Add(Me.chkFreRWP)
        Me.pnlFresatura.Controls.Add(Me.chkFreTBN)
        Me.pnlFresatura.Controls.Add(Me.chkFreTRF)
        Me.pnlFresatura.Controls.Add(Me.txtFreIPP)
        Me.pnlFresatura.Controls.Add(Me.chkFreIPP)
        Me.pnlFresatura.Controls.Add(Me.txtFreIPT)
        Me.pnlFresatura.Controls.Add(Me.chkFreIPT)
        Me.pnlFresatura.Controls.Add(Me.txtFreCHA)
        Me.pnlFresatura.Controls.Add(Me.chkFreCHA)
        Me.pnlFresatura.Controls.Add(Me.chkFreBHF)
        Me.pnlFresatura.Controls.Add(Me.txtFreRAD)
        Me.pnlFresatura.Controls.Add(Me.chkFreRAD)
        Me.pnlFresatura.Controls.Add(Me.chkFreBHM)
        Me.pnlFresatura.Location = New System.Drawing.Point(1182, 270)
        Me.pnlFresatura.Name = "pnlFresatura"
        Me.pnlFresatura.Size = New System.Drawing.Size(540, 535)
        Me.pnlFresatura.TabIndex = 319
        Me.pnlFresatura.Visible = False
        '
        'chkFreSAR
        '
        Me.chkFreSAR.Checked = True
        Me.chkFreSAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreSAR.Location = New System.Drawing.Point(170, 483)
        Me.chkFreSAR.Name = "chkFreSAR"
        Me.chkFreSAR.Size = New System.Drawing.Size(250, 17)
        Me.chkFreSAR.TabIndex = 432
        Me.chkFreSAR.Text = "ARCA 'RT0041' COSMETIC QUALITY"
        Me.chkFreSAR.UseVisualStyleBackColor = True
        '
        'cboFreSEL
        '
        Me.cboFreSEL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboFreSEL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFreSEL.Enabled = False
        Me.cboFreSEL.FormattingEnabled = True
        Me.cboFreSEL.Items.AddRange(New Object() {"0,10", "0,15", "0,20", "0,25", "0,30", "0,35", "0,40", "0,40", "0,45", "0,50"})
        Me.cboFreSEL.Location = New System.Drawing.Point(269, 146)
        Me.cboFreSEL.Name = "cboFreSEL"
        Me.cboFreSEL.Size = New System.Drawing.Size(63, 21)
        Me.cboFreSEL.Sorted = True
        Me.cboFreSEL.TabIndex = 431
        '
        'PictureBox33
        '
        Me.PictureBox33.BackColor = System.Drawing.Color.Black
        Me.PictureBox33.Location = New System.Drawing.Point(338, 97)
        Me.PictureBox33.Name = "PictureBox33"
        Me.PictureBox33.Size = New System.Drawing.Size(1, 55)
        Me.PictureBox33.TabIndex = 430
        Me.PictureBox33.TabStop = False
        '
        'cboFreSEU
        '
        Me.cboFreSEU.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboFreSEU.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboFreSEU.Enabled = False
        Me.cboFreSEU.FormattingEnabled = True
        Me.cboFreSEU.Items.AddRange(New Object() {"0,10", "0,15", "0,20", "0,25", "0,30", "0,35", "0,40", "0,40", "0,45", "0,50"})
        Me.cboFreSEU.Location = New System.Drawing.Point(269, 124)
        Me.cboFreSEU.Name = "cboFreSEU"
        Me.cboFreSEU.Size = New System.Drawing.Size(63, 21)
        Me.cboFreSEU.Sorted = True
        Me.cboFreSEU.TabIndex = 429
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(396, 155)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(58, 13)
        Me.Label44.TabIndex = 426
        Me.Label44.Text = "ISO 13715"
        '
        'chkFreBEC
        '
        Me.chkFreBEC.Checked = True
        Me.chkFreBEC.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreBEC.Enabled = False
        Me.chkFreBEC.ForeColor = System.Drawing.Color.Black
        Me.chkFreBEC.Location = New System.Drawing.Point(355, 129)
        Me.chkFreBEC.Name = "chkFreBEC"
        Me.chkFreBEC.Size = New System.Drawing.Size(104, 17)
        Me.chkFreBEC.TabIndex = 425
        Me.chkFreBEC.Text = "CHAMFER x45�"
        Me.chkFreBEC.UseVisualStyleBackColor = True
        '
        'chkFreBER
        '
        Me.chkFreBER.Checked = True
        Me.chkFreBER.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreBER.Enabled = False
        Me.chkFreBER.ForeColor = System.Drawing.Color.Black
        Me.chkFreBER.Location = New System.Drawing.Point(355, 101)
        Me.chkFreBER.Name = "chkFreBER"
        Me.chkFreBER.Size = New System.Drawing.Size(70, 17)
        Me.chkFreBER.TabIndex = 424
        Me.chkFreBER.Text = "ROUND"
        Me.chkFreBER.UseVisualStyleBackColor = True
        '
        'PictureBox79
        '
        Me.PictureBox79.BackColor = System.Drawing.Color.Black
        Me.PictureBox79.Location = New System.Drawing.Point(338, 151)
        Me.PictureBox79.Name = "PictureBox79"
        Me.PictureBox79.Size = New System.Drawing.Size(178, 1)
        Me.PictureBox79.TabIndex = 423
        Me.PictureBox79.TabStop = False
        '
        'PictureBox84
        '
        Me.PictureBox84.BackColor = System.Drawing.Color.Black
        Me.PictureBox84.Location = New System.Drawing.Point(338, 123)
        Me.PictureBox84.Name = "PictureBox84"
        Me.PictureBox84.Size = New System.Drawing.Size(178, 1)
        Me.PictureBox84.TabIndex = 422
        Me.PictureBox84.TabStop = False
        '
        'PictureBox85
        '
        Me.PictureBox85.BackColor = System.Drawing.Color.Black
        Me.PictureBox85.Location = New System.Drawing.Point(1, 173)
        Me.PictureBox85.Name = "PictureBox85"
        Me.PictureBox85.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox85.TabIndex = 421
        Me.PictureBox85.TabStop = False
        '
        'PictureBox86
        '
        Me.PictureBox86.Image = CType(resources.GetObject("PictureBox86.Image"), System.Drawing.Image)
        Me.PictureBox86.Location = New System.Drawing.Point(171, 120)
        Me.PictureBox86.Name = "PictureBox86"
        Me.PictureBox86.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox86.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox86.TabIndex = 420
        Me.PictureBox86.TabStop = False
        '
        'chkFreSEN
        '
        Me.chkFreSEN.Checked = True
        Me.chkFreSEN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreSEN.Location = New System.Drawing.Point(171, 97)
        Me.chkFreSEN.Name = "chkFreSEN"
        Me.chkFreSEN.Size = New System.Drawing.Size(165, 17)
        Me.chkFreSEN.TabIndex = 366
        Me.chkFreSEN.Text = "SHARPE EDGE"
        Me.chkFreSEN.UseVisualStyleBackColor = True
        '
        'PictureBox26
        '
        Me.PictureBox26.BackColor = System.Drawing.Color.Black
        Me.PictureBox26.Location = New System.Drawing.Point(1, 376)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox26.TabIndex = 365
        Me.PictureBox26.TabStop = False
        '
        'PictureBox16
        '
        Me.PictureBox16.BackColor = System.Drawing.Color.Black
        Me.PictureBox16.Location = New System.Drawing.Point(1, 508)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox16.TabIndex = 362
        Me.PictureBox16.TabStop = False
        '
        'Label130
        '
        Me.Label130.AutoSize = True
        Me.Label130.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label130.ForeColor = System.Drawing.Color.Black
        Me.Label130.Location = New System.Drawing.Point(3, 6)
        Me.Label130.Name = "Label130"
        Me.Label130.Size = New System.Drawing.Size(214, 20)
        Me.Label130.TabIndex = 359
        Me.Label130.Text = "MILLING WORKING SPECS"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(3, 514)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(61, 13)
        Me.Label24.TabIndex = 350
        Me.Label24.Text = "CLEANING"
        '
        'Label33
        '
        Me.Label33.Location = New System.Drawing.Point(3, 448)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(126, 34)
        Me.Label33.TabIndex = 349
        Me.Label33.Text = "SURFACE"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(3, 402)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(126, 13)
        Me.Label34.TabIndex = 348
        Me.Label34.Text = "DIMENSION CHECKING"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(3, 382)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(155, 13)
        Me.Label40.TabIndex = 347
        Me.Label40.Text = "DIMENSION NOT INDICATED"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(3, 358)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(88, 13)
        Me.Label61.TabIndex = 346
        Me.Label61.Text = "PRESS-FITTING"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(3, 336)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(65, 13)
        Me.Label62.TabIndex = 345
        Me.Label62.Text = "RIVETTING"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(3, 270)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(54, 13)
        Me.Label63.TabIndex = 344
        Me.Label63.Text = "TAPPING"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(3, 225)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(71, 13)
        Me.Label84.TabIndex = 343
        Me.Label84.Text = "INSERT, PIN"
        '
        'Label124
        '
        Me.Label124.AutoSize = True
        Me.Label124.Location = New System.Drawing.Point(3, 203)
        Me.Label124.Name = "Label124"
        Me.Label124.Size = New System.Drawing.Size(59, 13)
        Me.Label124.TabIndex = 342
        Me.Label124.Text = "CHAMFER"
        '
        'Label125
        '
        Me.Label125.AutoSize = True
        Me.Label125.Location = New System.Drawing.Point(3, 179)
        Me.Label125.Name = "Label125"
        Me.Label125.Size = New System.Drawing.Size(36, 13)
        Me.Label125.TabIndex = 341
        Me.Label125.Text = "RADII"
        '
        'PictureBox65
        '
        Me.PictureBox65.BackColor = System.Drawing.Color.Black
        Me.PictureBox65.Location = New System.Drawing.Point(1, 443)
        Me.PictureBox65.Name = "PictureBox65"
        Me.PictureBox65.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox65.TabIndex = 339
        Me.PictureBox65.TabStop = False
        '
        'PictureBox69
        '
        Me.PictureBox69.BackColor = System.Drawing.Color.Black
        Me.PictureBox69.Location = New System.Drawing.Point(0, 399)
        Me.PictureBox69.Name = "PictureBox69"
        Me.PictureBox69.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox69.TabIndex = 338
        Me.PictureBox69.TabStop = False
        '
        'PictureBox71
        '
        Me.PictureBox71.BackColor = System.Drawing.Color.Black
        Me.PictureBox71.Location = New System.Drawing.Point(1, 331)
        Me.PictureBox71.Name = "PictureBox71"
        Me.PictureBox71.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox71.TabIndex = 337
        Me.PictureBox71.TabStop = False
        '
        'PictureBox72
        '
        Me.PictureBox72.BackColor = System.Drawing.Color.Black
        Me.PictureBox72.Location = New System.Drawing.Point(1, 353)
        Me.PictureBox72.Name = "PictureBox72"
        Me.PictureBox72.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox72.TabIndex = 336
        Me.PictureBox72.TabStop = False
        '
        'PictureBox76
        '
        Me.PictureBox76.BackColor = System.Drawing.Color.Black
        Me.PictureBox76.Location = New System.Drawing.Point(1, 265)
        Me.PictureBox76.Name = "PictureBox76"
        Me.PictureBox76.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox76.TabIndex = 334
        Me.PictureBox76.TabStop = False
        '
        'PictureBox80
        '
        Me.PictureBox80.BackColor = System.Drawing.Color.Black
        Me.PictureBox80.Location = New System.Drawing.Point(1, 219)
        Me.PictureBox80.Name = "PictureBox80"
        Me.PictureBox80.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox80.TabIndex = 333
        Me.PictureBox80.TabStop = False
        '
        'PictureBox81
        '
        Me.PictureBox81.BackColor = System.Drawing.Color.Black
        Me.PictureBox81.Location = New System.Drawing.Point(1, 197)
        Me.PictureBox81.Name = "PictureBox81"
        Me.PictureBox81.Size = New System.Drawing.Size(515, 1)
        Me.PictureBox81.TabIndex = 332
        Me.PictureBox81.TabStop = False
        '
        'cboFreTTG
        '
        Me.cboFreTTG.FormattingEnabled = True
        Me.cboFreTTG.Items.AddRange(New Object() {"6H"})
        Me.cboFreTTG.Location = New System.Drawing.Point(300, 309)
        Me.cboFreTTG.Name = "cboFreTTG"
        Me.cboFreTTG.Size = New System.Drawing.Size(48, 21)
        Me.cboFreTTG.TabIndex = 320
        Me.cboFreTTG.Text = "6H"
        '
        'PictureBox82
        '
        Me.PictureBox82.BackColor = System.Drawing.Color.Black
        Me.PictureBox82.Location = New System.Drawing.Point(338, 96)
        Me.PictureBox82.Name = "PictureBox82"
        Me.PictureBox82.Size = New System.Drawing.Size(178, 1)
        Me.PictureBox82.TabIndex = 330
        Me.PictureBox82.TabStop = False
        '
        'Label128
        '
        Me.Label128.AutoSize = True
        Me.Label128.Location = New System.Drawing.Point(4, 55)
        Me.Label128.Name = "Label128"
        Me.Label128.Size = New System.Drawing.Size(82, 13)
        Me.Label128.TabIndex = 329
        Me.Label128.Text = "BURR HEIGHT"
        '
        'chkFreTTG
        '
        Me.chkFreTTG.Checked = True
        Me.chkFreTTG.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreTTG.ForeColor = System.Drawing.Color.Black
        Me.chkFreTTG.Location = New System.Drawing.Point(170, 313)
        Me.chkFreTTG.Name = "chkFreTTG"
        Me.chkFreTTG.Size = New System.Drawing.Size(165, 17)
        Me.chkFreTTG.TabIndex = 319
        Me.chkFreTTG.Text = "TOLERANCE GRADE"
        Me.chkFreTTG.UseVisualStyleBackColor = True
        '
        'chkFreCLD
        '
        Me.chkFreCLD.Checked = True
        Me.chkFreCLD.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFreCLD.Enabled = False
        Me.chkFreCLD.Location = New System.Drawing.Point(170, 513)
        Me.chkFreCLD.Name = "chkFreCLD"
        Me.chkFreCLD.Size = New System.Drawing.Size(250, 17)
        Me.chkFreCLD.TabIndex = 328
        Me.chkFreCLD.Text = "DIRT, OIL, GREASE NOT PERMITTED"
        Me.chkFreCLD.UseVisualStyleBackColor = True
        '
        'chkFreHAD
        '
        Me.chkFreHAD.Checked = True
        Me.chkFreHAD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreHAD.ForeColor = System.Drawing.Color.Black
        Me.chkFreHAD.Location = New System.Drawing.Point(170, 447)
        Me.chkFreHAD.Name = "chkFreHAD"
        Me.chkFreHAD.Size = New System.Drawing.Size(339, 37)
        Me.chkFreHAD.TabIndex = 327
        Me.chkFreHAD.Text = "ON SURFACE IDENTIFIED WITH ""-- - --"" BURR, NOTCH AND SCRATCH NICK NOT PERMITTED"
        Me.chkFreHAD.UseVisualStyleBackColor = True
        '
        'chkFreDCL
        '
        Me.chkFreDCL.Checked = True
        Me.chkFreDCL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFreDCL.Enabled = False
        Me.chkFreDCL.Location = New System.Drawing.Point(170, 425)
        Me.chkFreDCL.Name = "chkFreDCL"
        Me.chkFreDCL.Size = New System.Drawing.Size(234, 17)
        Me.chkFreDCL.TabIndex = 326
        Me.chkFreDCL.Text = "LEAVE AT 20� � 5�C FOR 4 HOURS"
        Me.chkFreDCL.UseVisualStyleBackColor = True
        '
        'chkFreDCA
        '
        Me.chkFreDCA.Checked = True
        Me.chkFreDCA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFreDCA.Enabled = False
        Me.chkFreDCA.Location = New System.Drawing.Point(170, 403)
        Me.chkFreDCA.Name = "chkFreDCA"
        Me.chkFreDCA.Size = New System.Drawing.Size(186, 17)
        Me.chkFreDCA.TabIndex = 325
        Me.chkFreDCA.Text = "ALL AFTER TREATMENT"
        Me.chkFreDCA.UseVisualStyleBackColor = True
        '
        'chkFreDNI
        '
        Me.chkFreDNI.Checked = True
        Me.chkFreDNI.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFreDNI.Enabled = False
        Me.chkFreDNI.Location = New System.Drawing.Point(170, 381)
        Me.chkFreDNI.Name = "chkFreDNI"
        Me.chkFreDNI.Size = New System.Drawing.Size(211, 17)
        Me.chkFreDNI.TabIndex = 324
        Me.chkFreDNI.Text = "REFER TO THE 3D DRAWING"
        Me.chkFreDNI.UseVisualStyleBackColor = True
        '
        'chkFrePFD
        '
        Me.chkFrePFD.Checked = True
        Me.chkFrePFD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFrePFD.ForeColor = System.Drawing.Color.Black
        Me.chkFrePFD.Location = New System.Drawing.Point(170, 357)
        Me.chkFrePFD.Name = "chkFrePFD"
        Me.chkFrePFD.Size = New System.Drawing.Size(292, 17)
        Me.chkFrePFD.TabIndex = 322
        Me.chkFrePFD.Text = "DRIVE THE PIN OR INSERT UNTIL THE STOP"
        Me.chkFrePFD.UseVisualStyleBackColor = True
        '
        'chkFreRWP
        '
        Me.chkFreRWP.Checked = True
        Me.chkFreRWP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreRWP.ForeColor = System.Drawing.Color.Black
        Me.chkFreRWP.Location = New System.Drawing.Point(170, 335)
        Me.chkFreRWP.Name = "chkFreRWP"
        Me.chkFreRWP.Size = New System.Drawing.Size(250, 17)
        Me.chkFreRWP.TabIndex = 321
        Me.chkFreRWP.Text = "WITHOUT PROTUSION ON SURFACE"
        Me.chkFreRWP.UseVisualStyleBackColor = True
        '
        'chkFreTBN
        '
        Me.chkFreTBN.Checked = True
        Me.chkFreTBN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreTBN.ForeColor = System.Drawing.Color.Black
        Me.chkFreTBN.Location = New System.Drawing.Point(170, 291)
        Me.chkFreTBN.Name = "chkFreTBN"
        Me.chkFreTBN.Size = New System.Drawing.Size(182, 17)
        Me.chkFreTBN.TabIndex = 318
        Me.chkFreTBN.Text = "BURR NOT PERMITTED"
        Me.chkFreTBN.UseVisualStyleBackColor = True
        '
        'chkFreTRF
        '
        Me.chkFreTRF.Checked = True
        Me.chkFreTRF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreTRF.ForeColor = System.Drawing.Color.Black
        Me.chkFreTRF.Location = New System.Drawing.Point(170, 269)
        Me.chkFreTRF.Name = "chkFreTRF"
        Me.chkFreTRF.Size = New System.Drawing.Size(145, 17)
        Me.chkFreTRF.TabIndex = 317
        Me.chkFreTRF.Text = "ROLL FORM TAP"
        Me.chkFreTRF.UseVisualStyleBackColor = True
        '
        'txtFreIPP
        '
        Me.txtFreIPP.BackColor = System.Drawing.Color.White
        Me.txtFreIPP.Enabled = False
        Me.txtFreIPP.Location = New System.Drawing.Point(325, 244)
        Me.txtFreIPP.Name = "txtFreIPP"
        Me.txtFreIPP.Size = New System.Drawing.Size(48, 20)
        Me.txtFreIPP.TabIndex = 316
        '
        'chkFreIPP
        '
        Me.chkFreIPP.Checked = True
        Me.chkFreIPP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreIPP.ForeColor = System.Drawing.Color.Black
        Me.chkFreIPP.Location = New System.Drawing.Point(170, 247)
        Me.chkFreIPP.Name = "chkFreIPP"
        Me.chkFreIPP.Size = New System.Drawing.Size(186, 17)
        Me.chkFreIPP.TabIndex = 315
        Me.chkFreIPP.Text = "PULL-OUT STRENGTH N"
        Me.chkFreIPP.UseVisualStyleBackColor = True
        '
        'txtFreIPT
        '
        Me.txtFreIPT.BackColor = System.Drawing.Color.White
        Me.txtFreIPT.Enabled = False
        Me.txtFreIPT.Location = New System.Drawing.Point(325, 222)
        Me.txtFreIPT.Name = "txtFreIPT"
        Me.txtFreIPT.Size = New System.Drawing.Size(48, 20)
        Me.txtFreIPT.TabIndex = 314
        '
        'chkFreIPT
        '
        Me.chkFreIPT.Checked = True
        Me.chkFreIPT.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreIPT.ForeColor = System.Drawing.Color.Black
        Me.chkFreIPT.Location = New System.Drawing.Point(170, 225)
        Me.chkFreIPT.Name = "chkFreIPT"
        Me.chkFreIPT.Size = New System.Drawing.Size(190, 17)
        Me.chkFreIPT.TabIndex = 313
        Me.chkFreIPT.Text = "TORSION STRENGTH Nm"
        Me.chkFreIPT.UseVisualStyleBackColor = True
        '
        'txtFreCHA
        '
        Me.txtFreCHA.BackColor = System.Drawing.Color.White
        Me.txtFreCHA.Enabled = False
        Me.txtFreCHA.Location = New System.Drawing.Point(264, 198)
        Me.txtFreCHA.Name = "txtFreCHA"
        Me.txtFreCHA.Size = New System.Drawing.Size(36, 20)
        Me.txtFreCHA.TabIndex = 312
        '
        'chkFreCHA
        '
        Me.chkFreCHA.Checked = True
        Me.chkFreCHA.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreCHA.ForeColor = System.Drawing.Color.Black
        Me.chkFreCHA.Location = New System.Drawing.Point(170, 201)
        Me.chkFreCHA.Name = "chkFreCHA"
        Me.chkFreCHA.Size = New System.Drawing.Size(130, 17)
        Me.chkFreCHA.TabIndex = 311
        Me.chkFreCHA.Text = "CHAMFER mm"
        Me.chkFreCHA.UseVisualStyleBackColor = True
        '
        'chkFreBHF
        '
        Me.chkFreBHF.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.chkFreBHF.Checked = True
        Me.chkFreBHF.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreBHF.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkFreBHF.Location = New System.Drawing.Point(171, 76)
        Me.chkFreBHF.Name = "chkFreBHF"
        Me.chkFreBHF.Size = New System.Drawing.Size(194, 17)
        Me.chkFreBHF.TabIndex = 308
        Me.chkFreBHF.Text = "BURR FREE"
        Me.chkFreBHF.UseVisualStyleBackColor = False
        '
        'txtFreRAD
        '
        Me.txtFreRAD.BackColor = System.Drawing.Color.White
        Me.txtFreRAD.Enabled = False
        Me.txtFreRAD.Location = New System.Drawing.Point(222, 176)
        Me.txtFreRAD.Name = "txtFreRAD"
        Me.txtFreRAD.Size = New System.Drawing.Size(36, 20)
        Me.txtFreRAD.TabIndex = 310
        '
        'chkFreRAD
        '
        Me.chkFreRAD.Checked = True
        Me.chkFreRAD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkFreRAD.ForeColor = System.Drawing.Color.Black
        Me.chkFreRAD.Location = New System.Drawing.Point(170, 179)
        Me.chkFreRAD.Name = "chkFreRAD"
        Me.chkFreRAD.Size = New System.Drawing.Size(86, 17)
        Me.chkFreRAD.TabIndex = 309
        Me.chkFreRAD.Text = "R mm"
        Me.chkFreRAD.UseVisualStyleBackColor = True
        '
        'chkFreBHM
        '
        Me.chkFreBHM.Checked = True
        Me.chkFreBHM.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFreBHM.Location = New System.Drawing.Point(171, 54)
        Me.chkFreBHM.Name = "chkFreBHM"
        Me.chkFreBHM.Size = New System.Drawing.Size(250, 17)
        Me.chkFreBHM.TabIndex = 307
        Me.chkFreBHM.Text = "25% METAL THICKNESS TOLERANCE"
        Me.chkFreBHM.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = Global.Attributi_SE.NET.My.Resources.Resources.BannerArcaHR
        Me.PictureBox2.Location = New System.Drawing.Point(-5, -9)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(1130, 125)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 254
        Me.PictureBox2.TabStop = False
        '
        'pnlLogo
        '
        Me.pnlLogo.AutoSize = True
        Me.pnlLogo.BackgroundImage = Global.Attributi_SE.NET.My.Resources.Resources.RS488_logo_ARCA_vertical_lockup_purple
        Me.pnlLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pnlLogo.Location = New System.Drawing.Point(330, 659)
        Me.pnlLogo.Name = "pnlLogo"
        Me.pnlLogo.Size = New System.Drawing.Size(558, 482)
        Me.pnlLogo.TabIndex = 320
        '
        'grpPrincipale
        '
        Me.grpPrincipale.Controls.Add(Me.Label10)
        Me.grpPrincipale.Controls.Add(Me.cboData)
        Me.grpPrincipale.Controls.Add(Me.cmdSlitta)
        Me.grpPrincipale.Controls.Add(Me.cboTTermico)
        Me.grpPrincipale.Controls.Add(Me.cmdReset)
        Me.grpPrincipale.Controls.Add(Me.Label11)
        Me.grpPrincipale.Controls.Add(Me.cmdAzzeraRev)
        Me.grpPrincipale.Controls.Add(Me.cboFamiglia)
        Me.grpPrincipale.Controls.Add(Me.cmdLeggi)
        Me.grpPrincipale.Controls.Add(Me.cmdSalva)
        Me.grpPrincipale.Controls.Add(Me.Label27)
        Me.grpPrincipale.Controls.Add(Me.cmdEsci)
        Me.grpPrincipale.Controls.Add(Me.Label15)
        Me.grpPrincipale.Controls.Add(Me.cboProduttore)
        Me.grpPrincipale.Controls.Add(Me.Label16)
        Me.grpPrincipale.Controls.Add(Me.cboAbbreviazione)
        Me.grpPrincipale.Controls.Add(Me.Label17)
        Me.grpPrincipale.Controls.Add(Me.cboRating)
        Me.grpPrincipale.Controls.Add(Me.Label18)
        Me.grpPrincipale.Controls.Add(Me.cboPNMateriale)
        Me.grpPrincipale.Controls.Add(Me.Label19)
        Me.grpPrincipale.Controls.Add(Me.cboFlame)
        Me.grpPrincipale.Controls.Add(Me.Label21)
        Me.grpPrincipale.Controls.Add(Me.cboFileUL)
        Me.grpPrincipale.Controls.Add(Me.txtCodice)
        Me.grpPrincipale.Controls.Add(Me.cboColore)
        Me.grpPrincipale.Controls.Add(Me.txtDescrizione)
        Me.grpPrincipale.Controls.Add(Me.Label112)
        Me.grpPrincipale.Controls.Add(Me.txtAutore)
        Me.grpPrincipale.Controls.Add(Me.Label139)
        Me.grpPrincipale.Controls.Add(Me.txtResponsabile)
        Me.grpPrincipale.Controls.Add(Me.Label140)
        Me.grpPrincipale.Controls.Add(Me.txtSemilavorato)
        Me.grpPrincipale.Controls.Add(Me.Label141)
        Me.grpPrincipale.Controls.Add(Me.cboTSuperficiale)
        Me.grpPrincipale.Controls.Add(Me.Label142)
        Me.grpPrincipale.Controls.Add(Me.Label35)
        Me.grpPrincipale.Controls.Add(Me.Label143)
        Me.grpPrincipale.Controls.Add(Me.txtLivello)
        Me.grpPrincipale.Controls.Add(Me.Label144)
        Me.grpPrincipale.Controls.Add(Me.Label36)
        Me.grpPrincipale.Controls.Add(Me.txtDerivato)
        Me.grpPrincipale.Controls.Add(Me.Label37)
        Me.grpPrincipale.Controls.Add(Me.Label8)
        Me.grpPrincipale.Controls.Add(Me.txtSostituisce)
        Me.grpPrincipale.Controls.Add(Me.cboDescrizione)
        Me.grpPrincipale.Controls.Add(Me.Label9)
        Me.grpPrincipale.Controls.Add(Me.cboCodifica)
        Me.grpPrincipale.Controls.Add(Me.txtDestinazione)
        Me.grpPrincipale.Controls.Add(Me.cboSpessore)
        Me.grpPrincipale.Controls.Add(Me.Label39)
        Me.grpPrincipale.Controls.Add(Me.Label42)
        Me.grpPrincipale.Controls.Add(Me.lblCaratteriDescrizione)
        Me.grpPrincipale.Controls.Add(Me.cboMateriale)
        Me.grpPrincipale.Controls.Add(Me.lblCaratteriCodice)
        Me.grpPrincipale.Controls.Add(Me.Label12)
        Me.grpPrincipale.Controls.Add(Me.GroupBox2)
        Me.grpPrincipale.Location = New System.Drawing.Point(21, 105)
        Me.grpPrincipale.Name = "grpPrincipale"
        Me.grpPrincipale.Size = New System.Drawing.Size(553, 520)
        Me.grpPrincipale.TabIndex = 321
        Me.grpPrincipale.TabStop = False
        '
        'pnlGrOfficina
        '
        Me.pnlGrOfficina.AutoScroll = True
        Me.pnlGrOfficina.Controls.Add(Me.chkOffSAR)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffIPC)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox8)
        Me.pnlGrOfficina.Controls.Add(Me.Label38)
        Me.pnlGrOfficina.Controls.Add(Me.cboOffSFR)
        Me.pnlGrOfficina.Controls.Add(Me.Label56)
        Me.pnlGrOfficina.Controls.Add(Me.Label66)
        Me.pnlGrOfficina.Controls.Add(Me.Label76)
        Me.pnlGrOfficina.Controls.Add(Me.Label83)
        Me.pnlGrOfficina.Controls.Add(Me.Label115)
        Me.pnlGrOfficina.Controls.Add(Me.Label117)
        Me.pnlGrOfficina.Controls.Add(Me.Label119)
        Me.pnlGrOfficina.Controls.Add(Me.Label126)
        Me.pnlGrOfficina.Controls.Add(Me.Label127)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox73)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox87)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox88)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox89)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox90)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox91)
        Me.pnlGrOfficina.Controls.Add(Me.PictureBox95)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffCDN)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffSBN)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffDCL)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffDCA)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffDNR)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffSFR)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffPFD)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffRWP)
        Me.pnlGrOfficina.Controls.Add(Me.txtOffIPP)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffIPP)
        Me.pnlGrOfficina.Controls.Add(Me.txtOffIPT)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffIPT)
        Me.pnlGrOfficina.Controls.Add(Me.txtOffSWM)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffSWM)
        Me.pnlGrOfficina.Controls.Add(Me.txtOffSWP)
        Me.pnlGrOfficina.Controls.Add(Me.chkOffSWP)
        Me.pnlGrOfficina.Location = New System.Drawing.Point(1385, 102)
        Me.pnlGrOfficina.Name = "pnlGrOfficina"
        Me.pnlGrOfficina.Size = New System.Drawing.Size(540, 378)
        Me.pnlGrOfficina.TabIndex = 322
        Me.pnlGrOfficina.Visible = False
        '
        'chkOffSAR
        '
        Me.chkOffSAR.Checked = True
        Me.chkOffSAR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffSAR.Location = New System.Drawing.Point(168, 318)
        Me.chkOffSAR.Name = "chkOffSAR"
        Me.chkOffSAR.Size = New System.Drawing.Size(250, 17)
        Me.chkOffSAR.TabIndex = 382
        Me.chkOffSAR.Text = "ARCA 'RT0041' COESMETIC QUALITY"
        Me.chkOffSAR.UseVisualStyleBackColor = True
        '
        'chkOffIPC
        '
        Me.chkOffIPC.Checked = True
        Me.chkOffIPC.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffIPC.ForeColor = System.Drawing.Color.Black
        Me.chkOffIPC.Location = New System.Drawing.Point(168, 128)
        Me.chkOffIPC.Name = "chkOffIPC"
        Me.chkOffIPC.Size = New System.Drawing.Size(352, 17)
        Me.chkOffIPC.TabIndex = 381
        Me.chkOffIPC.Text = "COMM SELF CLICHING-REFER TO MANUFACTURER"
        Me.chkOffIPC.UseVisualStyleBackColor = True
        '
        'PictureBox8
        '
        Me.PictureBox8.BackColor = System.Drawing.Color.Black
        Me.PictureBox8.Location = New System.Drawing.Point(-2, 198)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox8.TabIndex = 378
        Me.PictureBox8.TabStop = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.Black
        Me.Label38.Location = New System.Drawing.Point(7, 5)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(303, 20)
        Me.Label38.TabIndex = 377
        Me.Label38.Text = "ASSEMBLED PLATE WORKING SPECS"
        '
        'cboOffSFR
        '
        Me.cboOffSFR.Enabled = False
        Me.cboOffSFR.FormattingEnabled = True
        Me.cboOffSFR.Items.AddRange(New Object() {"1,20", "0,80", "1,50"})
        Me.cboOffSFR.Location = New System.Drawing.Point(223, 202)
        Me.cboOffSFR.Name = "cboOffSFR"
        Me.cboOffSFR.Size = New System.Drawing.Size(68, 21)
        Me.cboOffSFR.TabIndex = 323
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(1, 346)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(61, 13)
        Me.Label56.TabIndex = 363
        Me.Label56.Text = "CLEANING"
        '
        'Label66
        '
        Me.Label66.Location = New System.Drawing.Point(1, 298)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(146, 30)
        Me.Label66.TabIndex = 361
        Me.Label66.Text = "SURFACE ""-- - -- - --"""
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(1, 252)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(126, 13)
        Me.Label76.TabIndex = 360
        Me.Label76.Text = "DIMENSION CHECKING"
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(1, 232)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(155, 13)
        Me.Label83.TabIndex = 359
        Me.Label83.Text = "DIMENSION NOT INDICATED"
        '
        'Label115
        '
        Me.Label115.AutoSize = True
        Me.Label115.Location = New System.Drawing.Point(1, 206)
        Me.Label115.Name = "Label115"
        Me.Label115.Size = New System.Drawing.Size(96, 13)
        Me.Label115.TabIndex = 357
        Me.Label115.Text = "SATIN-FINISHING"
        '
        'Label117
        '
        Me.Label117.AutoSize = True
        Me.Label117.Location = New System.Drawing.Point(1, 179)
        Me.Label117.Name = "Label117"
        Me.Label117.Size = New System.Drawing.Size(88, 13)
        Me.Label117.TabIndex = 356
        Me.Label117.Text = "PRESS-FITTING"
        '
        'Label119
        '
        Me.Label119.AutoSize = True
        Me.Label119.Location = New System.Drawing.Point(1, 157)
        Me.Label119.Name = "Label119"
        Me.Label119.Size = New System.Drawing.Size(65, 13)
        Me.Label119.TabIndex = 355
        Me.Label119.Text = "RIVETTING"
        '
        'Label126
        '
        Me.Label126.AutoSize = True
        Me.Label126.Location = New System.Drawing.Point(1, 81)
        Me.Label126.Name = "Label126"
        Me.Label126.Size = New System.Drawing.Size(71, 13)
        Me.Label126.TabIndex = 353
        Me.Label126.Text = "INSERT, PIN"
        '
        'Label127
        '
        Me.Label127.AutoSize = True
        Me.Label127.Location = New System.Drawing.Point(1, 35)
        Me.Label127.Name = "Label127"
        Me.Label127.Size = New System.Drawing.Size(90, 13)
        Me.Label127.TabIndex = 352
        Me.Label127.Text = "SPOT WELDING"
        '
        'PictureBox73
        '
        Me.PictureBox73.BackColor = System.Drawing.Color.Black
        Me.PictureBox73.Location = New System.Drawing.Point(-2, 339)
        Me.PictureBox73.Name = "PictureBox73"
        Me.PictureBox73.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox73.TabIndex = 346
        Me.PictureBox73.TabStop = False
        '
        'PictureBox87
        '
        Me.PictureBox87.BackColor = System.Drawing.Color.Black
        Me.PictureBox87.Location = New System.Drawing.Point(-2, 293)
        Me.PictureBox87.Name = "PictureBox87"
        Me.PictureBox87.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox87.TabIndex = 345
        Me.PictureBox87.TabStop = False
        '
        'PictureBox88
        '
        Me.PictureBox88.BackColor = System.Drawing.Color.Black
        Me.PictureBox88.Location = New System.Drawing.Point(-2, 249)
        Me.PictureBox88.Name = "PictureBox88"
        Me.PictureBox88.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox88.TabIndex = 344
        Me.PictureBox88.TabStop = False
        '
        'PictureBox89
        '
        Me.PictureBox89.BackColor = System.Drawing.Color.Black
        Me.PictureBox89.Location = New System.Drawing.Point(-2, 227)
        Me.PictureBox89.Name = "PictureBox89"
        Me.PictureBox89.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox89.TabIndex = 343
        Me.PictureBox89.TabStop = False
        '
        'PictureBox90
        '
        Me.PictureBox90.BackColor = System.Drawing.Color.Black
        Me.PictureBox90.Location = New System.Drawing.Point(-2, 152)
        Me.PictureBox90.Name = "PictureBox90"
        Me.PictureBox90.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox90.TabIndex = 342
        Me.PictureBox90.TabStop = False
        '
        'PictureBox91
        '
        Me.PictureBox91.BackColor = System.Drawing.Color.Black
        Me.PictureBox91.Location = New System.Drawing.Point(-2, 174)
        Me.PictureBox91.Name = "PictureBox91"
        Me.PictureBox91.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox91.TabIndex = 341
        Me.PictureBox91.TabStop = False
        '
        'PictureBox95
        '
        Me.PictureBox95.BackColor = System.Drawing.Color.Black
        Me.PictureBox95.Location = New System.Drawing.Point(-2, 77)
        Me.PictureBox95.Name = "PictureBox95"
        Me.PictureBox95.Size = New System.Drawing.Size(520, 1)
        Me.PictureBox95.TabIndex = 337
        Me.PictureBox95.TabStop = False
        '
        'chkOffCDN
        '
        Me.chkOffCDN.Checked = True
        Me.chkOffCDN.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOffCDN.Enabled = False
        Me.chkOffCDN.Location = New System.Drawing.Point(168, 345)
        Me.chkOffCDN.Name = "chkOffCDN"
        Me.chkOffCDN.Size = New System.Drawing.Size(250, 17)
        Me.chkOffCDN.TabIndex = 329
        Me.chkOffCDN.Text = "DIRT, OIL, GREASE NOT PERMITTED"
        Me.chkOffCDN.UseVisualStyleBackColor = True
        '
        'chkOffSBN
        '
        Me.chkOffSBN.Checked = True
        Me.chkOffSBN.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffSBN.ForeColor = System.Drawing.Color.Black
        Me.chkOffSBN.Location = New System.Drawing.Point(168, 297)
        Me.chkOffSBN.Name = "chkOffSBN"
        Me.chkOffSBN.Size = New System.Drawing.Size(336, 17)
        Me.chkOffSBN.TabIndex = 328
        Me.chkOffSBN.Text = "BURRS, NOTCH AND SCRATCH NICK NOT PERMITTED"
        Me.chkOffSBN.UseVisualStyleBackColor = True
        '
        'chkOffDCL
        '
        Me.chkOffDCL.Checked = True
        Me.chkOffDCL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOffDCL.Enabled = False
        Me.chkOffDCL.Location = New System.Drawing.Point(168, 275)
        Me.chkOffDCL.Name = "chkOffDCL"
        Me.chkOffDCL.Size = New System.Drawing.Size(234, 17)
        Me.chkOffDCL.TabIndex = 327
        Me.chkOffDCL.Text = "LEAVE AT 20� � 5�C FOR 4 HOURS"
        Me.chkOffDCL.UseVisualStyleBackColor = True
        '
        'chkOffDCA
        '
        Me.chkOffDCA.Checked = True
        Me.chkOffDCA.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOffDCA.Enabled = False
        Me.chkOffDCA.Location = New System.Drawing.Point(168, 253)
        Me.chkOffDCA.Name = "chkOffDCA"
        Me.chkOffDCA.Size = New System.Drawing.Size(186, 17)
        Me.chkOffDCA.TabIndex = 326
        Me.chkOffDCA.Text = "ALL AFTER TREATMENT"
        Me.chkOffDCA.UseVisualStyleBackColor = True
        '
        'chkOffDNR
        '
        Me.chkOffDNR.Checked = True
        Me.chkOffDNR.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOffDNR.Enabled = False
        Me.chkOffDNR.Location = New System.Drawing.Point(168, 231)
        Me.chkOffDNR.Name = "chkOffDNR"
        Me.chkOffDNR.Size = New System.Drawing.Size(211, 17)
        Me.chkOffDNR.TabIndex = 325
        Me.chkOffDNR.Text = "REFER TO THE 3D DRAWING"
        Me.chkOffDNR.UseVisualStyleBackColor = True
        '
        'chkOffSFR
        '
        Me.chkOffSFR.Checked = True
        Me.chkOffSFR.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffSFR.ForeColor = System.Drawing.Color.Black
        Me.chkOffSFR.Location = New System.Drawing.Point(168, 205)
        Me.chkOffSFR.Name = "chkOffSFR"
        Me.chkOffSFR.Size = New System.Drawing.Size(90, 17)
        Me.chkOffSFR.TabIndex = 322
        Me.chkOffSFR.Text = "Ra um"
        Me.chkOffSFR.UseVisualStyleBackColor = True
        '
        'chkOffPFD
        '
        Me.chkOffPFD.Checked = True
        Me.chkOffPFD.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffPFD.ForeColor = System.Drawing.Color.Black
        Me.chkOffPFD.Location = New System.Drawing.Point(168, 178)
        Me.chkOffPFD.Name = "chkOffPFD"
        Me.chkOffPFD.Size = New System.Drawing.Size(292, 17)
        Me.chkOffPFD.TabIndex = 320
        Me.chkOffPFD.Text = "DRIVE THE PIN OR INSERT UNTIL THE STOP"
        Me.chkOffPFD.UseVisualStyleBackColor = True
        '
        'chkOffRWP
        '
        Me.chkOffRWP.Checked = True
        Me.chkOffRWP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffRWP.ForeColor = System.Drawing.Color.Black
        Me.chkOffRWP.Location = New System.Drawing.Point(168, 156)
        Me.chkOffRWP.Name = "chkOffRWP"
        Me.chkOffRWP.Size = New System.Drawing.Size(250, 17)
        Me.chkOffRWP.TabIndex = 319
        Me.chkOffRWP.Text = "WITHOUT PROTUSION ON SURFACE"
        Me.chkOffRWP.UseVisualStyleBackColor = True
        '
        'txtOffIPP
        '
        Me.txtOffIPP.BackColor = System.Drawing.Color.White
        Me.txtOffIPP.Enabled = False
        Me.txtOffIPP.Location = New System.Drawing.Point(334, 100)
        Me.txtOffIPP.Name = "txtOffIPP"
        Me.txtOffIPP.Size = New System.Drawing.Size(48, 20)
        Me.txtOffIPP.TabIndex = 314
        '
        'chkOffIPP
        '
        Me.chkOffIPP.Checked = True
        Me.chkOffIPP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffIPP.ForeColor = System.Drawing.Color.Black
        Me.chkOffIPP.Location = New System.Drawing.Point(168, 103)
        Me.chkOffIPP.Name = "chkOffIPP"
        Me.chkOffIPP.Size = New System.Drawing.Size(186, 17)
        Me.chkOffIPP.TabIndex = 313
        Me.chkOffIPP.Text = "PULL-OUT STRENGTH > N"
        Me.chkOffIPP.UseVisualStyleBackColor = True
        '
        'txtOffIPT
        '
        Me.txtOffIPT.BackColor = System.Drawing.Color.White
        Me.txtOffIPT.Enabled = False
        Me.txtOffIPT.Location = New System.Drawing.Point(334, 78)
        Me.txtOffIPT.Name = "txtOffIPT"
        Me.txtOffIPT.Size = New System.Drawing.Size(48, 20)
        Me.txtOffIPT.TabIndex = 312
        '
        'chkOffIPT
        '
        Me.chkOffIPT.Checked = True
        Me.chkOffIPT.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffIPT.ForeColor = System.Drawing.Color.Black
        Me.chkOffIPT.Location = New System.Drawing.Point(168, 81)
        Me.chkOffIPT.Name = "chkOffIPT"
        Me.chkOffIPT.Size = New System.Drawing.Size(190, 17)
        Me.chkOffIPT.TabIndex = 311
        Me.chkOffIPT.Text = "TORSION STRENGTH > Nm"
        Me.chkOffIPT.UseVisualStyleBackColor = True
        '
        'txtOffSWM
        '
        Me.txtOffSWM.BackColor = System.Drawing.Color.White
        Me.txtOffSWM.Enabled = False
        Me.txtOffSWM.Location = New System.Drawing.Point(301, 56)
        Me.txtOffSWM.Name = "txtOffSWM"
        Me.txtOffSWM.Size = New System.Drawing.Size(48, 20)
        Me.txtOffSWM.TabIndex = 310
        '
        'chkOffSWM
        '
        Me.chkOffSWM.Checked = True
        Me.chkOffSWM.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffSWM.ForeColor = System.Drawing.Color.Black
        Me.chkOffSWM.Location = New System.Drawing.Point(168, 59)
        Me.chkOffSWM.Name = "chkOffSWM"
        Me.chkOffSWM.Size = New System.Drawing.Size(206, 17)
        Me.chkOffSWM.TabIndex = 309
        Me.chkOffSWM.Text = "MARK H < mm"
        Me.chkOffSWM.UseVisualStyleBackColor = True
        '
        'txtOffSWP
        '
        Me.txtOffSWP.BackColor = System.Drawing.Color.White
        Me.txtOffSWP.Enabled = False
        Me.txtOffSWP.Location = New System.Drawing.Point(301, 35)
        Me.txtOffSWP.Name = "txtOffSWP"
        Me.txtOffSWP.Size = New System.Drawing.Size(48, 20)
        Me.txtOffSWP.TabIndex = 308
        '
        'chkOffSWP
        '
        Me.chkOffSWP.Checked = True
        Me.chkOffSWP.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkOffSWP.ForeColor = System.Drawing.Color.Black
        Me.chkOffSWP.Location = New System.Drawing.Point(168, 37)
        Me.chkOffSWP.Name = "chkOffSWP"
        Me.chkOffSWP.Size = New System.Drawing.Size(194, 17)
        Me.chkOffSWP.TabIndex = 307
        Me.chkOffSWP.Text = "PEEL STRENGTH > N"
        Me.chkOffSWP.UseVisualStyleBackColor = True
        '
        'frmAttributiSE
        '
        Me.AcceptButton = Me.cmdSalva
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.CancelButton = Me.cmdEsci
        Me.ClientSize = New System.Drawing.Size(1908, 1045)
        Me.Controls.Add(Me.txtSpessore)
        Me.Controls.Add(Me.grpPrincipale)
        Me.Controls.Add(Me.pnlTorneria)
        Me.Controls.Add(Me.pnlFresatura)
        Me.Controls.Add(Me.pnlPressofuso)
        Me.Controls.Add(Me.pnlResina)
        Me.Controls.Add(Me.pnlGomma)
        Me.Controls.Add(Me.pnlLamiera)
        Me.Controls.Add(Me.cmdSalvaEsci)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.pnlGrOfficina)
        Me.Controls.Add(Me.pnlLogo)
        Me.Controls.Add(Me.cboScala)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAttributiSE"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Attributi SolidEdge 2.0.1 - "
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.pnlTorneria.ResumeLayout(False)
        Me.pnlTorneria.PerformLayout()
        CType(Me.PictureBox74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlLamiera.ResumeLayout(False)
        Me.pnlLamiera.PerformLayout()
        CType(Me.PictureBox92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlPressofuso.ResumeLayout(False)
        Me.pnlPressofuso.PerformLayout()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox34, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlResina.ResumeLayout(False)
        Me.pnlResina.PerformLayout()
        CType(Me.PictureBox68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox44, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlGomma.ResumeLayout(False)
        Me.pnlGomma.PerformLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFresatura.ResumeLayout(False)
        Me.pnlFresatura.PerformLayout()
        CType(Me.PictureBox33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPrincipale.ResumeLayout(False)
        Me.grpPrincipale.PerformLayout()
        Me.pnlGrOfficina.ResumeLayout(False)
        Me.pnlGrOfficina.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox95, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Sub frmAttributiSE_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim iLXpnl, iLYpnl As Integer

        If Me.Width < 1193 Then Me.Width = 1193
        If Me.Height < 682 Then Me.Height = 682
        iLXpnl = Me.Width - pnlFresatura.Width - 30 ' grpPrincipale.Width + grpPrincipale.Location.X + 10
        iLYpnl = 120
        pnlFresatura.Location = New Point(iLXpnl, iLYpnl)
        pnlGomma.Location = New Point(iLXpnl, iLYpnl)
        pnlLamiera.Location = New Point(iLXpnl, iLYpnl)
        pnlPressofuso.Location = New Point(iLXpnl, iLYpnl)
        pnlResina.Location = New Point(iLXpnl, iLYpnl)
        pnlTorneria.Location = New Point(iLXpnl, iLYpnl)
        pnlLogo.Location = New Point(iLXpnl, iLYpnl)
        pnlGrOfficina.Location = New Point(iLXpnl, iLYpnl)

        pnlFresatura.Height = Me.Height - 200
        pnlFresatura.AutoScroll = True
        pnlGomma.Height = Me.Height - 200
        pnlLamiera.Height = Me.Height - 200
        pnlPressofuso.Height = Me.Height - 200
        pnlPressofuso.AutoScroll = True
        pnlResina.Height = Me.Height - 200
        pnlTorneria.Height = Me.Height - 200
        pnlLogo.Height = Me.Height - 200

    End Sub

    Private Sub frmAttributiSE_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ini = New INIFile(Application.StartupPath & "\attributise.ini")
        Dim regX64Key = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64)
        'Per conoscere quale versione � installata
        'Dim regSE = regX64Key.OpenSubKey("SOFTWARE\Unigraphics Solutions\Solid Edge") '
        Dim regSE = regX64Key.OpenSubKey(ini.ReadValue("config", "regVersionKey")) '
        'Dim prova As String() = regSE.GetSubKeyNames
        solidEdgePath = regSE.GetValue("InstallDir") 'InstallDir - InstallPath - PathName - PreferencesPath
        solidEdgePreferencesPath = regSE.GetValue("PreferencesPath") 'InstallDir - InstallPath - PathName - PreferencesPath
        solidEdgeLanguage = regSE.GetValue("InstalledLanguage")
        Dim tentativi As Integer = 0
        dbConnectionString = ""
        Dim dbFolder As String = ini.ReadValue("config", "dbMaterialFolder")
        If dbFolder.Substring(dbFolder.Length() - 1, 1) <> "\" Then dbFolder &= "\"

        Dim dbName As String = ini.ReadValue("config", "dbMaterialName")
        If File.Exists(dbFolder & dbName) = False Then
            MessageBox.Show("Database materiali non trovato all'indirizzo " & dbFolder & dbName)
        End If
        'Do While File.Exists(dbConnectionString) = False
        '    Select Case tentativi
        '        Case 0
        '            dbConnectionString = "H:\Cartigli Fase1\2018-01-18\dbMateriali.accdb"
        '        Case 1
        '            dbConnectionString = My.Application.Info.DirectoryPath & "\dbMateriali.accdb"
        '        Case 2
        '            dbConnectionString = solidEdgePreferencesPath & "\Materials\dbMateriali.accdb"
        '        Case 3
        '            dbConnectionString = "\\cashprosrv\SOFTWARE\SolidEdge_ST10\2D-3D\Setup\Materiali\dbMateriali.accdb"
        '        Case 4
        '            dbConnectionString = "\\cashprosrv\SOFTWARE\SolidEdge_ST9\2D-3D\Setup\Materiali\dbMateriali.accdb"
        '        Case Else
        '            MsgBox("Non trovo il database")
        '            Exit Do
        '    End Select

        '    tentativi += 1
        'Loop

        dbConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & dbFolder & dbName
        connOleDB = New OleDbConnection(dbConnectionString)
        Dim iSXpnl, iSYpnl As Integer
        iSXpnl = 540
        iSYpnl = 485

        Me.Width = iSXpnl + grpPrincipale.Width + 80
        Me.Height = 682
        Me.CenterToScreen()
        pnlLogo.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        pnlTorneria.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        pnlGomma.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        pnlLamiera.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        pnlPressofuso.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        pnlResina.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        pnlFresatura.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        pnlGrOfficina.Size = New System.Drawing.Size(iSXpnl, iSYpnl)
        Call CollegamentoSolidEdge()
        Call CaricaFamiglieTecnologiche()
        Call CaricaToolTip()
        Call LeggiAttributi()
    End Sub
    Private Sub CaricaToolTip()
        Dim ToolTipNote As New ToolTip()
        ToolTipNote.AutoPopDelay = 10000
        ToolTipNote.InitialDelay = 1000
        ToolTipNote.ReshowDelay = 500
        ToolTipNote.ShowAlways = True

        ToolTipNote.SetToolTip(Me.chkPreDRA, "Massimo angolo di spoglia accettabile")
        ToolTipNote.SetToolTip(Me.chkPreDAR, "Le dimensioni sono riferite alla base dell'elemento. Nel caso in cui debbano essere mantenute lungo tutta la sua altezza comparir� al loro fianco la nota 'Spoglia non accettata/draft angle not accepted'")
        ToolTipNote.SetToolTip(Me.chkPreDNI, "Riferirsi al disegno in formato 3D per le quote non specificate sul 2D")
        ToolTipNote.SetToolTip(Me.chkPreRAD, "Dimensione raggi non indicati sul disegno")
        ToolTipNote.SetToolTip(Me.chkPreCHA, "Dimensione smussi non indicati sul disegno")
        ToolTipNote.SetToolTip(Me.chkPrePLF, "Altezza massima delle bave su divisioni stampo")
        ToolTipNote.SetToolTip(Me.chkPreMGM, "Profondit� del punto di iniezione. Deve essere improntato sulla superficie della parte")
        ToolTipNote.SetToolTip(Me.chkPreMOF, "Profondit� dell' attacco pozzetto uscita lega fredda. Devono essere improntati sulla superficie della parte")
        ToolTipNote.SetToolTip(Me.chkPreMEP, "Profondit� delle impronte degli estrattori. Devono essere improntati sulla superficie della parte")
        ToolTipNote.SetToolTip(Me.chkPreMAE, "Errore massimo di allineamento su linee divisione matrici e carrelli. Deve essere sempre all�interno delle tolleranze delle dimensioni del pezzo")
        ToolTipNote.SetToolTip(Me.chkPreSCB, "Ogni modifica riguardante la sagoma del pezzo, le nervature, gli spessori, atta ad agevolare il processo di stampaggio e/o il rispetto delle tolleranze richieste deve essere preventivamente concordata con ARCA")
        ToolTipNote.SetToolTip(Me.chkPreIPT, "Coppia torcente minima a cui deve resistere il componente costampato o piantato")
        ToolTipNote.SetToolTip(Me.chkPreIPP, "Forza minima di trazione  a cui deve resistere il componente costampato o piantato")
        ToolTipNote.SetToolTip(Me.chkPreIPJ, "Forza minima di trazione applicata a sbalzo ed in torsione a cui deve resistere il componente costampato o piantato")
        ToolTipNote.SetToolTip(Me.chkPreTRF, "Specifica l�uso di maschi a rullare")
        ToolTipNote.SetToolTip(Me.chkPreTBN, "Specifica l�assenza di bave da filettatura")
        ToolTipNote.SetToolTip(Me.chkPreTTG, "Specifica il grado di tolleranza del filetto")
        ToolTipNote.SetToolTip(Me.chkPreHOS, "Non sono accettate difettosit� quali buchi, bave, ammaccature, righe, sulle superfici funzionali indicate con questa tipologia di linea")
        ToolTipNote.SetToolTip(Me.chkPreMAR, "Altezza o profondit� massima delle indicazioni logo, codice,materiale, simbolo di riciclabilit�")
        ToolTipNote.SetToolTip(Me.chkPreMRA, "Indicazioni sporgenti")
        ToolTipNote.SetToolTip(Me.chkPreMNR, "Indicazioni non sporgenti")
        ToolTipNote.SetToolTip(Me.chkPreCLH, "le parti devono essere consegnate pulite; senza residui di sporcizia, cavitazioni, ecc.")
        ToolTipNote.SetToolTip(Me.chkPreFSA, "Eseguire processo di sabbiatura")
        ToolTipNote.SetToolTip(Me.chkPreFSH, "Eseguire processo di  pallinatura")
        ToolTipNote.SetToolTip(Me.chkPreDCA, "Quote e tolleranze devono essere rispettate e controllate dopo il trattamento superficiale")
        ToolTipNote.SetToolTip(Me.chkPreDCL, "Effettuare i rilievi dimensionali solo dopo permanenza minima dei pezzi a 20�+-5� per quattro ore")

        ToolTipNote.SetToolTip(Me.chkGomFLA, "Massima altezza accettabile delle bave")
        ToolTipNote.SetToolTip(Me.chkGomMAR, "Profondit� punto di iniezione. Non tollerato in rilievo")
        ToolTipNote.SetToolTip(Me.chkGomAST, "Coppia torcente minima a cui deve resistere la parte in gomma costampata o piantata sull�anima in metallo o plastica")
        ToolTipNote.SetToolTip(Me.chkGomASP, "Forza minima di trazione a cui deve resistere la parte in gomma costampata o piantata sull�anima in metallo/plastica")
        ToolTipNote.SetToolTip(Me.chkGomDNI, "Riferirsi al disegno in formato 3D per le quote non specificate sulla versione 2D")
        ToolTipNote.SetToolTip(Me.chkGomGTO, "Coppia minima alla quale la gomma non deve slittare. Area e metodo col quale effettuare il controllo sono riportati sul disegno")
        ToolTipNote.SetToolTip(Me.chkGomGTR, "Forza di trazione minima alla quale la gomma non deve slittare. Area e metodo col quale effettuare il controllo sono riportati sul disegno")
        ToolTipNote.SetToolTip(Me.chkGomHAR, "Durezza della gomma. La misurazione deve essere eseguita nell�area evidenziata sul disegno")
        ToolTipNote.SetToolTip(Me.chkGomHOS, "Non sono accettate difettosit� quali buchi, bave, ammaccature, righe, sulle superfici funzionali indicate con questa tipologia di linea")
        ToolTipNote.SetToolTip(Me.chkGomHAI, "Non sono tollerate occlusioni e/o porosit� da aria ")
        ToolTipNote.SetToolTip(Me.chkGomHGR, "Rugosit� Ra da ottenere e mantenere con processo di rettifica")
        ToolTipNote.SetToolTip(Me.chkGomCLE, "I particolari devono essere consegnati esenti da sporcizia, oli, grassi")
        ToolTipNote.SetToolTip(Me.chkGomMEA, "Effettuare i rilievi dimensionali solo dopo permanenza minima dei pezzi a 20�+-5� per quattro ore")

        ToolTipNote.SetToolTip(Me.chkResDRA, "Massimo angolo di spoglia accettabile")
        ToolTipNote.SetToolTip(Me.chkResDAR, "Le dimensioni sono riferite alla base dell'elemento")
        ToolTipNote.SetToolTip(Me.chkResDNI, "Riferirsi al disegno in formato 3D per le quote non specificate sul 2D")
        ToolTipNote.SetToolTip(Me.chkResRAD, "Dimensione raggi non indicati sul disegno")
        ToolTipNote.SetToolTip(Me.chkResCHA, "Dimensione smussi non indicati sul disegno")
        ToolTipNote.SetToolTip(Me.chkResPLF, "Altezza massima delle bave su divisioni stampo")
        ToolTipNote.SetToolTip(Me.chkResMGM, "Profondit� del punto di iniezione. Deve essere improntato sulla superficie della parte")
        ToolTipNote.SetToolTip(Me.chkResMEM, "Profondit� delle impronte degli estrattori. Devono essere improntati sulla superficie della parte")
        ToolTipNote.SetToolTip(Me.chkResMSA, "Errore massimo di allineamento su linee divisione matrici e carrelli. Deve essere sempre all�interno delle tolleranze delle dimensioni del pezzo")
        ToolTipNote.SetToolTip(Me.chkResSRT, "Ogni modifica riguardante la sagoma del pezzo, le nervature, gli spessori, atta ad agevolare il processo di stampaggio e/o il rispetto delle tolleranze richieste deve essere preventivamente concordata con ARCA")
        ToolTipNote.SetToolTip(Me.chkResIPT, "Coppia torcente minima a cui deve resistere il componente costampato o piantato ")
        ToolTipNote.SetToolTip(Me.chkResIPP, "Forza minima di trazione  a cui deve resistere il componente costampato o piantato")
        ToolTipNote.SetToolTip(Me.chkResIPJ, "Forza minima di trazione applicata a sbalzo ed in torsione a cui deve resistere il componente costampato o piantato")
        ToolTipNote.SetToolTip(Me.chkResMAH, "Altezza o profondit� massima delle indicazioni logo, codice,materiale, eventuale simbolo di riciclabilit�")
        ToolTipNote.SetToolTip(Me.chkResMAR, "Indicazioni sporgenti")
        ToolTipNote.SetToolTip(Me.chkResMAN, "Indicazioni non sporgenti")
        ToolTipNote.SetToolTip(Me.chkResSVD, "Grado generale di rugosit� superficiale richiesto. Nel disegno 2D possono essere evidenziate specifiche aree su cui � richiesto un valore differente o una lavorazione di lucidatura")
        ToolTipNote.SetToolTip(Me.chkResHAD, "Non sono accettate difettosit� quali buchi, bave, ammaccature, righe, sulle superfici funzionali indicate con questa tipologia di linea")
        ToolTipNote.SetToolTip(Me.chkResMEL, "Effettuare i rilievi dimensionali solo dopo permanenza minima dei pezzi a 20�+-5�C per quattro ore")

        ToolTipNote.SetToolTip(Me.chkLamBHM, "L�altezza della bava di lavorazione deve essere al massimo il 25% dello spessore")
        ToolTipNote.SetToolTip(Me.chkLamBHF, "Non � tollerata alcuna bava, ma sono comunque accettati spigoli vivi")
        ToolTipNote.SetToolTip(Me.chkLamRAD, "Dimensione raggi non indicati sul disegno ")
        ToolTipNote.SetToolTip(Me.chkLamCHA, "Dimensione smussi non indicati sul disegno")
        ToolTipNote.SetToolTip(Me.chkLamBAI, "Massimo raggio interno di piegatura")
        ToolTipNote.SetToolTip(Me.chkLamBPS, "Non accettati gradini nei punti di ritrancia")
        'ToolTipNote.SetToolTip(Me.chkLamSWP, "Valore minimo della forza di distacco della saldatura")
        'ToolTipNote.SetToolTip(Me.chkLamSWS, "Altezza massima del punto di saldatura")
        'ToolTipNote.SetToolTip(Me.chkLamIPT, "Coppia torcente minima a cui deve resistere il componente saldato o piantato")
        'ToolTipNote.SetToolTip(Me.chkLamIPP, "Forza minima di trazione  a cui deve resistere il componente saldato o piantato")
        ToolTipNote.SetToolTip(Me.chkLamTRF, "Specifica l�uso di maschi a rullare")
        ToolTipNote.SetToolTip(Me.chkLamTBN, "Specifica l�assenza di bave da filettatura")
        ToolTipNote.SetToolTip(Me.chkLamTTG, "Specifica il grado di tolleranza del filetto")
        'ToolTipNote.SetToolTip(Me.chkLamRWP, "Testimone di ribaditura a filo della superficie, non accettato in rilievo")
        'ToolTipNote.SetToolTip(Me.chkLamPFD, "Piantare a battuta i perni o inserti; adattare le dimensioni dei fori sulla lamiera alla tolleranza dei perni")
        ToolTipNote.SetToolTip(Me.chkLamSFR, "Effettuare processo di satinatura; indicato il valore di rugosit� Ra da raggiungere e la direzione della lavorazione")
        ToolTipNote.SetToolTip(Me.chkLamLCM, "Attacco taglio laser a filo del profilo del pezzo o incassato al suo interno")
        ToolTipNote.SetToolTip(Me.chkLamDNI, "Riferirsi al disegno in formato 3D per le quote non specificate sulla versione 2D")
        ToolTipNote.SetToolTip(Me.chkLamDCA, "Quote e tolleranze devono essere rispettate e controllate dopo il trattamento superficiale ")
        ToolTipNote.SetToolTip(Me.chkLamDCL, "Effettuare i rilievi dimensionali solo dopo permanenza minima dei pezzi a 20+-5�C per quattro ore")
        ToolTipNote.SetToolTip(Me.chkLamHAD, "Non sono accettate difettosit� quali buchi, bave, ammaccature, righe, sulle superfici funzionali indicate con questa tipologia di linea")
        ToolTipNote.SetToolTip(Me.chkLamCLD, "Le parti devono essere consegnate pulite; senza residui di sporcizia, cavitazioni, ecc")

        ToolTipNote.SetToolTip(Me.chkTorEFC, "Non � accettato il testimone della troncatura finale")
        ToolTipNote.SetToolTip(Me.chkTorEFN, "Testimone della troncatura finale accettato all�interno delle dimensioni indicate")
        ToolTipNote.SetToolTip(Me.chkTorCHR, "Specifica l�uso di maschi a rullare")
        ToolTipNote.SetToolTip(Me.chkTorCHB, "Specifica l�assenza di bave da filettatura")
        'ToolTipNote.SetToolTip(Me.chkTorCHT, "Specifica il grado di tolleranza del filetto")
        ToolTipNote.SetToolTip(Me.chkTorDNI, "Riferirsi al disegno in formato 3D per le quote non specificate sulla versione 2D")
        ToolTipNote.SetToolTip(Me.chkTorDCA, "Quote, tolleranze ed assenza difetti estetici devono essere rispettati e controllati dopo il trattamento superficiale")
        ToolTipNote.SetToolTip(Me.ChkTorDCL, "Effettuare i rilievi dimensionali solo dopo permanenza minima dei pezzi a 20 +-5�C per quattro ore")
        ToolTipNote.SetToolTip(Me.chkTorDCO, "Non sono accettate difettosit� quali buchi, bave, ammaccature, righe, sulle superfici funzionali indicate con questa tipologia di linea ")
        ToolTipNote.SetToolTip(Me.chkTorCLE, "I particolari devono essere consegnati esenti da sporcizia, oli, grassi")

        ToolTipNote.SetToolTip(Me.chkFreBHM, "L�altezza della bava di lavorazione deve essere al massimo 0,035mm")
        ToolTipNote.SetToolTip(Me.chkFreBHF, "Non � tollerata alcuna bava, ma sono comunque accettati spigoli vivi")
        ToolTipNote.SetToolTip(Me.chkFreRAD, "Dimensione raggi non indicati sul disegno")
        ToolTipNote.SetToolTip(Me.chkFreCHA, "Dimensione smussi non indicati sul disegno")
        ToolTipNote.SetToolTip(Me.chkFreIPT, "Coppia torcente minima a cui deve resistere il componente saldato o piantato")
        ToolTipNote.SetToolTip(Me.chkFreIPP, "Forza minima di trazione  a cui deve resistere il componente saldato o piantato")
        ToolTipNote.SetToolTip(Me.chkFreTRF, "Specifica l�uso di maschi a rullare")
        ToolTipNote.SetToolTip(Me.chkFreTBN, "Specifica l�assenza di bave da filettatura")
        ToolTipNote.SetToolTip(Me.chkFreTTG, "Specifica il grado di tolleranza del filetto")
        ToolTipNote.SetToolTip(Me.chkFreRWP, "Testimone di ribaditura a filo della superficie, non accettato in rilievo")
        ToolTipNote.SetToolTip(Me.chkFrePFD, "Piantare a battuta i perni o inserti; adattare le dimensioni dei fori posti sulla parte alla tolleranza dei perni")
        ToolTipNote.SetToolTip(Me.chkFreDNI, "Riferirsi al disegno in formato 3D per le quote non specificate sulla versione")
        ToolTipNote.SetToolTip(Me.chkFreDCA, "Quote e tolleranze devono essere rispettate e controllate dopo il trattamento superficiale")
        ToolTipNote.SetToolTip(Me.chkFreDCL, "Effettuare i rilievi dimensionali solo dopo permanenza minima dei pezzi a 20+-5�C per quattro ore")
        ToolTipNote.SetToolTip(Me.chkFreHAD, "Non sono accettate difettosit� quali buchi, bave, ammaccature, righe, sulle superfici funzionali indicate con questa tipologia di linea")
        ToolTipNote.SetToolTip(Me.chkFreCLD, "I particolari devono essere consegnati esenti da sporcizia, oli, grassi")

    End Sub
    Public Sub CollegamentoSolidEdge()

        Try
            objApplication = Marshal.GetActiveObject("SolidEdge.Application")
            objDocument = objApplication.ActiveDocument
            Select Case objDocument.Type
                Case 3, 10
                    stTipoDocumento = "Assembly"
                Case 2
                    stTipoDocumento = "Draft"
                Case 1, 8
                    stTipoDocumento = "Part"
                Case 4, 9
                    stTipoDocumento = "SheetMetal"
                Case 7
                    stTipoDocumento = "Weldment"
                Case Else '5
                    stTipoDocumento = "Sconosciuto"
            End Select
            stNomeFile = objDocument.Name
        Catch When Err.Number = 429 'Solid Edge non � in esecuzione
            stTipoDocumento = "NonAttivo"
            MsgBox("Solid Edge non � in esecuzione. Title Block Editor non verr� eseguito")
            End
        Catch When Err.Number = -2147467259 'Non c'� nessun documento attivo
            stTipoDocumento = "Nessuno"
            MsgBox("Nessun documento Attivo. Title Block Editor non verr� eseguito")
            End
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub cboFamiglia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFamiglia.SelectedIndexChanged
        cboCodifica.Items.Clear()
        cboDescrizione.Items.Clear()
        cboMateriale.Items.Clear()
        Dim cmdOle As New OleDbCommand("SELECT IDFamiglia.value, Codifica FROM tblCatTec WHERE IDFamiglia.value = '" & cboFamiglia.Text & "'", connOleDB)
        connOleDB.Open()
        Dim cmdSQL As OleDbDataReader = cmdOle.ExecuteReader()
        While cmdSQL.Read()
            cboCodifica.Items.Add(cmdSQL("Codifica").ToString())
            'cboDescrizione.Items.Add(cmdSQL("Descrizione").ToString())
        End While
        cmdSQL.Close()


        cboCodifica.Text = ""

        pnlFresatura.Visible = False
        pnlGomma.Visible = False
        pnlLamiera.Visible = False
        pnlPressofuso.Visible = False
        pnlResina.Visible = False
        pnlTorneria.Visible = False
        pnlGrOfficina.Visible = False

        cboPNMateriale.Enabled = False
        cboAbbreviazione.Enabled = False
        cboProduttore.Enabled = False
        cboColore.Enabled = False
        cboFlame.Enabled = False
        cboFileUL.Enabled = False
        cboRating.Enabled = False
        cboSpessore.Enabled = False
        cboTTermico.Enabled = False
        cboTSuperficiale.Enabled = False

        cboMateriale.Text = ""
        cboPNMateriale.Text = ""
        cboAbbreviazione.Text = ""
        cboProduttore.Text = ""
        cboColore.Text = ""
        cboFlame.Text = ""
        cboFileUL.Text = ""
        cboRating.Text = ""

        cboMateriale.Enabled = True

        Select Case cboFamiglia.Text
            Case "Resina"
                pnlResina.Visible = True
                cboPNMateriale.Enabled = True
                cboAbbreviazione.Enabled = True
                cboProduttore.Enabled = True
                cboColore.Enabled = True
                cboFlame.Enabled = True
                cboFileUL.Enabled = True
                cboRating.Enabled = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
                stSQL = "SELECT * FROM tblVdiRa"
                cmdOle.CommandText = stSQL
                cmdSQL = cmdOle.ExecuteReader()
                While cmdSQL.Read()
                    cboResSVD.Items.Add(cmdSQL("Vdi").ToString())
                    cboResSRA.Items.Add(cmdSQL("Ra").ToString())
                End While
                cmdSQL.Close()
            Case "Gomma"
                pnlGomma.Visible = True
                cboPNMateriale.Enabled = True
                cboProduttore.Enabled = True
                cboColore.Enabled = True
            Case "Lamiera"
                pnlLamiera.Visible = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
                cboSpessore.Enabled = True

            Case "Gr Officina"
                pnlGrOfficina.Visible = True
                cboMateriale.Enabled = False
                cboColore.Enabled = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            Case "Pressofuso"
                pnlPressofuso.Visible = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            'cboAbbreviazione.Enabled = True
            Case "Molla"
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            Case "Torneria"
                pnlTorneria.Visible = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            Case "Generico"
            Case "Fresatura"
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
                pnlFresatura.Visible = True
        End Select

        connOleDB.Close()
        Call CaricaMateriali()
    End Sub
    Private Sub cboMateriale_LostFocus(sender As Object, e As EventArgs) Handles cboMateriale.LostFocus, cboPNMateriale.LostFocus, cboProduttore.LostFocus, cboColore.LostFocus, cboFlame.LostFocus, cboRating.LostFocus, cboFileUL.LostFocus, cboAbbreviazione.LostFocus
        If Len(sender.text) > 0 Then Call CaricaMateriali()
    End Sub
    Private Sub cboMateriale_GotFocus(sender As Object, e As EventArgs) Handles cboMateriale.GotFocus, cboPNMateriale.GotFocus, cboProduttore.GotFocus, cboColore.GotFocus, cboFlame.GotFocus, cboRating.GotFocus, cboFileUL.GotFocus, cboAbbreviazione.GotFocus
        If sender.Items.Count = 1 Then sender.selectedindex = 0
    End Sub
    Public Sub CaricaMateriali()
        Dim bDoppio As Boolean
        Dim cmdOle As New OleDbCommand("", connOleDB)
        connOleDB.Open()

        stSQL = "SELECT * FROM tblMateriale WHERE IDFamiglia.value = '" & cboFamiglia.Text & "'"
        cmdOle.CommandText = stSQL
        cmdSQL = cmdOle.ExecuteReader()
        While cmdSQL.Read()
            If cboMateriale.Items.Count = 0 Then
                cboMateriale.Items.Add(cmdSQL("NomeCommerciale").ToString)
            Else
                bDoppio = False
                For i = 0 To cboMateriale.Items.Count - 1
                    If cboMateriale.Items.Item(i) = cmdSQL("NomeCommerciale").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboMateriale.Items.Add(cmdSQL("NomeCommerciale").ToString)
            End If
        End While
        cmdSQL.Close()

        cboSpessore.Items.Clear()
        If Len(cboMateriale.Text) > 0 Then
            stSQL = "SELECT * FROM tblSpessori WHERE Designazione = '" & cboMateriale.Text & "'"
            cmdOle.CommandText = stSQL
            cmdSQL = cmdOle.ExecuteReader()
            While cmdSQL.Read()
                cboSpessore.Items.Add(cmdSQL("SpessoreTolleranza").ToString)
            End While
        End If
        cmdSQL.Close()

        'spessore = LeggiAttributo("MechanicalModeling",)
        cboTTermico.Items.Clear()
        If Len(cboMateriale.Text) > 0 Then
            stSQL = "SELECT * FROM tblTratTermico WHERE Applicazione.value = '" & cboFamiglia.Text & "'"
            cmdOle.CommandText = stSQL
            cmdSQL = cmdOle.ExecuteReader()
            While cmdSQL.Read()
                cboTTermico.Items.Add(cmdSQL("Designazione").ToString)
            End While
        End If
        cmdSQL.Close()

        cboTSuperficiale.Items.Clear()
        If Len(cboMateriale.Text) > 0 Then
            stSQL = "SELECT * FROM tblTratSuperficiale WHERE Applicazione.value = '" & cboFamiglia.Text & "'"
            cmdOle.CommandText = stSQL
            cmdSQL = cmdOle.ExecuteReader()
            While cmdSQL.Read()
                cboTSuperficiale.Items.Add(cmdSQL("Designazione").ToString)
            End While
        End If
        cmdSQL.Close()

        Call CreaStringa()
        cmdOle.CommandText = stSQL
        cmdSQL = cmdOle.ExecuteReader()
        While cmdSQL.Read()
            If cboMateriale.Items.Count = 0 Then
                'cboMateriale.Items.Add(cmdSQL("NomeCommerciale").ToString)
                cboPNMateriale.Items.Add(cmdSQL("PartNumber").ToString)
                cboProduttore.Items.Add(cmdSQL("IDProduttore").ToString)
                cboColore.Items.Add(cmdSQL("IDColore").ToString)
                cboAbbreviazione.Items.Add(cmdSQL("Abbreviazione").ToString)
                cboFlame.Items.Add(cmdSQL("FlamV").ToString)
                cboRating.Items.Add(cmdSQL("FlamAt").ToString)
                cboFileUL.Items.Add(cmdSQL("FlamFile").ToString)
            Else

                bDoppio = False
                For i = 0 To cboPNMateriale.Items.Count - 1
                    If cboPNMateriale.Items.Item(i) = cmdSQL("PartNumber").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboPNMateriale.Items.Add(cmdSQL("PartNumber").ToString)

                bDoppio = False
                For i = 0 To cboProduttore.Items.Count - 1
                    If cboProduttore.Items.Item(i) = cmdSQL("IDProduttore").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboProduttore.Items.Add(cmdSQL("IDProduttore").ToString)

                bDoppio = False
                For i = 0 To cboColore.Items.Count - 1
                    If cboColore.Items.Item(i) = cmdSQL("IDColore").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboColore.Items.Add(cmdSQL("IDColore").ToString)

                bDoppio = False
                For i = 0 To cboAbbreviazione.Items.Count - 1
                    If cboAbbreviazione.Items.Item(i) = cmdSQL("Abbreviazione").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboAbbreviazione.Items.Add(cmdSQL("Abbreviazione").ToString)

                bDoppio = False
                For i = 0 To cboFlame.Items.Count - 1
                    If cboFlame.Items.Item(i) = cmdSQL("FlamV").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboFlame.Items.Add(cmdSQL("FlamV").ToString)

                bDoppio = False
                For i = 0 To cboRating.Items.Count - 1
                    If cboRating.Items.Item(i) = cmdSQL("FlamAt").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboRating.Items.Add(cmdSQL("FlamAt").ToString)

                bDoppio = False
                For i = 0 To cboFileUL.Items.Count - 1
                    If cboFileUL.Items.Item(i) = cmdSQL("FlamFile").ToString Then
                        bDoppio = True
                        Exit For
                    End If
                Next
                If bDoppio = False Then cboFileUL.Items.Add(cmdSQL("FlamFile").ToString)
            End If

        End While

        cmdSQL.Close()
        stSQL = "SELECT * FROM tblVdiRa ORDER BY Vdi"
        cmdOle.CommandText = stSQL
        cmdSQL = cmdOle.ExecuteReader()
        cboResSVD.Items.Clear()
        cboResSRA.Items.Clear()
        While cmdSQL.Read()
            cboResSVD.Items.Add(cmdSQL("Vdi").ToString)
            cboResSRA.Items.Add(cmdSQL("Ra").ToString)

        End While
        connOleDB.Close()
    End Sub
    Public Sub ResetComboMateriale()

        cboPNMateriale.Enabled = False
        cboSpessore.Enabled = False
        cboProduttore.Enabled = False
        cboColore.Enabled = False
        cboAbbreviazione.Enabled = False
        cboFlame.Enabled = False
        cboRating.Enabled = False
        cboFileUL.Enabled = False
        cboTTermico.Enabled = False
        cboTSuperficiale.Enabled = False

        cboMateriale.Text = ""
        cboPNMateriale.Text = ""
        cboSpessore.Text = ""
        cboProduttore.Text = ""
        cboColore.Text = ""
        cboAbbreviazione.Text = ""
        cboFlame.Text = ""
        cboRating.Text = ""
        cboFileUL.Text = ""
        cboTTermico.Text = ""
        cboTSuperficiale.Text = ""

        Select Case cboFamiglia.Text
            Case "Resina"
                cboPNMateriale.Enabled = True
                cboProduttore.Enabled = True
                cboColore.Enabled = True
                cboAbbreviazione.Enabled = True
                cboFlame.Enabled = True
                cboRating.Enabled = True
                cboFileUL.Enabled = True
            Case "Gomma"
                cboPNMateriale.Enabled = True
                cboSpessore.Enabled = True
                cboProduttore.Enabled = True
                cboColore.Enabled = True
            Case "Lamiera"
                cboSpessore.Enabled = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            Case "Pressofuso"
                cboSpessore.Enabled = True
                cboAbbreviazione.Enabled = True
            Case "Molla"
                cboSpessore.Enabled = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            Case "Torneria"
                cboSpessore.Enabled = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            Case "Generico"
                cboSpessore.Enabled = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
            Case "Fresatura"
                cboSpessore.Enabled = True
                cboTTermico.Enabled = True
                cboTSuperficiale.Enabled = True
        End Select
    End Sub
    Public Sub CreaStringa()
        stSQL = "SELECT * FROM tblMateriale WHERE IDFamiglia.value = '" & cboFamiglia.Text & "'"
        If Len(cboMateriale.Text) > 0 Then stSQL = stSQL & " AND NomeCommerciale = '" & cboMateriale.Text & "'"
        If Len(cboPNMateriale.Text) > 0 Then stSQL = stSQL & " AND PartNumber = '" & cboPNMateriale.Text & "'"
        If Len(cboProduttore.Text) > 0 Then stSQL = stSQL & " AND IDProduttore = '" & cboProduttore.Text & "'"
        If Len(cboColore.Text) > 0 Then stSQL = stSQL & " AND IDColore = '" & cboColore.Text & "'"
        If Len(cboAbbreviazione.Text) > 0 Then stSQL = stSQL & " AND Abbreviazione = '" & cboAbbreviazione.Text & "'"
        If Len(cboFlame.Text) > 0 Then stSQL = stSQL & " AND FlamV = '" & cboFlame.Text & "'"
        If Len(cboRating.Text) > 0 Then stSQL = stSQL & " AND FlamAt = '" & cboRating.Text & "'"
        If Len(cboFileUL.Text) > 0 Then stSQL = stSQL & " AND FlamFile = '" & cboFileUL.Text & "'"

        'cboMateriale.Items.Clear()
        cboPNMateriale.Items.Clear()
        cboProduttore.Items.Clear()
        cboColore.Items.Clear()
        cboAbbreviazione.Items.Clear()
        cboFlame.Items.Clear()
        cboRating.Items.Clear()
        cboFileUL.Items.Clear()
    End Sub
    Public Sub CaricaFamiglieTecnologiche()
        cboCodifica.Items.Clear()
        cboDescrizione.Items.Clear()
        cboFamiglia.Items.Clear()

        Dim cmdOle As New OleDbCommand("SELECT Famiglia FROM tblFamCatTec", connOleDB)
        connOleDB.Open()
        Dim cmdSQL As OleDbDataReader = cmdOle.ExecuteReader()
        While cmdSQL.Read()
            cboFamiglia.Items.Add(cmdSQL("Famiglia").ToString)
        End While
        cmdSQL.Close()
        connOleDB.Close()
    End Sub

    Private Sub cmdSlitta_Click(sender As Object, e As EventArgs) Handles cmdSlitta.Click
        txtMod01.Text = txtMod02.Text
        txtData01.Text = txtData02.Text
        txtAut01.Text = txtAut02.Text
        txtMod02.Text = txtMod03.Text
        txtData02.Text = txtData03.Text
        txtAut02.Text = txtAut03.Text
        txtMod03.Text = txtMod04.Text
        txtData03.Text = txtData04.Text
        txtAut03.Text = txtAut04.Text
        txtMod04.Text = txtMod05.Text
        txtData04.Text = txtData05.Text
        txtAut04.Text = txtAut05.Text
        txtMod05.Text = txtMod06.Text
        txtData05.Text = txtData06.Text
        txtAut05.Text = txtAut06.Text
        txtMod06.Text = ""
        txtData06.Text = ""
        txtAut06.Text = ""
    End Sub

    Private Sub cmdSalva_Click(sender As Object, e As EventArgs) Handles cmdSalva.Click
        Call SalvaAttributi()
    End Sub

    Public Sub SalvaAttributi()
        Dim stNoteMancanti As String
        objApplication = Marshal.GetActiveObject("SolidEdge.Application")
        objDocument = objApplication.ActiveDocument
        objPropertySets = objDocument.Properties

        'Verifico il tipo di file attivo
        CollegamentoSolidEdge()
        If stTipoDocumento = "Draft" Or stTipoDocumento = "Sconosciuto" Then
            MsgBox("Il documento attivo non � adatto all'utilizzo con Title Block Editor.", MsgBoxStyle.Critical)
            Exit Sub
        End If

        stPropertySet = "Custom"
        Call ScriviAttributo(cboFamiglia, "FamigliaMateriale")
        stFamigliaMateriale = cboFamiglia.Text
        stNoteMancanti = ""
        Call ScriviAttributo(cboSpessore, "Spessore")
        Call ScriviAttributo(cboMateriale, "MaterialeAssieme")
        If Not stModello = "asm" Then
            stPropertySet = "MechanicalModeling"
            Call ScriviAttributo(cboMateriale, "Material")
        End If
        stPropertySet = "Custom"
        Select Case UCase(stFamigliaMateriale)
            Case "RESINA"
                For Each chk In pnlResina.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then
                        If chk.CheckState = CheckState.Indeterminate Then
                            MsgBox("Non � possibile proseguire, � necessario compilare tutte le note", vbCritical) 'aggiungere nel 'tag' il messaggio da visualizzare
                            Exit Sub
                        End If
                    End If
                Next
                If Len(stNoteMancanti) > 0 Then
                    MsgBox("Non � possibile proseguire, � necessario compilare tutte le note. Definire le note:" & stNoteMancanti, vbCritical) 'aggiungere nel 'tag' il messaggio da visualizzare
                    Exit Sub
                End If

                objProperties = objPropertySets.Item("Custom")
                objProperty = objProperties.Item("MaterialeAssieme")
                objProperty.Value = "SEE NOTES"

                Call ScriviValoreChk(chkResDRA, "�", "ResDRA")
                Call ScriviValoreChk(chkResDAR, "�", "ResDAR")
                Call ScriviValoreChk(chkResDNI, "�", "ResDNI")
                Call ScriviValoreChk(chkResRAD, "�", "ResRAD")
                Call ScriviValoreChk(chkResCHA, "�", "ResCHA")
                Call ScriviValoreChk(chkResPLF, "�", "ResPLF")
                Call ScriviValoreChk(chkResMGM, "�", "ResMGM")
                Call ScriviValoreChk(chkResMEM, "�", "ResMEM")
                Call ScriviValoreChk(chkResMSA, "�", "ResMSA")
                Call ScriviValoreChk(chkResSRT, "�", "ResSRT")
                Call ScriviValoreChk(chkResIPT, "�", "ResIPT")
                Call ScriviValoreChk(chkResIPP, "�", "ResIPP")
                Call ScriviValoreChk(chkResIPJ, "�", "ResIPJ")
                Call ScriviValoreChk(chkResMAH, "�", "ResMAH")
                Call ScriviValoreChk(chkResMAR, "�", "ResMAR")
                Call ScriviValoreChk(chkResMAN, "�", "ResMAN")
                Call ScriviValoreChk(chkResSVD, "�", "ResSVD")
                Call ScriviValoreChk(chkResSGL, "�", "ResSGL")
                Call ScriviValoreChk(chkResHAD, "�", "ResHAD")
                Call ScriviValoreChk(chkResSAR, "�", "ResSAR")
                Call ScriviValoreChk(chkResMEL, "�", "ResMEL")

                Call CreaSingoloCustom("Custom", "M")
                Call ScriviAttributo(cboResDRA, "txtResDRA")
                Call ScriviAttributo(txtResRAD, "txtResRAD")
                Call ScriviAttributo(txtResCHA, "txtResCHA")
                Call ScriviAttributo(cboResPLF, "txtResPLF")
                Call ScriviAttributo(cboResMGM, "txtResMGM")
                Call ScriviAttributo(cboResMEM, "txtResMEM")
                Call ScriviAttributo(cboResMSA, "txtResMSA")
                Call ScriviAttributo(txtResIPT, "txtResIPT")
                Call ScriviAttributo(txtResIPP, "txtResIPP")
                Call ScriviAttributo(txtResIPJ, "txtResIPJ")
                Call ScriviAttributo(txtResMAH, "txtResMAH")
                Call ScriviAttributo(cboResSVD, "txtResSVD")
                Call ScriviAttributo(cboResSRA, "txtResSRA")
                Call ScriviAttributo(cboMateriale, "txtResMAT")
                Call ScriviAttributo(cboPNMateriale, "txtResMPN")
                Call ScriviAttributo(cboProduttore, "txtResPRO")
                Call ScriviAttributo(cboColore, "txtResCOL")
                Call ScriviAttributo(cboAbbreviazione, "txtResABB")
                Call ScriviAttributo(cboFlame, "txtResFRV")
                Call ScriviAttributo(cboRating, "txtResFRR")
                Call ScriviAttributo(cboFileUL, "txtResFFU")

            Case "GOMMA"
                For Each chk In pnlGomma.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then
                        If chk.CheckState = CheckState.Indeterminate Then
                            MsgBox("Non � possibile proseguire, � necessario compilare tutte le note", vbCritical) 'aggiungere nel 'tag' il messaggio da visualizzare
                            Exit Sub
                        End If
                    End If
                Next

                objProperties = objPropertySets.Item("Custom")
                objProperty = objProperties.Item("MaterialeAssieme")
                objProperty.Value = "SEE NOTES"

                Call ScriviValoreChk(chkGomFLA, "�", "GomFLA")
                Call ScriviValoreChk(chkGomMAR, "�", "GomMAR")
                Call ScriviValoreChk(chkGomAST, "�", "GomAST")
                Call ScriviValoreChk(chkGomASP, "�", "GomASP")
                Call ScriviValoreChk(chkGomDNI, "�", "GomDNI")
                Call ScriviValoreChk(chkGomGTO, "�", "GomGTO")
                Call ScriviValoreChk(chkGomGTR, "�", "GomGTR")
                Call ScriviValoreChk(chkGomHAR, "�", "GomHAR")
                Call ScriviValoreChk(chkGomHOS, "�", "GomHOS")
                Call ScriviValoreChk(chkGomHAI, "�", "GomHAI")
                Call ScriviValoreChk(chkGomHGR, "�", "GomHGR")
                Call ScriviValoreChk(chkGomSAR, "�", "GomSAR")
                Call ScriviValoreChk(chkGomCLE, "�", "GomCLE")
                Call ScriviValoreChk(chkGomMEA, "�", "GomMEA")

                Call ScriviAttributo(cboGomFLH, "txtGomFLH")
                Call ScriviAttributo(txtGomMGT, "txtGomMGT")
                Call ScriviAttributo(txtGomATO, "txtGomATO")
                Call ScriviAttributo(txtGomAPU, "txtGomAPU")
                Call ScriviAttributo(txtGomTOR, "txtGomTOR")
                Call ScriviAttributo(txtGomTRA, "txtGomTRA")
                Call ScriviAttributo(txtGomSHA, "txtGomSHA")
                Call ScriviAttributo(txtGomSHD, "txtGomSHD")
                Call ScriviAttributo(txtGomHRA, "txtGomHRA")
                Call ScriviAttributo(cboMateriale, "txtGomMAT")
                Call ScriviAttributo(cboPNMateriale, "txtGomMPN")
                Call ScriviAttributo(cboProduttore, "txtGomPRO")
                Call ScriviAttributo(cboColore, "txtGomCOL")
            Case "LAMIERA"
                For Each chk In pnlLamiera.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then
                        If chk.CheckState = CheckState.Indeterminate Then
                            MsgBox("Non � possibile proseguire, � necessario compilare tutte le note", vbCritical) 'aggiungere nel 'tag' il messaggio da visualizzare
                            Exit Sub
                        End If
                    End If
                Next


                Call ScriviValoreChk(chkLamBHM, "�", "LamBHM")
                Call ScriviValoreChk(chkLamBHF, "�", "LamBHF")
                Call ScriviValoreChk(chkLamBHE, "�", "LamBHE")
                Call ScriviValoreChk(chkLamBER, "�", "LamBER")
                Call ScriviValoreChk(chkLamBEC, "�", "LamBEC")
                Call ScriviValoreChk(chkLamRAD, "�", "LamRAD")
                Call ScriviValoreChk(chkLamCHA, "�", "LamCHA")
                Call ScriviValoreChk(chkLamBAI, "�", "LamBAI")
                Call ScriviValoreChk(chkLamBPS, "�", "LamBPS")
                Call ScriviValoreChk(chkLamTRF, "�", "LamTRF")
                Call ScriviValoreChk(chkLamTBN, "�", "LamTBN")
                Call ScriviValoreChk(chkLamTTG, "�", "LamTTG")
                Call ScriviValoreChk(chkLamSFR, "�", "LamSFR")
                Call ScriviValoreChk(chkLamLCM, "�", "LamLCM")
                Call ScriviValoreChk(chkLamDNI, "�", "LamDNI")
                Call ScriviValoreChk(chkLamDCA, "�", "LamDCA")
                Call ScriviValoreChk(chkLamDCL, "�", "LamDCL")
                Call ScriviValoreChk(chkLamHAD, "�", "LamHAD")
                Call ScriviValoreChk(chkLamSCQ, "�", "LamSCQ")
                Call ScriviValoreChk(chkLamCLD, "�", "LamCLD")

                Call ScriviAttributo(cboLamESH, "txtLamESH")
                Call ScriviAttributo(cboLamESL, "txtLamESL")
                Call ScriviAttributo(txtLamRAD, "txtLamRAD")
                Call ScriviAttributo(txtLamCHA, "txtLamCHA")
                Call ScriviAttributo(txtLamBAI, "txtLamBAI")
                Call ScriviAttributo(cboLamTTG, "txtLamTTG")
                Call ScriviAttributo(cboLamSFR, "txtLamSFR")

            Case "PRESSOFUSO"
                For Each chk In pnlPressofuso.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then
                        If chk.CheckState = CheckState.Indeterminate Then
                            MsgBox("Non � possibile proseguire, � necessario compilare tutte le note", vbCritical) 'aggiungere nel 'tag' il messaggio da visualizzare
                            Exit Sub
                        End If
                    End If
                Next

                Call ScriviValoreChk(chkPreDRA, "�", "PreDRA")
                Call ScriviValoreChk(chkPreDAR, "�", "PreDAR")
                Call ScriviValoreChk(chkPreDNI, "�", "PreDNI")
                Call ScriviValoreChk(chkPreRAD, "�", "PreRAD")
                Call ScriviValoreChk(chkPreCHA, "�", "PreCHA")
                Call ScriviValoreChk(chkPrePLF, "�", "PrePLF")
                Call ScriviValoreChk(chkPreMGM, "�", "PreMGM")
                Call ScriviValoreChk(chkPreMOF, "�", "PreMOF")
                Call ScriviValoreChk(chkPreMEP, "�", "PreMEP")
                Call ScriviValoreChk(chkPreMAE, "�", "PreMAE")
                Call ScriviValoreChk(chkPreSCB, "�", "PreSCB")
                Call ScriviValoreChk(chkPreIPT, "�", "PreIPT")
                Call ScriviValoreChk(chkPreIPP, "�", "PreIPP")
                Call ScriviValoreChk(chkPreIPJ, "�", "PreIPJ")
                Call ScriviValoreChk(chkPreTRF, "�", "PreTRF")
                Call ScriviValoreChk(chkPreTBN, "�", "PreTBN")
                Call ScriviValoreChk(chkPreTTG, "�", "PreTTG")
                Call ScriviValoreChk(chkPreHOS, "�", "PreHOS")
                Call ScriviValoreChk(chkPreSAR, "�", "PreSAR")
                Call ScriviValoreChk(chkPreMAR, "�", "PreMAR")
                Call ScriviValoreChk(chkPreMRA, "�", "PreMRA")
                Call ScriviValoreChk(chkPreMNR, "�", "PreMNR")
                Call ScriviValoreChk(chkPreCLH, "�", "PreCLH")
                Call ScriviValoreChk(chkPreFSA, "�", "PreFSA")
                Call ScriviValoreChk(chkPreFSH, "�", "PreFSH")
                Call ScriviValoreChk(chkPreDCA, "�", "PreDCA")
                Call ScriviValoreChk(chkPreDCL, "�", "PreDCL")

                Call ScriviAttributo(cboPreDRA, "txtPreDRA")
                Call ScriviAttributo(txtPreRAD, "txtPreRAD")
                Call ScriviAttributo(txtPreCHA, "txtPreCHA")
                Call ScriviAttributo(cboPrePLF, "txtPrePLF")
                Call ScriviAttributo(txtPreMGM, "txtPreMGM")
                Call ScriviAttributo(txtPreMOF, "txtPreMOF")
                Call ScriviAttributo(txtPreMEP, "txtPreMEP")
                Call ScriviAttributo(cboPreMAE, "txtPreMAE")
                Call ScriviAttributo(txtPreIPT, "txtPreIPT")
                Call ScriviAttributo(txtPreIPP, "txtPreIPP")
                Call ScriviAttributo(txtPreIPJ, "txtPreIPJ")
                Call ScriviAttributo(cboPreTTG, "txtPreTTG")
                Call ScriviAttributo(txtPreMAH, "txtPreMAH")
                Call ScriviAttributo(cboAbbreviazione, "txtPreABB")

            Case "MOLLA"
            Case "TORNERIA"
                For Each chk In pnlTorneria.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then
                        If chk.CheckState = CheckState.Indeterminate Then
                            MsgBox("Non � possibile proseguire, � necessario compilare tutte le note", vbCritical) 'aggiungere nel 'tag' il messaggio da visualizzare
                            Exit Sub
                        End If
                    End If
                Next

                Call ScriviValoreChk(chkTorEFC, "�", "TorEFC")
                Call ScriviValoreChk(chkTorEFN, "�", "TorEFN")
                Call ScriviValoreChk(chkTorIES, "�", "TorIES")
                Call ScriviValoreChk(chkTorIER, "�", "TorIER")
                Call ScriviValoreChk(chkTorIEC, "�", "TorIEC")
                Call ScriviValoreChk(chkTorEES, "�", "TorEES")
                Call ScriviValoreChk(chkTorEER, "�", "TorEER")
                Call ScriviValoreChk(chkTorEEC, "�", "TorEEC")
                Call ScriviValoreChk(chkTorCHR, "�", "TorCHR")
                Call ScriviValoreChk(chkTorCHB, "�", "TorCHB")
                Call ScriviValoreChk(chkTorCHS, "�", "TorCHS")
                Call ScriviValoreChk(chkTorCHN, "�", "TorCHN")
                chkTorDNI.Checked = True 'Abilitazione obbligatoria
                Call ScriviValoreChk(chkTorDNI, "�", "TorDNI")
                chkTorDCA.Checked = True 'Abilitazione obbligatoria
                Call ScriviValoreChk(chkTorDCA, "�", "TorDCA")
                ChkTorDCL.Checked = True 'Abilitazione obbligatoria
                Call ScriviValoreChk(ChkTorDCL, "�", "TorDCL")
                Call ScriviValoreChk(chkTorDCO, "�", "TorDCO")
                Call ScriviValoreChk(chkTorCAQ, "�", "TorCAQ")
                chkTorCLE.Checked = True 'Abilitazione obbligatoria
                Call ScriviValoreChk(chkTorCLE, "�", "TorCLE")

                Call ScriviAttributo(txtTorNIB, "txtTorNIB")
                Call ScriviAttributo(txtTorH, "txtTorEFH")
                Call ScriviAttributo(cboTorIES, "txtTorIES")
                Call ScriviAttributo(cboTorEES, "txtTorEES")
                Call ScriviAttributo(cboTorCHS, "txtTorCHS")
                Call ScriviAttributo(cboTorCHN, "txtTorCHN")

            Case "GENERICO"
            Case "FRESATURA"
                For Each chk In pnlFresatura.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then
                        If chk.CheckState = CheckState.Indeterminate Then
                            MsgBox("Non � possibile proseguire, � necessario compilare tutte le note", vbCritical) 'aggiungere nel 'tag' il messaggio da visualizzare
                            Exit Sub
                        End If
                    End If
                Next

                Call ScriviValoreChk(chkFreBHM, "�", "FreBHM")
                Call ScriviValoreChk(chkFreBHF, "�", "FreBHF")
                Call ScriviValoreChk(chkFreSEN, "�", "FreSEN")
                Call ScriviValoreChk(chkFreBER, "�", "FreBER")
                Call ScriviValoreChk(chkFreBEC, "�", "FreBEC")
                Call ScriviValoreChk(chkFreRAD, "�", "FreRAD")
                Call ScriviValoreChk(chkFreCHA, "�", "FreCHA")
                Call ScriviValoreChk(chkFreIPT, "�", "FreIPT")
                Call ScriviValoreChk(chkFreIPP, "�", "FreIPP")
                Call ScriviValoreChk(chkFreTRF, "�", "FreTRF")
                Call ScriviValoreChk(chkFreTBN, "�", "FreTBN")
                Call ScriviValoreChk(chkFreTTG, "�", "FreTTG")
                Call ScriviValoreChk(chkFreRWP, "�", "FreRWP")
                Call ScriviValoreChk(chkFrePFD, "�", "FrePFD")
                Call ScriviValoreChk(chkFreDNI, "�", "FreDNI")
                Call ScriviValoreChk(chkFreDCA, "�", "FreDCA")
                Call ScriviValoreChk(chkFreDCL, "�", "FreDCL")
                Call ScriviValoreChk(chkFreHAD, "�", "FreHAD")
                Call ScriviValoreChk(chkFreSAR, "�", "FreSAR")
                Call ScriviValoreChk(chkFreCLD, "�", "FreCLD")
                Call ScriviValoreChk(chkFreSEN, "�", "FreSEN")

                Call ScriviAttributo(cboFreSEU, "txtFreSEU")
                Call ScriviAttributo(cboFreSEL, "txtFreSEL")
                Call ScriviAttributo(txtFreRAD, "txtFreRAD")
                Call ScriviAttributo(txtFreCHA, "txtFreCHA")
                Call ScriviAttributo(txtFreIPT, "txtFreIPT")
                Call ScriviAttributo(txtFreIPP, "txtFreIPP")
                Call ScriviAttributo(cboFreTTG, "txtFreTTG")

            Case "GR OFFICINA"

                Call ScriviValoreChk(chkOffSWP, "�", "OffSWP")
                Call ScriviValoreChk(chkOffSWM, "�", "OffSWM")
                Call ScriviValoreChk(chkOffIPT, "�", "OffIPT")
                Call ScriviValoreChk(chkOffIPP, "�", "OffIPP")
                Call ScriviValoreChk(chkOffIPC, "�", "OffIPC")
                Call ScriviValoreChk(chkOffRWP, "�", "OffRWP")
                Call ScriviValoreChk(chkOffPFD, "�", "OffPFD")
                Call ScriviValoreChk(chkOffSFR, "�", "OffSFR")
                Call ScriviValoreChk(chkOffDNR, "�", "OffDNR")
                Call ScriviValoreChk(chkOffDCA, "�", "OffDCA")
                Call ScriviValoreChk(chkOffDCL, "�", "OffDCL")
                Call ScriviValoreChk(chkOffSBN, "�", "OffSBN")
                Call ScriviValoreChk(chkOffSAR, "�", "OffSAR")
                Call ScriviValoreChk(chkOffCDN, "�", "OffCDN")

                Call ScriviAttributo(txtOffSWP, "txtOffSWP")
                Call ScriviAttributo(txtOffSWM, "txtOffSWM")
                Call ScriviAttributo(txtOffIPT, "txtOffIPT")
                Call ScriviAttributo(txtOffIPP, "txtOffIPP")
                Call ScriviAttributo(cboOffSFR, "txtOffSFR")
        End Select

        Call ScriviAttributo(txtDerivato, "Derivato")
        Call ScriviAttributo(txtSostituisce, "Sostituisce")
        Call ScriviAttributo(txtSemilavorato, "Cod. Semilavorato")
        Call ScriviAttributo(txtDestinazione, "Cod. Destinatario")
        Call ScriviAttributo(cboData, "Data")
        Call ScriviAttributo(cboScala, "Scala")
        Call ScriviAttributo(cboTTermico, "Tratt_Termico")
        Call ScriviAttributo(cboTSuperficiale, "Tratt_Superficiale")

        Call ScriviAttributo(txtMod01, "Mov01")
        Call ScriviAttributo(txtData01, "Mov01-data")
        Call ScriviAttributo(txtAut01, "Mov01-mod")
        Call ScriviAttributo(txtMod02, "Mov02")
        Call ScriviAttributo(txtData02, "Mov02-data")
        Call ScriviAttributo(txtAut02, "Mov02-mod")
        Call ScriviAttributo(txtMod03, "Mov03")
        Call ScriviAttributo(txtData03, "Mov03-data")
        Call ScriviAttributo(txtAut03, "Mov03-mod")
        Call ScriviAttributo(txtMod04, "Mov04")
        Call ScriviAttributo(txtData04, "Mov04-data")
        Call ScriviAttributo(txtAut04, "Mov04-mod")
        Call ScriviAttributo(txtMod05, "Mov05")
        Call ScriviAttributo(txtData05, "Mov05-data")
        Call ScriviAttributo(txtAut05, "Mov05-mod")
        Call ScriviAttributo(txtMod06, "Mov06")
        Call ScriviAttributo(txtData06, "Mov06-data")
        Call ScriviAttributo(txtAut06, "Mov06-mod")




        'Scrivo gli attributi comuni
        stPropertySet = "ProjectInformation"
        Call ScriviAttributo(txtCodice, "Document Number")
        Call ScriviAttributo(txtLivello, "Revision")

        stPropertySet = "SummaryInformation"
        Call ScriviAttributo(txtAutore, "Autore") '"Author") 'Autore
        Call ScriviAttributo(txtDescrizione, "Oggetto") '"Subject") 'Oggetto

        stPropertySet = "DocumentSummaryInformation"
        Call ScriviAttributo(txtResponsabile, "Manager")
        Call ScriviAttributo(cboCodifica, "Categoria") ' "Category") 'Categoria

    End Sub

    Public Sub ScriviValoreChk(objControllo As CheckBox, stValore As String, stProperty As String)
        While True
            Try
                objProperties = objPropertySets.Item(stPropertySet)
                objProperty = objProperties.Item(stProperty)
                If objControllo.CheckState = CheckState.Checked Then
                    objProperty.Value = stValore
                Else
                    objProperty.Value = " "
                End If
                Exit While
            Catch ex As Exception
                Call CreaSingoloCustom(stPropertySet, stProperty)
            End Try
        End While
    End Sub

    Public Function LeggiAttributo(stPropertySet As String, stProperty As String) As String
        Try
            objProperties = objPropertySets.Item(stPropertySet)
            objProperty = objProperties.Item(stProperty)

        Catch ex As Exception
            Select Case Err.Number
                Case 9 'Attributo inesistente
                    Call CreaSingoloCustom(stPropertySet, stProperty)

            End Select
        End Try
        Return objProperty.Value
    End Function
    Public Sub ScriviAttributo(objControllo As Control, stProperty As String)
        While True
            Try
                objProperties = objPropertySets.Item(stPropertySet)
                objProperty = objProperties.Item(stProperty)

                If Len(objControllo.Text) > 0 Then
                    objProperty.Value = objControllo.Text ' & " "
                Else
                    objProperty.Value = " "
                End If
                Exit While
            Catch ex As Exception
                If Err.Number = 9 Then Call CreaSingoloCustom(stPropertySet, stProperty)
            End Try
        End While
    End Sub
    Sub CreaSingoloCustom(stPropertySet As String, stProperty As String)
        'Try
        objProperties = objPropertySets.Item(stPropertySet)
        objProperties.Add(stProperty, "")
        objProperty = objProperties.Item(stProperty)
        'Catch
        'End Try
    End Sub

    Private Sub cboMateriale_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMateriale.SelectedIndexChanged
        cboPNMateriale.Text = ""
        cboAbbreviazione.Text = ""
        cboSpessore.Text = ""
        cboTTermico.Text = ""
        cboProduttore.Text = ""
        cboColore.Text = ""
        cboTSuperficiale.Text = ""
        cboFlame.Text = ""
        cboFileUL.Text = ""
        cboRating.Text = ""
    End Sub

    Public Sub LeggiAttributi()
        On Error Resume Next

        objApplication = Marshal.GetActiveObject("SolidEdge.Application")
        objDocument = objApplication.ActiveDocument
        objPropertySets = objDocument.Properties


        'Verifico che sia attivo un documento valido (par, psm, asm)
        CollegamentoSolidEdge()
        If stTipoDocumento = "Draft" Or stTipoDocumento = "Sconosciuto" Then
            MsgBox("Il documento attivo non � adatto all'utilizzo con Title Block Editor.", MsgBoxStyle.Critical)
            Exit Sub
        End If

        'Memorizzo il nome del file corrente e lo scrivo come titolo del form
        'stFileCodice = Microsoft.VisualBasic.Left(objDocument.Name, (Len(objDocument.Name) - 4))
        stFileCodice = objDocument.Name
        Me.Text = stFileCodice
        'Scompongo il Codice dal Livello e li memorizzo
        For i = 1 To Len(stFileCodice)
            If Mid(stFileCodice, i, 1) = "-" Then
                stFileLivello = Mid(stFileCodice, i + 1, Len(stFileCodice) - i)
                stFileCodice = Microsoft.VisualBasic.Left(stFileCodice, i - 1)
                Exit For
            End If
            stFileLivello = "00"
        Next

        stPropertySet = "SummaryInformation"
        stModello = Microsoft.VisualBasic.Right(LeggiAttributo(stPropertySet, "Modello"), 3)
        'stModello = Microsoft.VisualBasic.Right(LeggiAttributo(stPropertySet, "Model"), 3)
        If stModello = "" Then
            Dim objDoc As Object
            objDoc = objApplication.ActiveDocument
            stModello = Microsoft.VisualBasic.Right(objDoc.fullname, 3)
        End If

        stPropertySet = "Custom"
        stFamigliaMateriale = LeggiAttributo(stPropertySet, "FamigliaMateriale")
        If Len(stFamigliaMateriale) = 0 Then
            stFamigliaMateriale = LeggiAttributo(stPropertySet, "Famiglia")
        End If

        For i = 0 To cboFamiglia.Items.Count - 1
            If UCase(cboFamiglia.Items.Item(i).ToString) = UCase(stFamigliaMateriale) Then
                cboFamiglia.SelectedIndex = i
                Exit For
            End If
        Next

        txtDerivato.Text = LeggiAttributo(stPropertySet, "Derivato")
        txtSostituisce.Text = LeggiAttributo(stPropertySet, "Sostituisce")
        txtSemilavorato.Text = LeggiAttributo(stPropertySet, "Cod. Semilavorato")
        txtDestinazione.Text = LeggiAttributo(stPropertySet, "Cod. Destinatario")
        cboData.Text = LeggiAttributo(stPropertySet, "Data")
        cboScala.Text = LeggiAttributo(stPropertySet, "Scala")
        cboTTermico.Text = LeggiAttributo(stPropertySet, "Tratt_Termico")
        cboTSuperficiale.Text = LeggiAttributo(stPropertySet, "Tratt_Superficiale")

        txtMod01.Text = LeggiAttributo(stPropertySet, "Mov01")
        txtData01.Text = LeggiAttributo(stPropertySet, "Mov01-data")
        txtAut01.Text = LeggiAttributo(stPropertySet, "Mov01-mod")
        txtMod02.Text = LeggiAttributo(stPropertySet, "Mov02")
        txtData02.Text = LeggiAttributo(stPropertySet, "Mov02-data")
        txtAut02.Text = LeggiAttributo(stPropertySet, "Mov02-mod")
        txtMod03.Text = LeggiAttributo(stPropertySet, "Mov03")
        txtData03.Text = LeggiAttributo(stPropertySet, "Mov03-data")
        txtAut03.Text = LeggiAttributo(stPropertySet, "Mov03-mod")
        txtMod04.Text = LeggiAttributo(stPropertySet, "Mov04")
        txtData04.Text = LeggiAttributo(stPropertySet, "Mov04-data")
        txtAut04.Text = LeggiAttributo(stPropertySet, "Mov04-mod")
        txtMod05.Text = LeggiAttributo(stPropertySet, "Mov05")
        txtData05.Text = LeggiAttributo(stPropertySet, "Mov05-data")
        txtAut05.Text = LeggiAttributo(stPropertySet, "Mov05-mod")
        txtMod06.Text = LeggiAttributo(stPropertySet, "Mov06")
        txtData06.Text = LeggiAttributo(stPropertySet, "Mov06-data")
        txtAut06.Text = LeggiAttributo(stPropertySet, "Mov06-mod")




        If stModello = "ASM" Or stModello = "asm" Then
            stPropertySet = "Custom"
            cboMateriale.Text = LeggiAttributo(stPropertySet, "MaterialeAssieme")
        Else
            stPropertySet = "MechanicalModeling"
            cboMateriale.Text = LeggiAttributo(stPropertySet, "Material")
        End If

        stPropertySet = "Custom"
        cboSpessore.Text = LeggiAttributo(stPropertySet, "Spessore")
        Select Case UCase(stFamigliaMateriale)
            Case "RESINA"
                For Each chk In pnlResina.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then chk.Checked = False
                Next
                If LeggiAttributo(stPropertySet, "ResDRA") = "�" Then chkResDRA.CheckState = CheckState.Checked : Else chkResDRA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResDAR") = "�" Then chkResDAR.CheckState = CheckState.Checked : Else chkResDAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResDNI") = "�" Then chkResDNI.CheckState = CheckState.Checked : Else chkResDNI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResRAD") = "�" Then chkResRAD.CheckState = CheckState.Checked : Else chkResRAD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResCHA") = "�" Then chkResCHA.CheckState = CheckState.Checked : Else chkResCHA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResPLF") = "�" Then chkResPLF.CheckState = CheckState.Checked : Else chkResPLF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResMGM") = "�" Then chkResMGM.CheckState = CheckState.Checked : Else chkResMGM.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResMEM") = "�" Then chkResMEM.CheckState = CheckState.Checked : Else chkResMEM.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResMSA") = "�" Then chkResMSA.CheckState = CheckState.Checked : Else chkResMSA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResSRT") = "�" Then chkResSRT.CheckState = CheckState.Checked : Else chkResSRT.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResIPT") = "�" Then chkResIPT.CheckState = CheckState.Checked : Else chkResIPT.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResIPP") = "�" Then chkResIPP.CheckState = CheckState.Checked : Else chkResIPP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResIPJ") = "�" Then chkResIPJ.CheckState = CheckState.Checked : Else chkResIPJ.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResMAH") = "�" Then chkResMAH.CheckState = CheckState.Checked : Else chkResMAH.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResMAR") = "�" Then chkResMAR.CheckState = CheckState.Checked : Else chkResMAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResMAN") = "�" Then chkResMAN.CheckState = CheckState.Checked : Else chkResMAN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResSVD") = "�" Then chkResSVD.CheckState = CheckState.Checked : Else chkResSVD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResSGL") = "�" Then chkResSGL.CheckState = CheckState.Checked : Else chkResSGL.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResHAD") = "�" Then chkResHAD.CheckState = CheckState.Checked : Else chkResHAD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResSAR") = "�" Then chkResSAR.CheckState = CheckState.Checked : Else chkResSAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "ResMEL") = "�" Then chkResMEL.CheckState = CheckState.Checked : Else chkResMEL.CheckState = CheckState.Unchecked

                cboResDRA.Text = LeggiAttributo(stPropertySet, "txtResDRA")
                txtResRAD.Text = LeggiAttributo(stPropertySet, "txtResRAD")
                txtResCHA.Text = LeggiAttributo(stPropertySet, "txtResCHA")
                cboResPLF.Text = LeggiAttributo(stPropertySet, "txtResPLF")
                cboResMGM.Text = LeggiAttributo(stPropertySet, "txtResMGM")
                cboResMEM.Text = LeggiAttributo(stPropertySet, "txtResMEM")
                cboResMSA.Text = LeggiAttributo(stPropertySet, "txtResMSA")
                txtResIPT.Text = LeggiAttributo(stPropertySet, "txtResIPT")
                txtResIPP.Text = LeggiAttributo(stPropertySet, "txtResIPP")
                txtResIPJ.Text = LeggiAttributo(stPropertySet, "txtResIPJ")
                txtResMAH.Text = LeggiAttributo(stPropertySet, "txtResMAH")
                cboResSVD.Text = LeggiAttributo(stPropertySet, "txtResSVD")
                cboResSRA.Text = LeggiAttributo(stPropertySet, "txtResSRA")
                cboMateriale.Text = LeggiAttributo(stPropertySet, "txtResMAT")
                cboPNMateriale.Text = LeggiAttributo(stPropertySet, "txtResMPN")
                cboProduttore.Text = LeggiAttributo(stPropertySet, "txtResPRO")
                cboColore.Text = LeggiAttributo(stPropertySet, "txtResCOL")
                cboAbbreviazione.Text = LeggiAttributo(stPropertySet, "txtResABB")
                cboFlame.Text = LeggiAttributo(stPropertySet, "txtResFRV")
                cboRating.Text = LeggiAttributo(stPropertySet, "txtResFRR")
                cboFileUL.Text = LeggiAttributo(stPropertySet, "txtResFFU")

                'cboResDRA.Enabled = True
                'cboResPLF.Enabled = True
                'cboResMGM.Enabled = True
                'cboResMEM.Enabled = True
                'cboResMSA.Enabled = True

            Case "GOMMA"
                For Each chk In pnlGomma.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then chk.Checked = False
                Next
                If LeggiAttributo(stPropertySet, "GomFLA") = "�" Then chkGomFLA.CheckState = CheckState.Checked : Else chkGomFLA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomMAR") = "�" Then chkGomMAR.CheckState = CheckState.Checked : Else chkGomMAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomAST") = "�" Then chkGomAST.CheckState = CheckState.Checked : Else chkGomAST.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomASP") = "�" Then chkGomASP.CheckState = CheckState.Checked : Else chkGomASP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomDNI") = "�" Then chkGomDNI.CheckState = CheckState.Checked : Else chkGomDNI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomGTO") = "�" Then chkGomGTO.CheckState = CheckState.Checked : Else chkGomGTO.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomGTR") = "�" Then chkGomGTR.CheckState = CheckState.Checked : Else chkGomGTR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomHAR") = "�" Then chkGomHAR.CheckState = CheckState.Checked : Else chkGomHAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomHOS") = "�" Then chkGomHOS.CheckState = CheckState.Checked : Else chkGomHOS.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomHAI") = "�" Then chkGomHAI.CheckState = CheckState.Checked : Else chkGomHAI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomHGR") = "�" Then chkGomHGR.CheckState = CheckState.Checked : Else chkGomHGR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomSAR") = "�" Then chkGomSAR.CheckState = CheckState.Checked : Else chkGomSAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomCLE") = "�" Then chkGomCLE.CheckState = CheckState.Checked : Else chkGomCLE.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "GomMEA") = "�" Then chkGomMEA.CheckState = CheckState.Checked : Else chkGomMEA.CheckState = CheckState.Unchecked


                cboGomFLH.Text = LeggiAttributo(stPropertySet, "txtGomFLH")
                txtGomMGT.Text = LeggiAttributo(stPropertySet, "txtGomMGT")
                txtGomATO.Text = LeggiAttributo(stPropertySet, "txtGomATO")
                txtGomAPU.Text = LeggiAttributo(stPropertySet, "txtGomAPU")
                txtGomTOR.Text = LeggiAttributo(stPropertySet, "txtGomTOR")
                txtGomTRA.Text = LeggiAttributo(stPropertySet, "txtGomTRA")
                txtGomSHA.Text = LeggiAttributo(stPropertySet, "txtGomSHA")
                txtGomSHD.Text = LeggiAttributo(stPropertySet, "txtGomSHD")
                txtGomHRA.Text = LeggiAttributo(stPropertySet, "txtGomHRA")
                cboMateriale.Text = LeggiAttributo(stPropertySet, "txtGomMAT")
                cboPNMateriale.Text = LeggiAttributo(stPropertySet, "txtGomMPN")
                cboProduttore.Text = LeggiAttributo(stPropertySet, "txtGomPRO")
                cboColore.Text = LeggiAttributo(stPropertySet, "txtGomCOL")

            Case "LAMIERA"
                For Each chk In pnlLamiera.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then chk.Checked = False
                Next
                If LeggiAttributo(stPropertySet, "LamBHM") = "�" Then chkLamBHM.CheckState = CheckState.Checked : Else chkLamBHM.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamBHF") = "�" Then chkLamBHF.CheckState = CheckState.Checked : Else chkLamBHF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamBHE") = "�" Then
                    chkLamBHE.CheckState = CheckState.Checked
                    cboLamESH.Enabled = True
                    cboLamESL.Enabled = True
                    chkLamBER.Enabled = True
                    chkLamBEC.Enabled = True
                    cboLamESH.Text = LeggiAttributo(stPropertySet, "txtLamESH")
                    cboLamESL.Text = LeggiAttributo(stPropertySet, "txtLamESL")
                Else
                    chkLamBHE.CheckState = CheckState.Unchecked
                    cboLamESH.Enabled = False
                    cboLamESL.Enabled = False
                    chkLamBER.Enabled = False
                    chkLamBEC.Enabled = False
                    cboLamESH.Text = ""
                    cboLamESL.Text = ""
                End If
                If LeggiAttributo(stPropertySet, "LamBER") = "�" Then chkLamBER.CheckState = CheckState.Checked : Else chkLamBER.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamBEC") = "�" Then chkLamBEC.CheckState = CheckState.Checked : Else chkLamBEC.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamRAD") = "�" Then chkLamRAD.CheckState = CheckState.Checked : Else chkLamRAD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamCHA") = "�" Then chkLamCHA.CheckState = CheckState.Checked : Else chkLamCHA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamBAI") = "�" Then chkLamBAI.CheckState = CheckState.Checked : Else chkLamBAI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamBPS") = "�" Then chkLamBPS.CheckState = CheckState.Checked : Else chkLamBPS.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamTRF") = "�" Then chkLamTRF.CheckState = CheckState.Checked : Else chkLamTRF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamTBN") = "�" Then chkLamTBN.CheckState = CheckState.Checked : Else chkLamTBN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamTTG") = "�" Then chkLamTTG.CheckState = CheckState.Checked : Else chkLamTTG.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamSFR") = "�" Then
                    chkLamSFR.CheckState = CheckState.Checked
                    cboLamSFR.Enabled = True
                Else
                    chkLamSFR.CheckState = CheckState.Unchecked
                End If
                If LeggiAttributo(stPropertySet, "LamLCM") = "�" Then chkLamLCM.CheckState = CheckState.Checked : Else chkLamLCM.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamDNI") = "�" Then chkLamDNI.CheckState = CheckState.Checked : Else chkLamDNI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamDCA") = "�" Then chkLamDCA.CheckState = CheckState.Checked : Else chkLamDCA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamDCL") = "�" Then chkLamDCL.CheckState = CheckState.Checked : Else chkLamDCL.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamHAD") = "�" Then chkLamHAD.CheckState = CheckState.Checked : Else chkLamHAD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamSCQ") = "�" Then chkLamSCQ.CheckState = CheckState.Checked : Else chkLamSCQ.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "LamCLD") = "�" Then chkLamCLD.CheckState = CheckState.Checked : Else chkLamCLD.CheckState = CheckState.Unchecked


                txtLamRAD.Text = LeggiAttributo(stPropertySet, "txtLamRAD")
                txtLamCHA.Text = LeggiAttributo(stPropertySet, "txtLamCHA")
                txtLamBAI.Text = LeggiAttributo(stPropertySet, "txtLamBAI")
                cboLamTTG.Text = LeggiAttributo(stPropertySet, "txtLamTTG")
                cboLamSFR.Text = LeggiAttributo(stPropertySet, "txtLamSFR")

            Case "PRESSOFUSO"
                For Each chk In pnlPressofuso.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then chk.Checked = False
                Next
                If LeggiAttributo(stPropertySet, "PreDRA") = "�" Then chkPreDRA.CheckState = CheckState.Checked : Else chkPreDRA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreDAR") = "�" Then chkPreDAR.CheckState = CheckState.Checked : Else chkPreDAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreDNI") = "�" Then chkPreDNI.CheckState = CheckState.Checked : Else chkPreDNI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreRAD") = "�" Then chkPreRAD.CheckState = CheckState.Checked : Else chkPreRAD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreCHA") = "�" Then chkPreCHA.CheckState = CheckState.Checked : Else chkPreCHA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PrePLF") = "�" Then chkPrePLF.CheckState = CheckState.Checked : Else chkPrePLF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreMGM") = "�" Then chkPreMGM.CheckState = CheckState.Checked : Else chkPreMGM.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreMOF") = "�" Then chkPreMOF.CheckState = CheckState.Checked : Else chkPreMOF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreMEP") = "�" Then chkPreMEP.CheckState = CheckState.Checked : Else chkPreMEP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreMAE") = "�" Then chkPreMAE.CheckState = CheckState.Checked : Else chkPreMAE.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreSCB") = "�" Then chkPreSCB.CheckState = CheckState.Checked : Else chkPreSCB.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreIPT") = "�" Then chkPreIPT.CheckState = CheckState.Checked : Else chkPreIPT.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreIPP") = "�" Then chkPreIPP.CheckState = CheckState.Checked : Else chkPreIPP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreIPJ") = "�" Then chkPreIPJ.CheckState = CheckState.Checked : Else chkPreIPJ.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreTRF") = "�" Then chkPreTRF.CheckState = CheckState.Checked : Else chkPreTRF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreTBN") = "�" Then chkPreTBN.CheckState = CheckState.Checked : Else chkPreTBN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreTTG") = "�" Then chkPreTTG.CheckState = CheckState.Checked : Else chkPreTTG.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreHOS") = "�" Then chkPreHOS.CheckState = CheckState.Checked : Else chkPreHOS.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreSAR") = "�" Then chkPreSAR.CheckState = CheckState.Checked : Else chkPreSAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreMAR") = "�" Then chkPreMAR.CheckState = CheckState.Checked : Else chkPreMAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreMRA") = "�" Then chkPreMRA.CheckState = CheckState.Checked : Else chkPreMRA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreMNR") = "�" Then chkPreMNR.CheckState = CheckState.Checked : Else chkPreMNR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreCLH") = "�" Then chkPreCLH.CheckState = CheckState.Checked : Else chkPreCLH.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreFSA") = "�" Then chkPreFSA.CheckState = CheckState.Checked : Else chkPreFSA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreFSH") = "�" Then chkPreFSH.CheckState = CheckState.Checked : Else chkPreFSH.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreDCA") = "�" Then chkPreDCA.CheckState = CheckState.Checked : Else chkPreDCA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "PreDCL") = "�" Then chkPreDCL.CheckState = CheckState.Checked : Else chkPreDCL.CheckState = CheckState.Unchecked

                cboPreDRA.Text = LeggiAttributo(stPropertySet, "txtPreDRA")
                txtPreRAD.Text = LeggiAttributo(stPropertySet, "txtPreRAD")
                txtPreCHA.Text = LeggiAttributo(stPropertySet, "txtPreCHA")
                cboPrePLF.Text = LeggiAttributo(stPropertySet, "txtPrePLF")
                txtPreMGM.Text = LeggiAttributo(stPropertySet, "txtPreMGM")
                txtPreMOF.Text = LeggiAttributo(stPropertySet, "txtPreMOF")
                txtPreMEP.Text = LeggiAttributo(stPropertySet, "txtPreMEP")
                cboPreMAE.Text = LeggiAttributo(stPropertySet, "txtPreMAE")
                txtPreIPT.Text = LeggiAttributo(stPropertySet, "txtPreIPT")
                txtPreIPP.Text = LeggiAttributo(stPropertySet, "txtPreIPP")
                txtPreIPJ.Text = LeggiAttributo(stPropertySet, "txtPreIPJ")
                cboPreTTG.Text = LeggiAttributo(stPropertySet, "txtPreTTG")
                txtPreMAH.Text = LeggiAttributo(stPropertySet, "txtPreMAH")
                cboAbbreviazione.Text = LeggiAttributo(stPropertySet, "txtPreABB")
                'cboPreDRA.Enabled = True
                'cboPrePLF.Enabled = True
                'txtPreMGM.Enabled = True
                'txtPreMOF.Enabled = True
                'txtPreMEP.Enabled = True
                'cboPreMAE.Enabled = True

            Case "MOLLA"
            Case "TORNERIA"
                For Each chk In pnlTorneria.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then chk.Checked = False
                Next
                If LeggiAttributo(stPropertySet, "TorEFC") = "�" Then chkTorEFC.CheckState = CheckState.Checked : Else chkTorEFC.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorEFN") = "�" Then chkTorEFN.CheckState = CheckState.Checked : Else chkTorEFN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorIES") = "�" Then chkTorIES.CheckState = CheckState.Checked : Else chkTorIES.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorIER") = "�" Then chkTorIER.CheckState = CheckState.Checked : Else chkTorIER.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorIEC") = "�" Then chkTorIEC.CheckState = CheckState.Checked : Else chkTorIEC.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorEES") = "�" Then chkTorEES.CheckState = CheckState.Checked : Else chkTorEES.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorEER") = "�" Then chkTorEER.CheckState = CheckState.Checked : Else chkTorEER.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorEEC") = "�" Then chkTorEEC.CheckState = CheckState.Checked : Else chkTorEEC.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorCHR") = "�" Then chkTorCHR.CheckState = CheckState.Checked : Else chkTorCHR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorCHB") = "�" Then chkTorCHB.CheckState = CheckState.Checked : Else chkTorCHB.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorCHS") = "�" Then chkTorCHS.CheckState = CheckState.Checked : Else chkTorCHS.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorCHN") = "�" Then chkTorCHN.CheckState = CheckState.Checked : Else chkTorCHN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorDNI") = "�" Then chkTorDNI.CheckState = CheckState.Checked : Else chkTorDNI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorDCA") = "�" Then chkTorDCA.CheckState = CheckState.Checked : Else chkTorDCA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorDCL") = "�" Then ChkTorDCL.CheckState = CheckState.Checked : Else ChkTorDCL.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorDCO") = "�" Then chkTorDCO.CheckState = CheckState.Checked : Else chkTorDCO.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorCLE") = "�" Then chkTorCLE.CheckState = CheckState.Checked : Else chkTorCLE.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "TorCAQ") = "�" Then chkTorCAQ.CheckState = CheckState.Checked : Else chkTorCAQ.CheckState = CheckState.Unchecked

                txtTorNIB.Text = LeggiAttributo(stPropertySet, "txtTorNIB")
                txtTorH.Text = LeggiAttributo(stPropertySet, "txtTorEFH")
                cboTorIES.Text = LeggiAttributo(stPropertySet, "txtTorIES")
                cboTorEES.Text = LeggiAttributo(stPropertySet, "txtTorEES")
                cboTorCHS.Text = LeggiAttributo(stPropertySet, "txtTorCHS")
                cboTorCHN.Text = LeggiAttributo(stPropertySet, "txtTorCHN")

            Case "GENERICO"
            Case "FRESATURA"
                For Each chk In pnlFresatura.Controls
                    If (chk.GetType() Is GetType(CheckBox)) Then chk.Checked = False
                Next
                If LeggiAttributo(stPropertySet, "FreBHM") = "�" Then chkFreBHM.CheckState = CheckState.Checked : Else chkFreBHM.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreBHF") = "�" Then chkFreBHF.CheckState = CheckState.Checked : Else chkFreBHF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreSEN") = "�" Then chkFreSEN.CheckState = CheckState.Checked : Else chkFreSEN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreBER") = "�" Then chkFreBER.CheckState = CheckState.Checked : Else chkFreBER.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreBEC") = "�" Then chkFreBEC.CheckState = CheckState.Checked : Else chkFreBEC.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreRAD") = "�" Then chkFreRAD.CheckState = CheckState.Checked : Else chkFreRAD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreCHA") = "�" Then chkFreCHA.CheckState = CheckState.Checked : Else chkFreCHA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreIPT") = "�" Then chkFreIPT.CheckState = CheckState.Checked : Else chkFreIPT.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreIPP") = "�" Then chkFreIPP.CheckState = CheckState.Checked : Else chkFreIPP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreTRF") = "�" Then chkFreTRF.CheckState = CheckState.Checked : Else chkFreTRF.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreTBN") = "�" Then chkFreTBN.CheckState = CheckState.Checked : Else chkFreTBN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreTTG") = "�" Then chkFreTTG.CheckState = CheckState.Checked : Else chkFreTTG.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreRWP") = "�" Then chkFreRWP.CheckState = CheckState.Checked : Else chkFreRWP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FrePFD") = "�" Then chkFrePFD.CheckState = CheckState.Checked : Else chkFrePFD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreDNI") = "�" Then chkFreDNI.CheckState = CheckState.Checked : Else chkFreDNI.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreDCA") = "�" Then chkFreDCA.CheckState = CheckState.Checked : Else chkFreDCA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreDCL") = "�" Then chkFreDCL.CheckState = CheckState.Checked : Else chkFreDCL.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreHAD") = "�" Then chkFreHAD.CheckState = CheckState.Checked : Else chkFreHAD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreSAR") = "�" Then chkFreSAR.CheckState = CheckState.Checked : Else chkFreSAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "FreCLD") = "�" Then chkFreCLD.CheckState = CheckState.Checked : Else chkFreCLD.CheckState = CheckState.Unchecked

                cboFreSEU.Text = LeggiAttributo(stPropertySet, "txtFreSEU")
                cboFreSEL.Text = LeggiAttributo(stPropertySet, "txtFreSEL")
                txtFreRAD.Text = LeggiAttributo(stPropertySet, "txtFreRAD")
                txtFreCHA.Text = LeggiAttributo(stPropertySet, "txtFreCHA")
                txtFreIPT.Text = LeggiAttributo(stPropertySet, "txtFreIPT")
                txtFreIPP.Text = LeggiAttributo(stPropertySet, "txtFreIPP")
                cboFreTTG.Text = LeggiAttributo(stPropertySet, "txtFreTTG")

            Case "GR OFFICINA"
                If LeggiAttributo(stPropertySet, "OffSWP") = "�" Then chkOffSWP.CheckState = CheckState.Checked : Else chkOffSWP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffSWM") = "�" Then chkOffSWM.CheckState = CheckState.Checked : Else chkOffSWM.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffIPT") = "�" Then chkOffIPT.CheckState = CheckState.Checked : Else chkOffIPT.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffIPP") = "�" Then chkOffIPP.CheckState = CheckState.Checked : Else chkOffIPP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffIPC") = "�" Then chkOffIPC.CheckState = CheckState.Checked : Else chkOffIPC.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffRWP") = "�" Then chkOffRWP.CheckState = CheckState.Checked : Else chkOffRWP.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffPFD") = "�" Then chkOffPFD.CheckState = CheckState.Checked : Else chkOffPFD.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffSFR") = "�" Then chkOffSFR.CheckState = CheckState.Checked : Else chkOffSFR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffDNR") = "�" Then chkOffDNR.CheckState = CheckState.Checked : Else chkOffDNR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffDCA") = "�" Then chkOffDCA.CheckState = CheckState.Checked : Else chkOffDCA.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffDCL") = "�" Then chkOffDCL.CheckState = CheckState.Checked : Else chkOffDCL.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffSBN") = "�" Then chkOffSBN.CheckState = CheckState.Checked : Else chkOffSBN.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffSAR") = "�" Then chkOffSAR.CheckState = CheckState.Checked : Else chkOffSAR.CheckState = CheckState.Unchecked
                If LeggiAttributo(stPropertySet, "OffCDN") = "�" Then chkOffCDN.CheckState = CheckState.Checked : Else chkOffCDN.CheckState = CheckState.Unchecked

                txtOffSWP.Text = LeggiAttributo(stPropertySet, "txtOffSWP")
                txtOffSWM.Text = LeggiAttributo(stPropertySet, "txtOffSWM")
                txtOffIPT.Text = LeggiAttributo(stPropertySet, "txtOffIPT")
                txtOffIPP.Text = LeggiAttributo(stPropertySet, "txtOffIPP")
                cboOffSFR.Text = LeggiAttributo(stPropertySet, "txtOffSFR")

        End Select

        'Leggo gli attributi comuni
        stPropertySet = "ProjectInformation"
        txtCodice.Text = LeggiAttributo(stPropertySet, "Document Number")
        'Verifico che il codice letto sia uguale al nome del file
        'If stFileCodice <> txtCodice.Text Then
        '    MsgBox("Il codice � diverso dal nome del file", MsgBoxStyle.Information, "ATTENZIONE")
        'End If
        txtLivello.Text = LeggiAttributo(stPropertySet, "Revision")
        'Verifico che il livello letto sia uguale al nome del file
        'If stFileLivello <> txtLivello.Text Then
        '    MsgBox("Il livello del nome del file � diverso o non presente", MsgBoxStyle.Information, "ATTENZIONE")
        'End If

        stPropertySet = "SummaryInformation"
        txtAutore.Text = LeggiAttributo(stPropertySet, "Autore") 'Author Autore
        txtDescrizione.Text = LeggiAttributo(stPropertySet, "Oggetto")

        stPropertySet = "DocumentSummaryInformation"
        txtResponsabile.Text = LeggiAttributo(stPropertySet, "Manager")
        cboCodifica.Text = LeggiAttributo(stPropertySet, "Categoria") 'Category Categoria

    End Sub

    Private Sub cmdLeggi_Click(sender As Object, e As EventArgs) Handles cmdLeggi.Click
        Call LeggiAttributi()
    End Sub

    Private Sub TextBox_GotFocus(sender As Object, e As EventArgs) Handles txtCodice.GotFocus, txtLivello.GotFocus, txtDescrizione.GotFocus, txtSostituisce.GotFocus, txtDerivato.GotFocus, txtDestinazione.GotFocus, txtSemilavorato.GotFocus, txtAutore.GotFocus, txtResponsabile.GotFocus, cboData.GotFocus, cboScala.GotFocus, cboFamiglia.GotFocus, cboCodifica.GotFocus, cboDescrizione.GotFocus, cboMateriale.GotFocus, cboPNMateriale.GotFocus, cboAbbreviazione.GotFocus, cboSpessore.GotFocus, cboTTermico.GotFocus, cboProduttore.GotFocus, cboColore.GotFocus, cboTSuperficiale.GotFocus, cboFlame.GotFocus, cboFileUL.GotFocus, cboRating.GotFocus, txtMod01.GotFocus, txtData01.GotFocus, txtAut01.GotFocus, txtMod02.GotFocus, txtData02.GotFocus, txtAut02.GotFocus, txtMod03.GotFocus, txtData03.GotFocus, txtAut03.GotFocus, txtMod04.GotFocus, txtData04.GotFocus, txtAut04.GotFocus, txtMod05.GotFocus, txtData05.GotFocus, txtAut05.GotFocus, txtMod06.GotFocus, txtData06.GotFocus, txtAut06.GotFocus
        On Error Resume Next
        sender.BackColor = Color.Yellow
        sender.SelectAll()
    End Sub

    Private Sub TextBox_LostFocus(sender As Object, e As EventArgs) Handles txtCodice.LostFocus, txtLivello.LostFocus, txtDescrizione.LostFocus, txtSostituisce.LostFocus, txtDerivato.LostFocus, txtDestinazione.LostFocus, txtSemilavorato.LostFocus, txtAutore.LostFocus, txtResponsabile.LostFocus, cboData.LostFocus, cboScala.LostFocus, cboFamiglia.LostFocus, cboCodifica.LostFocus, cboDescrizione.LostFocus, cboMateriale.LostFocus, cboPNMateriale.LostFocus, cboAbbreviazione.LostFocus, cboSpessore.LostFocus, cboTTermico.LostFocus, cboProduttore.LostFocus, cboColore.LostFocus, cboTSuperficiale.LostFocus, cboFlame.LostFocus, cboFileUL.LostFocus, cboRating.LostFocus, txtMod01.LostFocus, txtData01.LostFocus, txtAut01.LostFocus, txtMod02.LostFocus, txtData02.LostFocus, txtAut02.LostFocus, txtMod03.LostFocus, txtData03.LostFocus, txtAut03.LostFocus, txtMod04.LostFocus, txtData04.LostFocus, txtAut04.LostFocus, txtMod05.LostFocus, txtData05.LostFocus, txtAut05.LostFocus, txtMod06.LostFocus, txtData06.LostFocus, txtAut06.LostFocus
        On Error Resume Next
        sender.BackColor = Color.White
        Select Case sender.name.ToString
            Case cboMateriale.Name
            Case cboTTermico.Name
            Case cboTSuperficiale.Name
            Case cboSpessore.Name
            Case cboFamiglia.Name
            Case Else
                sender.text = UCase(sender.text)
        End Select

    End Sub

    Private Sub chkLam_CheckedChanged(sender As Object, e As EventArgs) Handles chkLamRAD.CheckedChanged, chkLamCHA.CheckedChanged, chkLamBAI.CheckedChanged, chkLamSFR.CheckedChanged, chkLamTTG.CheckedChanged, chkLamBHM.Click, chkLamBHF.Click, chkLamBHE.Click, chkLamBER.Click, chkLamBEC.Click
        Select Case sender.name.ToString
            Case chkLamBHM.Name
                cboLamESH.Enabled = False
                cboLamESL.Enabled = False
                cboLamESH.Text = ""
                cboLamESL.Text = ""
                chkLamBER.CheckState = CheckState.Unchecked
                chkLamBEC.CheckState = CheckState.Unchecked
                chkLamBER.Enabled = False
                chkLamBEC.Enabled = False
                If chkLamBHM.CheckState = CheckState.Checked Then
                    chkLamBHE.CheckState = CheckState.Unchecked
                    chkLamBHF.CheckState = CheckState.Unchecked
                    chkLamBER.CheckState = CheckState.Unchecked
                    chkLamBEC.CheckState = CheckState.Unchecked
                Else
                    chkLamBHE.CheckState = CheckState.Unchecked
                    chkLamBHF.CheckState = CheckState.Checked
                End If
            Case chkLamBHF.Name
                cboLamESH.Enabled = False
                cboLamESL.Enabled = False
                cboLamESH.Text = ""
                cboLamESL.Text = ""
                chkLamBER.CheckState = CheckState.Unchecked
                chkLamBEC.CheckState = CheckState.Unchecked
                chkLamBER.Enabled = False
                chkLamBEC.Enabled = False
                If chkLamBHF.CheckState = CheckState.Checked Then
                    chkLamBHE.CheckState = CheckState.Unchecked
                    chkLamBHM.CheckState = CheckState.Unchecked
                    chkLamBER.CheckState = CheckState.Unchecked
                    chkLamBEC.CheckState = CheckState.Unchecked
                Else
                    chkLamBHE.CheckState = CheckState.Unchecked
                    chkLamBHM.CheckState = CheckState.Checked
                End If
            Case chkLamBHE.Name
                If chkLamBHE.CheckState = CheckState.Checked Then
                    chkLamBER.CheckState = CheckState.Checked
                    chkLamBEC.CheckState = CheckState.Unchecked
                    chkLamBHF.CheckState = CheckState.Unchecked
                    chkLamBHM.CheckState = CheckState.Unchecked
                    chkLamBEC.Enabled = True
                    chkLamBER.Enabled = True
                    cboLamESH.Enabled = True
                    cboLamESL.Enabled = True
                    cboLamESH.Text = "-0,10"
                    cboLamESL.Text = "-0,30"
                    cboLamESH.Focus()
                Else
                    chkLamBHF.CheckState = CheckState.Checked
                    chkLamBHM.CheckState = CheckState.Unchecked
                    cboLamESH.Enabled = False
                    cboLamESL.Enabled = False
                    cboLamESH.Text = ""
                    cboLamESL.Text = ""
                    chkLamBER.CheckState = CheckState.Unchecked
                    chkLamBEC.CheckState = CheckState.Unchecked
                    chkLamBER.Enabled = False
                    chkLamBEC.Enabled = False
                End If
            Case chkLamBER.Name
                If chkLamBER.CheckState = CheckState.Checked Then
                    chkLamBEC.CheckState = CheckState.Unchecked
                Else
                    chkLamBEC.CheckState = CheckState.Checked
                End If
            Case chkLamBEC.Name
                If chkLamBEC.CheckState = CheckState.Checked Then
                    chkLamBER.CheckState = CheckState.Unchecked
                Else
                    chkLamBER.CheckState = CheckState.Checked
                End If
            Case chkLamRAD.Name
                If sender.CheckState = CheckState.Checked Then
                    txtLamRAD.Enabled = True
                    txtLamRAD.Text = ""
                    txtLamRAD.Focus()
                Else
                    txtLamRAD.Enabled = False
                    txtLamRAD.Text = ""
                End If
            Case chkLamCHA.Name
                If sender.CheckState = CheckState.Checked Then
                    txtLamCHA.Enabled = True
                    txtLamCHA.Text = ""
                    txtLamCHA.Focus()
                Else
                    txtLamCHA.Enabled = False
                    txtLamCHA.Text = ""
                End If
            Case chkLamBAI.Name
                If sender.CheckState = CheckState.Checked Then
                    txtLamBAI.Enabled = True
                    txtLamBAI.Text = ""
                    txtLamBAI.Focus()
                Else
                    txtLamBAI.Enabled = False
                    txtLamBAI.Text = ""
                End If
            Case chkLamTTG.Name
                If sender.CheckState = CheckState.Checked Then
                    cboLamTTG.Enabled = True
                    cboLamTTG.Text = "6H"
                    cboLamTTG.Focus()
                Else
                    cboLamTTG.Enabled = False
                    cboLamTTG.Text = ""
                End If
            Case chkLamSFR.Name
                If sender.CheckState = CheckState.Checked Then
                    cboLamSFR.Enabled = True
                    cboLamSFR.Text = ""
                    cboLamSFR.Focus()
                Else
                    cboLamSFR.Enabled = False
                    cboLamSFR.Text = ""
                End If
        End Select
    End Sub

    Private Sub FreBurrHeight_CheckedChanged(sender As Object, e As EventArgs) Handles chkFreBHM.Click, chkFreBHF.Click, chkFreSEN.Click
        Dim sharpeEdge As Boolean = False
        Select Case sender.name.ToString
            Case chkFreBHM.Name
                chkFreBHM.Checked = True
                chkFreBHF.Checked = False
                chkFreSEN.Checked = False
            Case chkFreBHF.Name
                chkFreBHF.Checked = True
                chkFreBHM.Checked = False
                chkFreSEN.Checked = False
            Case chkFreSEN.Name
                chkFreSEN.Checked = True
                chkFreBHF.Checked = False
                chkFreBHM.Checked = False
                sharpeEdge = True
        End Select

        If sharpeEdge = True Then
            cboFreSEU.Enabled = True
            cboFreSEL.Enabled = True
            chkFreBER.Enabled = True
            chkFreBEC.Enabled = True
            chkFreBER.CheckState = CheckState.Indeterminate
            chkFreBEC.CheckState = CheckState.Indeterminate
            cboFreSEU.Text = "0.10"
            cboFreSEL.Text = "0.50"
        Else
            cboFreSEU.Enabled = False
            cboFreSEL.Enabled = False
            chkFreBER.Enabled = False
            chkFreBEC.Enabled = False
            chkFreBER.CheckState = CheckState.Unchecked
            chkFreBEC.CheckState = CheckState.Unchecked
            cboFreSEU.Text = ""
            cboFreSEL.Text = ""
        End If

        'Select Case sender.name.ToString
        '    Case chkFreBHM.Name
        '        If chkFreBHM.Checked = True Then
        '            chkFreBHF.Checked = False
        '            chkFreSEN.Checked = False
        '            cboFreSEU.Text = ""
        '            cboFreSEU.Enabled = False
        '            cboFreSEL.Text = ""
        '            cboFreSEL.Enabled = False
        '        Else
        '            chkFreBHF.Checked = True
        '        End If
        '    Case chkFreBHF.Name
        '        If chkFreBHF.Checked = True Then
        '            chkFreBHM.Checked = False
        '            chkFreBER.Enabled = False
        '            chkFreBER.CheckState = CheckState.Unchecked
        '            chkFreBEC.Enabled = False
        '            chkFreBEC.CheckState = CheckState.Unchecked
        '            cboFreSEU.Text = ""
        '            cboFreSEU.Enabled = False
        '            cboFreSEL.Text = ""
        '            cboFreSEL.Enabled = False
        '            chkFreSEN.Checked = False
        '        Else
        '            chkFreBHM.Checked = True
        '        End If
        '    Case chkFreSEN.Name
        '        If chkFreSEN.CheckState = CheckState.Checked Then
        '            chkFreBER.Enabled = True
        '            chkFreBER.CheckState = CheckState.Checked
        '            chkFreBEC.Enabled = True
        '            chkFreBHM.CheckState = CheckState.Unchecked
        '            chkFreBHF.CheckState = CheckState.Unchecked
        '        Else
        '            chkFreBER.Enabled = False
        '            chkFreBER.CheckState = CheckState.Unchecked
        '            chkFreBEC.Enabled = False
        '            cboFreSEU.Text = ""
        '            cboFreSEU.Enabled = False
        '            cboFreSEL.Text = ""
        '            cboFreSEL.Enabled = False
        '            chkFreBHM.Checked = True
        '        End If
        'End Select
        'If chkFreBHM.Checked = False And chkFreBHF.Checked = False Then chkFreBHM.Checked = True
    End Sub


    Private Sub chkFre_CheckedChanged(sender As Object, e As EventArgs) Handles chkFreTTG.CheckedChanged, chkFreCHA.CheckedChanged, chkFreIPT.CheckedChanged, chkFreRAD.CheckedChanged, chkFreIPP.CheckedChanged

        Select Case sender.name.ToString
            Case chkFreSEN.Name
                'If sender.CheckState = CheckState.Checked Then
                '    chkFreBER.Enabled = True
                '    chkFreBEC.Enabled = True
                '    chkFreBER.Checked = True
                '    txtFreSEU.Enabled = True
                '    txtFreSEL.Enabled = True
                'Else
                '    chkFreBER.Enabled = False
                '    chkFreBEC.Enabled = False
                '    chkFreBER.Checked = False
                '    chkFreBEC.Checked = False
                'End If
            Case chkFreRAD.Name
                txtFreRAD.Text = ""
                If sender.CheckState = CheckState.Checked Then
                    txtFreRAD.Enabled = True
                    txtFreRAD.Focus()
                Else
                    txtFreRAD.Enabled = False
                End If
            Case chkFreCHA.Name
                txtFreCHA.Text = ""
                If sender.CheckState = CheckState.Checked Then
                    txtFreCHA.Enabled = True
                    txtFreCHA.Focus()
                Else
                    txtFreCHA.Enabled = False
                End If
            Case chkFreTTG.Name
                cboFreTTG.Text = ""
                If sender.CheckState = CheckState.Checked Then
                    cboFreTTG.Enabled = True
                    cboFreTTG.Text = "6H"
                    cboFreTTG.Focus()
                Else
                    cboFreTTG.Enabled = False
                End If
            Case chkFreIPP.Name
                txtFreIPP.Text = ""
                If sender.CheckState = CheckState.Checked Then
                    txtFreIPP.Enabled = True
                    txtFreIPP.Focus()
                Else
                    txtFreIPP.Enabled = False
                End If
            Case chkFreIPT.Name
                txtFreIPT.Text = ""
                If sender.CheckState = CheckState.Checked Then
                    txtFreIPT.Enabled = True
                    txtFreIPT.Focus()
                Else
                    txtFreIPT.Enabled = False
                End If
        End Select
    End Sub

    Private Sub chkTor_CheckedChanged(sender As Object, e As EventArgs) Handles chkTorEFN.CheckedChanged, chkTorEFC.CheckedChanged

        Select Case sender.name.ToString
            Case chkTorEFC.Name
                chkTorEFN.Checked = Not chkTorEFC.Checked
            Case chkTorEFN.Name
                chkTorEFC.Checked = Not chkTorEFN.Checked
                If sender.checked = False Then txtTorNIB.Text = ""
                If sender.checked = False Then txtTorH.Text = ""
                txtTorNIB.Enabled = sender.checked
                txtTorH.Enabled = sender.checked
                txtTorNIB.Focus()
        End Select
    End Sub

    Private Sub cmdEsci_Click(sender As Object, e As EventArgs) Handles cmdEsci.Click
        End
    End Sub

    Private Sub cmdAzzeraRev_Click(sender As Object, e As EventArgs) Handles cmdAzzeraRev.Click
        txtMod01.Text = ""
        txtMod02.Text = ""
        txtMod03.Text = ""
        txtMod04.Text = ""
        txtMod05.Text = ""
        txtMod06.Text = ""
        txtData01.Text = ""
        txtData02.Text = ""
        txtData03.Text = ""
        txtData04.Text = ""
        txtData05.Text = ""
        txtData06.Text = ""
        txtAut01.Text = ""
        txtAut02.Text = ""
        txtAut03.Text = ""
        txtAut04.Text = ""
        txtAut05.Text = ""
        txtAut06.Text = ""

    End Sub


    Private Sub cmdReset_Click(sender As Object, e As EventArgs) Handles cmdReset.Click
        For Each nota As Object In Me.pnlFresatura.Controls
            If TypeOf nota Is CheckBox Then
                If nota.enabled = False Then
                    nota.CheckState = CheckState.Checked
                Else
                    nota.CheckState = CheckState.Indeterminate
                End If
            End If
            If TypeOf nota Is System.Windows.Forms.TextBox Then
                nota.text = ""
                nota.enabled = False
            End If
            If TypeOf nota Is System.Windows.Forms.ComboBox Then
                nota.text = ""
                nota.enabled = False
            End If
        Next
        For Each nota As Object In Me.pnlGomma.Controls
            If TypeOf nota Is CheckBox Then
                If nota.enabled = False Then
                    nota.CheckState = CheckState.Checked
                Else
                    nota.CheckState = CheckState.Indeterminate
                End If
            End If
            If TypeOf nota Is System.Windows.Forms.TextBox Then
                nota.text = ""
                nota.enabled = False
            End If
            If TypeOf nota Is System.Windows.Forms.ComboBox Then
                nota.text = ""
                nota.enabled = False
            End If
        Next
        For Each nota As Object In Me.pnlLamiera.Controls
            If TypeOf nota Is CheckBox Then
                If nota.enabled = False Then
                    nota.CheckState = CheckState.Checked
                Else
                    nota.CheckState = CheckState.Indeterminate
                End If
            End If
            If TypeOf nota Is System.Windows.Forms.TextBox Then
                nota.text = ""
                nota.enabled = False
            End If
            If TypeOf nota Is System.Windows.Forms.ComboBox Then
                nota.text = ""
                nota.enabled = False
            End If
        Next
        For Each nota As Object In Me.pnlPressofuso.Controls
            If TypeOf nota Is CheckBox Then
                If nota.enabled = False Then
                    nota.CheckState = CheckState.Checked
                Else
                    nota.CheckState = CheckState.Indeterminate
                End If
            End If
            If TypeOf nota Is System.Windows.Forms.TextBox Then
                nota.text = ""
                nota.enabled = False
            End If
            If TypeOf nota Is System.Windows.Forms.ComboBox Then
                nota.text = ""
                nota.enabled = False
            End If
        Next
        For Each nota As Object In Me.pnlResina.Controls
            If TypeOf nota Is CheckBox Then
                If nota.enabled = False Then
                    nota.CheckState = CheckState.Checked
                Else
                    nota.CheckState = CheckState.Indeterminate
                End If
            End If
            If TypeOf nota Is System.Windows.Forms.TextBox Then
                nota.text = ""
                nota.enabled = False
            End If
            If TypeOf nota Is System.Windows.Forms.ComboBox Then
                nota.text = ""
                nota.enabled = False
            End If
        Next
        For Each nota As Object In Me.pnlTorneria.Controls
            If TypeOf nota Is CheckBox Then
                If nota.enabled = False Then
                    nota.CheckState = CheckState.Checked
                Else
                    nota.CheckState = CheckState.Indeterminate
                End If
            End If
            If TypeOf nota Is System.Windows.Forms.TextBox Then
                nota.text = ""
                nota.enabled = False
            End If
            If TypeOf nota Is System.Windows.Forms.ComboBox Then
                nota.text = ""
                nota.enabled = False
            End If
        Next
        For Each nota As Object In Me.pnlGrOfficina.Controls
            If TypeOf nota Is CheckBox Then
                If nota.enabled = False Then
                    nota.CheckState = CheckState.Checked
                Else
                    nota.CheckState = CheckState.Indeterminate
                End If
            End If
            If TypeOf nota Is System.Windows.Forms.TextBox Then
                nota.text = ""
                nota.enabled = False
            End If
            If TypeOf nota Is System.Windows.Forms.ComboBox Then
                nota.text = ""
                nota.enabled = False
            End If
        Next
    End Sub



    Private Sub chkTorIES_CheckedChanged(sender As Object, e As EventArgs) Handles chkTorIES.Click
        If chkTorIES.CheckState = CheckState.Checked Then
            cboTorIES.Enabled = True
            cboTorIES.Text = "0,2"
            chkTorIER.Enabled = True
            chkTorIEC.Enabled = True
            chkTorIER.Checked = True
        Else
            cboTorIES.Enabled = False
            cboTorIES.Text = ""
            chkTorIER.Enabled = False
            chkTorIEC.Enabled = False
            chkTorIER.Checked = False
            chkTorIEC.Checked = False
        End If
    End Sub

    Private Sub chkTorIER_CheckedChanged_1(sender As Object, e As EventArgs) Handles chkTorIER.Click
        If chkTorIES.CheckState = CheckState.Checked Then chkTorIEC.Checked = Not chkTorIER.Checked
    End Sub

    Private Sub chkTorIEC_CheckedChanged(sender As Object, e As EventArgs) Handles chkTorIEC.Click
        If chkTorIES.CheckState = CheckState.Checked Then chkTorIER.Checked = Not chkTorIEC.Checked
    End Sub

    Private Sub chkTorEES_CheckedChanged(sender As Object, e As EventArgs) Handles chkTorEES.Click
        If chkTorEES.CheckState = CheckState.Checked Then
            cboTorEES.Enabled = True
            cboTorEES.Text = "0,2"
            chkTorEER.Enabled = True
            chkTorEEC.Enabled = True
            chkTorEER.Checked = True
        Else
            cboTorEES.Enabled = False
            cboTorEES.Text = ""
            chkTorEER.Enabled = False
            chkTorEEC.Enabled = False
            chkTorEER.Checked = False
            chkTorEEC.Checked = False
        End If
    End Sub

    Private Sub chkTorEER_CheckedChanged_1(sender As Object, e As EventArgs) Handles chkTorEER.Click
        If chkTorEES.CheckState = CheckState.Checked Then chkTorEEC.Checked = Not chkTorEER.Checked
    End Sub

    Private Sub chkTorEEC_CheckedChanged(sender As Object, e As EventArgs) Handles chkTorEEC.Click
        If chkTorEES.CheckState = CheckState.Checked Then chkTorEER.Checked = Not chkTorEEC.Checked
    End Sub

    Private Sub chkTorCHS_CheckedChanged(sender As Object, e As EventArgs) Handles chkTorCHS.Click
        If chkTorCHS.CheckState = CheckState.Checked Then
            cboTorCHS.Enabled = True
            cboTorCHS.Text = "6H"
        Else
            cboTorCHS.Enabled = False
            cboTorCHS.Text = ""
        End If

    End Sub

    Private Sub chkTorCHN_CheckedChanged(sender As Object, e As EventArgs) Handles chkTorCHN.Click
        If chkTorCHN.CheckState = CheckState.Checked Then
            cboTorCHN.Enabled = True
            cboTorCHN.Text = "6H"
        Else
            cboTorCHN.Enabled = False
            cboTorCHN.Text = ""
        End If
    End Sub

    Private Sub chkFreBER_CheckedChanged(sender As Object, e As EventArgs) Handles chkFreBER.Click
        If chkFreBER.CheckState = CheckState.Checked Then
            chkFreBEC.CheckState = CheckState.Unchecked
        Else
            chkFreBEC.CheckState = CheckState.Checked
        End If
    End Sub



    Private Sub chkFreBEC_CheckedChanged(sender As Object, e As EventArgs) Handles chkFreBEC.Click
        If chkFreBEC.CheckState = CheckState.Checked Then
            chkFreBER.CheckState = CheckState.Unchecked
        Else
            chkFreBER.CheckState = CheckState.Checked
        End If
    End Sub


    Private Sub ResScriptMark_CheckedChanged(sender As Object, e As EventArgs) Handles chkResMAR.CheckedChanged, chkResMAN.CheckedChanged
        If chkResMAH.Checked = True Then
            Select Case sender.Name.ToString
                Case chkResMAR.Name
                    chkResMAN.Checked = Not chkResMAR.Checked
                Case chkResMAN.Name
                    chkResMAR.Checked = Not chkResMAN.Checked
            End Select
        End If
    End Sub



    Private Sub cboResVDI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboResSVD.SelectedIndexChanged, cboResSRA.SelectedIndexChanged
        cboResSVD.SelectedIndex = sender.selectedindex
        cboResSRA.SelectedIndex = sender.selectedindex
    End Sub

    Private Sub chkRes_CheckedChanged(sender As Object, e As EventArgs) Handles chkResDRA.CheckedChanged, chkResSVD.CheckedChanged, chkResRAD.CheckedChanged, chkResCHA.CheckedChanged, chkResIPT.CheckedChanged, chkResIPP.CheckedChanged, chkResIPJ.CheckedChanged, chkResMAH.CheckedChanged, chkResPLF.CheckedChanged, chkResMSA.CheckedChanged, chkResMGM.CheckedChanged, chkResMEM.CheckedChanged, chkResPLF.CheckedChanged, chkResMGM.CheckedChanged, chkResMEM.CheckedChanged, chkResMSA.CheckedChanged
        'txtResMAH.Enabled = sender.checked
        Select Case sender.name.ToString
            Case chkResPLF.Name
                If sender.checkstate = CheckState.Checked Then
                    cboResPLF.Enabled = True
                    cboResPLF.Focus()
                Else
                    cboResPLF.Text = ""
                    cboResPLF.Enabled = False
                End If
            Case chkResMGM.Name
                If sender.checkstate = CheckState.Checked Then
                    cboResMGM.Enabled = True
                    cboResMGM.Focus()
                Else
                    cboResMGM.Text = ""
                    cboResMGM.Enabled = False
                End If
            Case chkResMEM.Name
                If sender.checkstate = CheckState.Checked Then
                    cboResMEM.Enabled = True
                    cboResMEM.Focus()
                Else
                    cboResMEM.Text = ""
                    cboResMEM.Enabled = False
                End If
            Case chkResMSA.Name
                If sender.checkstate = CheckState.Checked Then
                    cboResMSA.Enabled = True
                    cboResMSA.Focus()
                Else
                    cboResMSA.Text = ""
                    cboResMSA.Enabled = False
                End If

            Case chkResDRA.Name
                If sender.checkstate = CheckState.Checked Then
                    cboResDRA.Enabled = True
                    cboResDRA.Focus()
                Else
                    cboResDRA.Text = ""
                    cboResDRA.Enabled = False
                End If
            Case chkResRAD.Name
                If sender.CheckState = CheckState.Checked Then
                    txtResRAD.Enabled = True
                    txtResRAD.Text = ""
                    txtResRAD.Focus()
                Else
                    txtResRAD.Enabled = False
                    txtResRAD.Text = ""
                End If
            Case chkResCHA.Name
                If sender.CheckState = CheckState.Checked Then
                    txtResCHA.Enabled = True
                    txtResCHA.Text = ""
                    txtResCHA.Focus()
                Else
                    txtResCHA.Enabled = False
                    txtResCHA.Text = ""
                End If
            Case chkResIPT.Name
                If sender.CheckState = CheckState.Checked Then
                    txtResIPT.Enabled = True
                    txtResIPT.Text = ""
                    txtResIPT.Focus()
                Else
                    txtResIPT.Enabled = False
                    txtResIPT.Text = ""
                End If
            Case chkResIPP.Name
                If sender.CheckState = CheckState.Checked Then
                    txtResIPP.Enabled = True
                    txtResIPP.Text = ""
                    txtResIPP.Focus()
                Else
                    txtResIPP.Enabled = False
                    txtResIPP.Text = ""
                End If
            Case chkResIPJ.Name
                If sender.CheckState = CheckState.Checked Then
                    txtResIPJ.Enabled = True
                    txtResIPJ.Text = ""
                    txtResIPJ.Focus()
                Else
                    txtResIPJ.Enabled = False
                    txtResIPJ.Text = ""
                End If
            Case chkResMAH.Name
                If sender.CheckState = CheckState.Checked Then
                    chkResMAR.CheckState = CheckState.Indeterminate
                    chkResMAN.CheckState = CheckState.Indeterminate
                    chkResMAR.Enabled = True
                    chkResMAN.Enabled = True
                    txtResMAH.Enabled = True
                    txtResMAH.Text = ""
                    txtResMAH.Focus()
                Else
                    chkResMAR.Enabled = False
                    chkResMAN.Enabled = False
                    chkResMAR.Checked = False
                    chkResMAN.Checked = False
                    txtResMAH.Enabled = False
                    txtResMAH.Text = ""
                End If
            Case chkResSVD.Name
                If sender.CheckState = CheckState.Checked Then
                    cboResSVD.Enabled = True
                    cboResSVD.Text = "27"
                    cboResSRA.Enabled = True
                    cboResSRA.Text = "2,2"
                    cboResSVD.Focus()
                Else
                    cboResSVD.Enabled = False
                    cboResSVD.Text = ""
                    cboResSRA.Enabled = False
                    cboResSRA.Text = ""
                End If
        End Select
    End Sub
    ''' <summary>
    ''' Check box checked changed
    ''' </summary>
    ''' <param name="sender">check box cliccato</param>
    ''' <param name="e">Event Args</param>
    Private Sub chkGom_CheckedChanged(sender As Object, e As EventArgs) Handles chkGomMAR.CheckedChanged, chkGomAST.CheckedChanged, chkGomASP.CheckedChanged, chkGomGTO.CheckedChanged, chkGomGTR.CheckedChanged, chkGomHAR.CheckedChanged, chkGomHGR.CheckedChanged, chkGomFLA.CheckedChanged
        Select Case sender.name.ToString
            Case chkGomFLA.Name
                If sender.CheckState = CheckState.Checked Then
                    cboGomFLH.Enabled = True
                    cboGomFLH.Text = ""
                    cboGomFLH.Focus()
                Else
                    cboGomFLH.Enabled = False
                    cboGomFLH.Text = ""
                End If
            Case chkGomMAR.Name
                If sender.CheckState = CheckState.Checked Then
                    txtGomMGT.Enabled = True
                    txtGomMGT.Text = ""
                    txtGomMGT.Focus()
                Else
                    txtGomMGT.Enabled = False
                    txtGomMGT.Text = ""
                End If
            Case chkGomAST.Name
                If sender.CheckState = CheckState.Checked Then
                    txtGomATO.Enabled = True
                    txtGomATO.Text = ""
                    txtGomATO.Focus()
                Else
                    txtGomATO.Enabled = False
                    txtGomATO.Text = ""
                End If
            Case chkGomASP.Name
                If sender.CheckState = CheckState.Checked Then
                    txtGomAPU.Enabled = True
                    txtGomAPU.Text = ""
                    txtGomAPU.Focus()
                Else
                    txtGomAPU.Enabled = False
                    txtGomAPU.Text = ""
                End If
            Case chkGomGTO.Name
                If sender.CheckState = CheckState.Checked Then
                    txtGomTOR.Enabled = True
                    txtGomTOR.Text = ""
                    txtGomTOR.Focus()
                Else
                    txtGomTOR.Enabled = False
                    txtGomTOR.Text = ""
                End If
            Case chkGomGTR.Name
                If sender.CheckState = CheckState.Checked Then
                    txtGomTRA.Enabled = True
                    txtGomTRA.Text = ""
                    txtGomTRA.Focus()
                Else
                    txtGomTRA.Enabled = False
                    txtGomTRA.Text = ""
                End If
            Case chkGomHAR.Name
                If sender.CheckState = CheckState.Checked Then
                    txtGomSHA.Enabled = True
                    txtGomSHA.Text = ""
                    txtGomSHD.Enabled = True
                    txtGomSHD.Text = ""
                    txtGomSHD.Focus()
                Else
                    txtGomSHA.Enabled = False
                    txtGomSHA.Text = ""
                    txtGomSHD.Enabled = False
                    txtGomSHD.Text = ""
                End If
            Case chkGomHGR.Name
                If sender.CheckState = CheckState.Checked Then
                    txtGomHRA.Enabled = True
                    txtGomHRA.Text = ""
                    txtGomHRA.Focus()
                Else
                    txtGomHRA.Enabled = False
                    txtGomHRA.Text = ""
                End If
        End Select
    End Sub

    Private Sub PreScriptMark_CheckedChanged(sender As Object, e As EventArgs) Handles chkPreMRA.CheckedChanged, chkPreMNR.CheckedChanged
        If chkPreMAR.Checked = True Then
            Select Case sender.Name.ToString
                Case chkPreMRA.Name
                    chkPreMNR.Checked = Not chkPreMRA.Checked
                Case chkPreMNR.Name
                    chkPreMRA.Checked = Not chkPreMNR.Checked
            End Select
        End If
    End Sub

    Private Sub chkPre_CheckedChanged(sender As Object, e As EventArgs) Handles chkPreDRA.CheckedChanged, chkPreDAR.CheckedChanged, chkPreDNI.CheckedChanged, chkPreRAD.CheckedChanged, chkPreCHA.CheckedChanged, chkPrePLF.CheckedChanged, chkPreMGM.CheckedChanged, chkPreMOF.CheckedChanged, chkPreMEP.CheckedChanged, chkPreMAE.CheckedChanged, chkPreSCB.CheckedChanged, chkPreIPT.CheckedChanged, chkPreIPP.CheckedChanged, chkPreIPJ.CheckedChanged, chkPreTRF.CheckedChanged, chkPreTBN.CheckedChanged, chkPreTTG.CheckedChanged, chkPreHOS.CheckedChanged, chkPreMAR.CheckedChanged, chkPreMRA.CheckedChanged, chkPreMNR.CheckedChanged, chkPreFSA.CheckedChanged, chkPreFSH.CheckedChanged, chkPreDCA.CheckedChanged, chkPreDCL.CheckedChanged
        Select Case sender.name.ToString
            Case chkPreDRA.Name
                If sender.CheckState = CheckState.Checked Then
                    cboPreDRA.Enabled = True
                    cboPreDRA.Text = "2.5�"
                    cboPreDRA.Focus()
                Else
                    cboPreDRA.Text = ""
                    cboPreDRA.Enabled = False
                End If
            Case chkPreRAD.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreRAD.Enabled = True
                    txtPreRAD.Text = ""
                    txtPreRAD.Focus()
                Else
                    txtPreRAD.Enabled = False
                End If
            Case chkPreCHA.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreCHA.Enabled = True
                    txtPreCHA.Text = ""
                    txtPreCHA.Focus()
                Else
                    txtPreCHA.Enabled = False
                End If
            Case chkPrePLF.Name
                If sender.CheckState = CheckState.Checked Then
                    cboPrePLF.Enabled = True
                    cboPrePLF.Text = "0.10"
                    cboPrePLF.Focus()
                Else
                    cboPrePLF.Text = ""
                    cboPrePLF.Enabled = False
                End If
            Case chkPreMGM.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreMGM.Enabled = True
                    txtPreMGM.Text = "0.20"
                    txtPreMGM.Focus()
                Else
                    txtPreMGM.Text = ""
                    txtPreMGM.Enabled = False
                End If
            Case chkPreMOF.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreMOF.Enabled = True
                    txtPreMOF.Text = "0.20"
                    txtPreMOF.Focus()
                Else
                    txtPreMOF.Text = ""
                    txtPreMOF.Enabled = False
                End If
            Case chkPreMEP.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreMEP.Enabled = True
                    txtPreMEP.Text = "0.30"
                    txtPreMEP.Focus()
                Else
                    txtPreMEP.Text = ""
                    txtPreMEP.Enabled = False
                End If
            Case chkPreMAE.Name
                If sender.CheckState = CheckState.Checked Then
                    cboPreMAE.Enabled = True
                    cboPreMAE.Text = "0.30"
                    cboPreMAE.Focus()
                Else
                    cboPreMAE.Text = ""
                    cboPreMAE.Enabled = False
                End If
            Case chkPreIPT.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreIPT.Enabled = True
                    txtPreIPT.Text = ""
                    txtPreIPT.Focus()
                Else
                    txtPreIPT.Enabled = False
                End If
            Case chkPreIPP.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreIPP.Enabled = True
                    txtPreIPP.Text = ""
                    txtPreIPP.Focus()
                Else
                    txtPreIPP.Enabled = False
                End If
            Case chkPreIPJ.Name
                If sender.CheckState = CheckState.Checked Then
                    txtPreIPJ.Enabled = True
                    txtPreIPJ.Text = ""
                    txtPreIPJ.Focus()
                Else
                    txtPreIPJ.Enabled = False
                End If
            Case chkPreTTG.Name
                If sender.CheckState = CheckState.Checked Then
                    cboPreTTG.Enabled = True
                    cboPreTTG.Text = "6H"
                    cboPreTTG.Focus()
                Else
                    cboPreTTG.Enabled = False
                    cboPreTTG.Text = ""
                End If
            Case chkPreMAR.Name
                If sender.CheckState = CheckState.Checked Then
                    chkPreMRA.CheckState = CheckState.Indeterminate
                    chkPreMNR.CheckState = CheckState.Indeterminate
                    txtPreMAH.Enabled = True
                    txtPreMAH.Text = ""
                    txtPreMAH.Focus()
                Else
                    chkPreMRA.Checked = False
                    chkPreMNR.Checked = False
                    txtPreMAH.Enabled = False
                    txtPreMAH.Text = ""
                    txtPreMAH.Text = ""
                End If
        End Select
    End Sub



    Private Sub AssembledPlateCheckChange(sender As Object, e As EventArgs) Handles chkOffCDN.CheckedChanged, chkOffDCA.CheckedChanged, chkOffDCL.CheckedChanged, chkOffDNR.CheckedChanged, chkOffIPC.CheckedChanged, chkOffIPP.CheckedChanged, chkOffIPT.CheckedChanged, chkOffPFD.CheckedChanged, chkOffRWP.CheckedChanged, chkOffSAR.CheckedChanged, chkOffSBN.CheckedChanged, chkOffSFR.CheckedChanged, chkOffSWM.CheckedChanged, chkOffSWP.CheckedChanged
        Select Case sender.name
            Case chkOffSWP.Name
                If sender.checkstate = CheckState.Checked Then
                    txtOffSWP.Enabled = True
                    txtOffSWP.Focus()
                Else
                    txtOffSWP.Text = ""
                    txtOffSWP.Enabled = False
                End If
            Case chkOffSWM.Name
                If sender.checkstate = CheckState.Checked Then
                    txtOffSWM.Enabled = True
                    txtOffSWM.Focus()
                Else
                    txtOffSWM.Text = ""
                    txtOffSWM.Enabled = False
                End If
            Case chkOffIPT.Name
                If sender.checkstate = CheckState.Checked Then
                    txtOffIPT.Enabled = True
                    txtOffIPT.Focus()
                Else
                    txtOffIPT.Text = ""
                    txtOffIPT.Enabled = False
                End If
            Case chkOffIPP.Name
                If sender.checkstate = CheckState.Checked Then
                    txtOffIPP.Enabled = True
                    txtOffIPP.Focus()
                Else
                    txtOffIPP.Text = ""
                    txtOffIPP.Enabled = False
                End If
            Case chkOffSFR.Name
                If sender.checkstate = CheckState.Checked Then
                    cboOffSFR.Enabled = True
                    cboOffSFR.Focus()
                Else
                    cboOffSFR.Text = ""
                    cboOffSFR.Enabled = False
                End If
        End Select
    End Sub
End Class
